//
//  ServiceUrl.swift
//  HZL CSC
//
//  Created by SARVANG INFOTCH on 25/09/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation


var BASE_URL = "http://3.129.226.32/EventApi/"








class URLConstants: NSObject
{
    /// Register
    
     static let MasterStateListGet = BASE_URL+"MasterStateListGet/"
     static let MasterCountryListGet = BASE_URL+"MasterCountryListGet/"
    static let MasterCategoryListGet = BASE_URL+"MasterCategoryListGet/"
    static let CreateOTP = BASE_URL+"CreateOTP/"
    static let OTPVerify = BASE_URL+"OTPVerify/"
    static let Login = BASE_URL+"Login/"
 static let RegistrationCreate = BASE_URL+"RegistrationCreate/"
    static let UpdatePic = BASE_URL+"UpdatePic/"
   static let UpdateProfilePic = BASE_URL+"UpdateProfilePic/"
    static let UpdateProfile = BASE_URL+"UpdateProfile/"
    static let UpdateCurrentLocation = BASE_URL+"UpdateCurrentLocation/"
     static let MateRequest = BASE_URL+"MateRequest/"
    static let ActivityJoin = BASE_URL+"ActivityJoin"
    static let ActivityFavoriteAdd = BASE_URL+"ActivityFavoriteAdd"
    static let UpdateUserCategory = BASE_URL+"UpdateUserCategory/"
    static let GetMyProfile = BASE_URL+"GetMyProfile/"
    static let GetOthersProfile = BASE_URL+"GetOthersProfile/"
    static let ChattingSession = BASE_URL+"ChattingSession"
     static let SuggestedMatesList = BASE_URL+"SuggestedMatesList"
     static let BlockUserList = BASE_URL+"BlockUserList"
    static let MyActivityInviteList = BASE_URL+"MyActivityInviteList"
    static let ActivityUpcomming = BASE_URL+"ActivityUpcomming"
   
    static let ActivityCreate = BASE_URL+"ActivityCreate"
    static let ActivityListFilter = BASE_URL+"ActivityListFilter"
    static let ActivityFavoriteList = BASE_URL+"ActivityFavoriteList"
    
    static let ActivityDetail = BASE_URL+"ActivityDetail"
    
    
    static let ChattingMessage = BASE_URL+"ChattingMessage"
    static let ChattingMessageSend = BASE_URL+"ChattiChattingMessageSendngMessage"
    
    /// move
    static let UpdateSetting = BASE_URL+"UpdateSetting"
    
    static let ForgotPWDRequest = BASE_URL+"ForgotPWDRequest"
    /// Settings

     static let appName = "HZLONE"
     static let appNameInner = "HZLONE"
 static let platform = "iOS"
  
    
    //static let platform = "Android"
}

class Fontconstant: NSObject
{
    static let FontData = ""
}
