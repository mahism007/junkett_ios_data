//
//  Almofire.swift
//  !dea@Balco
//
//  Created by Safiqul Islam on 15/07/17.
//  Copyright © 2017 Safiqul Islam. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class WebServices: NSObject
{

    static let sharedInstances = WebServices()
    
    func sendGetRequest(Url: String, successHandler: @escaping ([String: AnyObject])-> Void,failureHandler: @escaping ([Error]) -> Void)
    {

        Alamofire.request(URL.init(string: Url)!, method: .get, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in


            print("------\("")-------\n Request URL:",response.request!)

          //  print("Get Response:",response.result.value!)
            
            switch response.result
            {

            case .success(_):

                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break

            }

        }

    }


    func sendPostRequest(url:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
    {

        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in

            if(Debug.show_debug == true)
            {

                print("------\("")-------\n Request URL:",response.request!)

                print("parameters:",parameters)
//                do {
//             // try print("Post Response:",response.result.value!)
//
//                } catch {
//                    
                //}
            }



            switch response.result
            {

            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):

                failureHandler([error as Error])
                break


            }

        }
        
        
    }
    func sendPostRequestwithHeader(url:String, parameters:[String: Any],header : HTTPHeaders,successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
        {

            Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in

                if(Debug.show_debug == true)
                {

                    print("------\("")-------\n Request URL:",response.request!)

                    print("parameters:",parameters)
    //                do {
    //             // try print("Post Response:",response.result.value!)
    //
    //                } catch {
    //
                    //}
                }



                switch response.result
                {

                case .success(_):
                    if let json = response.result.value
                    {
                        successHandler((json as! [String: AnyObject]))
                    }
                    break
                case .failure(let error):

                    failureHandler([error as Error])
                    break


                }

            }
            
            
        }
    func sendPutRequestwithHeader(url:String, parameters:[String: Any],header : HTTPHeaders,successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
        {

            Alamofire.request(URL.init(string: url)!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in

                if(Debug.show_debug == true)
                {

                    print("------\("")-------\n Request URL:",response.request!)

                    print("parameters:",parameters)
    //                do {
    //             // try print("Post Response:",response.result.value!)
    //
    //                } catch {
    //
                    //}
                }



                switch response.result
                {

                case .success(_):
                    if let json = response.result.value
                    {
                        successHandler((json as! [String: AnyObject]))
                    }
                    break
                case .failure(let error):

                    failureHandler([error as Error])
                    break


                }

            }
            
            
        }
    func sendPostHttpsRequest(urlStr:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
    {
        
        let session = URLSession.shared
        let url = urlStr
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
            let task = session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
                if let response = response {
                    let nsHTTPResponse = response as! HTTPURLResponse
                    let statusCode = nsHTTPResponse.statusCode
                  
                }
                
                if let data = data {
                    do{
                        let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions())
                      //  print ("data = \(jsonResponse)")
                        successHandler((jsonResponse as! [String: AnyObject]))
                    }catch _ {
                        if let error = error {
                            print ("\(error)")
                            failureHandler([error as Error])
                        }
                       
                    }
                }
            })
            task.resume()
        }catch _ {
          
        }
        
        
    }
    
    
    func sendPostJSONRequest(url:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
    {
       
        
        
        
        
        Alamofire.request(URL.init(string: url)!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            if(Debug.show_debug == true)
            {
                
                print("------\("")-------\n Request URL:",response.request!)
                
                print("parameters:",parameters)
                //                do {
                //             // try print("Post Response:",response.result.value!)
                //
                //                } catch {
                //
                //}
            }
            
            
            
            switch response.result
            {
                
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
                
                
            }
            
        }
        
        
    }
    
   
    
    func sendPostRequestWithoutParameters(url:String,successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
    {
        
        Alamofire.request(URL.init(string: url)!, method: .post, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            if(Debug.show_debug == true)
            {
                
                print("------\("")-------\n Request URL:",response.request!)
                
                
                
                //print("Post Response:",response.result.value!)
                
                
            }
            
            
            
            switch response.result
            {
                
            case .success(_):
                if let json = response.result.value
                {
                    successHandler((json as! [String: AnyObject]))
                }
                break
            case .failure(let error):
                
                failureHandler([error as Error])
                break
                
                
            }
            
        }
        
        
    }


    
}

class Debug: NSObject {
    
    
    static let show_debug = true
    
}

//struct webService {
//
//    static let sharedInstance = webService()
//    func sendPostRequest(url:String, parameters:[String: Any],successHandler: @escaping ([String: AnyObject]) -> Void,failureHandler: @escaping ([Error]) -> Void)
//    {
//        var jsonFromFetchedData : NSMutableDictionary = NSMutableDictionary()
//
//        let fetchUrl : NSURL = NSURL(string: url)!
//        do {
//            let fetchDataFromUrl : NSData = try NSData(contentsOf: fetchUrl as URL)
//            jsonFromFetchedData = try JSONSerialization.jsonObject(with: fetchDataFromUrl as Data, options: .mutableContainers) as! NSMutableDictionary;
//        }
//        catch {
//            print("Error : Data not Recieved");
//        }
//       // return jsonFromFetchedData;
//    }
//}















