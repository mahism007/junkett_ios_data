//
//  CommonClassViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 13/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
//import PKHUD
//import Reachability
import CoreData
import SVProgressHUD
//import MBProgressHUD
//import SDWebImage
import SwiftMessageBar
   var baseColor = "295890"
import PUGifLoading
import CoreLocation
class CommonClassViewController:  UIViewController {
    
    
    // Activity Indicator(start)
    
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let label: UITextView = UITextView()
    let active: UIActivityIndicatorView = UIActivityIndicatorView()
    var refresh = UIRefreshControl()
//    func startLoading(){
//
//
//        HUD.show(.progress)
//
//    }
 
     var AppName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        AppName = "Event App"
        self.active.center = self.view.center
        self.active.style = .gray
        self.active.hidesWhenStopped = true
        self.view.addSubview(self.active)
        
        
    }
    
    func randomString(length: Int) -> String {
      let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      return String((0..<length).map{ _ in letters.randomElement()! })
    }
    func linkPDF(urlData : String , title : String){
//           let storyBoard = UIStoryboard(name: "Truck", bundle: nil)
//           let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
//
//           let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
//           guard   let url = URL(string: urlData) else {
//               return
//           }
//           progressWebViewController.disableZoom = true
//           progressWebViewController.url = url
//           progressWebViewController.bypassedSSLHosts = [url.host!]
//           progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
//           progressWebViewController.websiteTitleInNavigationBar = false
//           progressWebViewController.navigationItem.title = title
//           progressWebViewController.leftNavigaionBarItemTypes = [.reload]
//           progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
//        progressWebViewController.modalPresentationStyle = .fullScreen
//           self.present(proNav, animated: true, completion: nil)
       }
    func fetchCityAndCountry(from location: CLLocation, completion: @escaping (_ city: String?, _ country:  String?, _ error: Error?) -> ()) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            completion(placemarks?.first?.locality,
                       placemarks?.first?.name,
                       error)
        }
    }
    func loadImage(strImage : String,imgView : UIImageView , imgNull : String){
//        let urlString = strImage
//        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        if let url = NSURL(string: urlShow!) {
//
//            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
//                (receivedSize :Int, ExpectedSize :Int) in
//
//            }, completed: {
//                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
//                if(image != nil) {
//
//                    imgView.image = image
//
//
//                }else{
//
//                    imgView.image = UIImage(named: imgNull)
//
//                }
//            })
//        }else{
//
//            imgView.image = UIImage(named: imgNull)
//
//        }
    }
    func getName()-> String{
        var name = String()
        
        let fname = UserDefaults.standard.string(forKey: "FirstName")
        let Mname = UserDefaults.standard.string(forKey: "MiddleName")
        let Lname = UserDefaults.standard.string(forKey: "Lastname")
        
        
        if(Mname == ""){
            name = fname! + " " + Lname!
        }else{
            name = fname! + " " + Mname!  + " " + Lname!
        }
        return name
    }
    /// profile Db
    var Profile_AuthKeyProfile = String()
    // var reachabltyProfile = Reachability()!
    @objc func profileDetailsData(){
 
//        let   Profile_AuthKey : String = (UserDefaults.standard.string(forKey: "Profile_AuthKey")!)
//        let   Email : String = (UserDefaults.standard.string(forKey: "Email")!)
//        let parameters = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
//                            "Email_ID": Email,
//                          "Version": versionAppData,
//                          "AuthKey": Profile_AuthKey]
//        print(parameters)
//        print(URLConstants.GetProfile)
//        WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetProfile, parameters: parameters, successHandler: { (response:[String : AnyObject]) in
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
//
//                let respon = response["response"] as! [String:AnyObject]
//
//                if self.reachabltyProfile.connection != .none{
//
//                    print(respon)
//                    if respon["status"] as! String == "success" {
//
//                        self.deleteProfileData()
//
//                        if let data = response["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [ProfileDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = ProfileDataModel()
//                                    object.setDataInModel(dict: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//
//
//
//
//                        }
//
//
//
//
//
//                        //self.saveProfileData(Employee_Name: Employee_Name as! String, SBU_ID: String(SBU_ID), SBU_Name: SBU_Name as! String, Department: String(Department), Department_Name: Department_Name as! String, Designation:  String(Designation), Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Designation_Name: Designation_Name as! String)
//
//
//
//
//
//
//
//
//
//
//                    }
//                }
//            }
//
//        }) { (err) in
//            print(err.description)
//        }
//
    }
    
    
    
    
    //Core Data
    
    
    func deleteProfileData()
    {
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Profile")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    
    
    
    
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    func startLoading(view:UIView){
        
        
        active.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2 - 100)
        active.style = .gray
        active.startAnimating()
        self.active.isHidden = false
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        view.addSubview(active)
        
        
    }
    func fontDataSemiBold() -> UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: .semibold)
    }
    
    func fontDataBold() -> UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium)
    }
    
    func stopLoading(view:UIView){
        
        
        active.stopAnimating()
        self.active.isHidden = true
        
    }
    let loading = PUGIFLoading()
        func startLoading(){

          //  loading.showOverlay("Loading", gifimagename: "loader")
            
//            SVProgressHUD.show()
//           // SVProgressHUD.setBackgroundColor(UIColor(hexString: baseColor, alpha: 1.0)!)
//            SVProgressHUD.setForegroundColor(UIColor(hexString: baseColor, alpha: 1.0))
//
//
        }
        
        func stopLoading(){
    //        GIFHUD.shared.dismiss()
    //        if( GIFHUD.shared.isShowing == true){
    //            GIFHUD.shared.dismiss()
    //
    //        }
         //   SVProgressHUD.dismiss()
    //        gif.dismiss()
    //
    //         gif.removeFromSuperview()
            
            loading.hide()
            
        }
        
        
        func startLoadingPK(view : UIView){
            
            //MBHud.show(animated: true)
            //         view.addSubview(MBHud)
          //  HUD.show(.labeledRotatingImage(image: UIImage(named: "loading"), title: "Processing", subtitle: ""))
           
    //       gif.setGif(named: "loaderData.gif")
    //        gif.show(withOverlay: true)
            SVProgressHUD.setDefaultMaskType(.black)
            SVProgressHUD.show()
                  // SVProgressHUD.setBackgroundColor(UIColor(hexString: baseColor, alpha: 1.0)!)
            SVProgressHUD.setForegroundColor(UIColor(hexString: baseColor, alpha: 1.0))
        }
        func stopLoadingPK(view : UIView){
            
    //       gif.dismiss()
    //        gif.removeFromSuperview()
             SVProgressHUD.dismiss()
        }
//    var MBHud = MBProgressHUD(frame: CGRect(x: 0, y: -100, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height + 100))
//    var pkHud = PKHUD()
//    func startLoadingPK(view : UIView){
//
////MBHud.show(animated: true)
////         view.addSubview(MBHud)
//       HUD.show(.labeledRotatingImage(image: UIImage(named: "loading"), title: "Processing", subtitle: ""))
//
//
//    }
//    func stopLoadingPK(view : UIView){
//
//       // MBHud.hide(animated: true)
//      HUD.hide()
//
//    }
    
    func startLoading(TB: UITableView){
        
        
        active.center = TB.center
        active.style = .gray
        active.startAnimating()
        active.isUserInteractionEnabled = false
        active.hidesWhenStopped = true
        TB.addSubview(active)
        
    }
    
//    func stopLoading(){
//        HUD.hide()
//
//
//    }
    
     func verifyUrl (urlString: String?) -> Bool {
           //Check for nil
           if let urlString = urlString {
               // create NSURL instance
               if let url = NSURL(string: urlString) {
                   // check if your application can open the NSURL instance
                return UIApplication.shared.canOpenURL(url as URL)
               }
           }
           return false
       }

    
    /// No Internet Image(Start)
    func noInternet()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "noReach")
        
        self.view.addSubview(image)
    }
    /// (end)
    
    
    
    /// No Data Image(Start)
    func noData()
    {
        let image: UIImageView = UIImageView()
        image.frame = CGRect.init(x: 20, y: 200, width: self.view.frame.width - 50, height: 250)
        image.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleLeftMargin, . flexibleRightMargin]
        
        image.center = self.view.center
        
        image.layer.cornerRadius = 10.0
        
        image.image = UIImage.init(named: "no_data")
        
        self.view.addSubview(image)
    }
    ///(end)
    
    
    /// No Data Label(Start)
    func noDataLabel(text: String)
    {
        
        
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 90)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = text
       label.isEditable = false
        label.isSelectable = false;
        label.isScrollEnabled = false
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        label.backgroundColor = UIColor.clear
        self.view.addSubview(label)
        
    }
    ///(end)
    
    func noInternetLabel()
    {
        
        let label: UILabel = UILabel()
        label.frame = CGRect.init(x: 30, y: 20, width: self.view.frame.width - 20, height: 30)
        label.center = self.view.center
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "No internet connection found. Check your internet connection or try again."
        label.numberOfLines = 2
        label.sizeToFit()
        label.textAlignment = .center
        label.autoresizingMask = [.flexibleTopMargin, .flexibleBottomMargin, .flexibleWidth]
        
        self.view.addSubview(label)
        
    }
    
    func errorChecking(error: Error)
    {
        let errorStr = error.localizedDescription
        
        if error._code == NSURLErrorTimedOut {
            
            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
            
            // obj.stopLoading()
            
            
        }
        
        if("\(error._code)" == "-1003")
        {
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
        }
        else
        {
            
            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Again", buttonName: "OK")
            
        }
        
        
    }
    
    
    ///show alert for only display information
    func showSingleButtonAlertWithoutAction (title:String) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    ///show alert with Single Button action
    func showSingleButtonAlertWithAction (title:String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleButtonAlertWithActionMessage (title:String,message : String,buttonTitle:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with right action button
    func showTwoButtonAlertWithRightAction (title:String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with left action button
    func showTwoButtonAlertWithLeftAction (title:String,message : String,buttonTitleLeft:String,buttonTitleRight:String,completionHandler:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: { action in
            completionHandler()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    ///show alert with two action button
    func showTwoButtonAlertWithTwoAction(title:String,message : String,buttonTitleLeft:String,buttonTitleRight:String,completionHandlerLeft:@escaping () -> (),completionHandlerRight:@escaping () -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonTitleLeft, style: UIAlertAction.Style.default, handler: { action in
            completionHandlerLeft()
        }))
        alert.addAction(UIAlertAction(title: buttonTitleRight, style: UIAlertAction.Style.default, handler: { action in
            completionHandlerRight()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func showSingleButtonWithMessage(title:String,message: String, buttonName: String)
    {
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: buttonName, style: UIAlertAction.Style.default, handler:nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
//    func errorChecking(error: Error)
//    {
//        let errorStr = error.localizedDescription
//
//        if error._code == NSURLErrorTimedOut {
//
//            self.showSingleButtonWithMessage(title: "TimeOut!", message: errorStr, buttonName: "OK")
//
//
//
//        }
//
//        if("\(error._code)" == "-1003")
//        {
//            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
//
//        }
//        else
//        {
//
//            self.showSingleButtonWithMessage(title: "Oops!", message: "something went wrong. Try Agin", buttonName: "OK")
//
//        }
//
//
//    }
    
    func checkTextIsEmpty(text: String) -> Bool
    {
        
        if(text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty)
        {
            return true
        }
        else
        {
            return false
        }
        
        
    }
    
    
    
    func noInternetShow()
    {
        self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
    }
    
    
    func alert (title:String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            self.stopLoadingWithoutTouch()
            let alertController = UIAlertController(title: nil, message: title, preferredStyle: .alert)
            alertController.view.tintColor = UIColor.black
            self.present(alertController, animated: true, completion: nil)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {() -> Void in
                alertController.dismiss(animated: true, completion: {() -> Void in
                    
                    
                })
            })
            
        }
    }
    private var uuid: UUID?
    @objc  func showSuccess(strMessage : String) {
          let message = strMessage
          uuid = SwiftMessageBar.showMessage(withTitle: nil, message: message, type: .success, duration: 3, dismiss: true) {
              print("Dismiss callback")
          }
      }
      
      @objc  func showError(strMessage : String) {
          let message = strMessage
          uuid = SwiftMessageBar.showMessage(withTitle: "", message: message, type: .error, duration: 3, dismiss: true) {
              print("Dismiss callback")
          }
      }
      
      @objc func showInfo(strMessage : String) {
          let message = strMessage
          uuid = SwiftMessageBar.showMessage(withTitle: "Info", message: message, type: .info, duration: 3) {
              print("Dismiss callback")
          }
      }
      
      @objc  func clearAll() {
          SwiftMessageBar.sharedMessageBar.cancelAll(force: true)
          uuid = nil
      }
      
      @objc  func clearCurrent() {
          if let id = uuid {
              SwiftMessageBar.sharedMessageBar.cancel(withId: id)
              uuid = nil
          }
      }
    func showToast(message : String) {
        
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width-20, height: 35))
        //toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.blue
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func stopLoadingWithoutTouch()
    {
        self.active.stopAnimating()
        self.active.isUserInteractionEnabled = true
        //self.active.removeFromSuperview()
        
    }
//    var reachability = Reachability()!
//    func internetChecking(isAlert: Bool) -> Bool
//    {
//        if(self.reachability.connection != .none)
//        {
//            return true
//        }
//        else{
//
//            if(isAlert == true)
//            {
//                self.noInternetShow()
//            }
//            else
//            {
//                self.noDataLabel(text: "No internet connection avaliable. Please check your internet connection and try again!")
//            }
//
//
//            return false
//
//        }
//
//
//
//    }
    func mydateFormat(dateFormat : String , dateFormatted : String , strDate : String) -> String {
           let dateFormatter = DateFormatter()
           dateFormatter.timeZone = NSTimeZone.system
           print(strDate)
           dateFormatter.dateFormat = dateFormat
           let date : Date = dateFormatter.date(from: strDate)!
           dateFormatter.string(from: date)
           let dateFormatter2 = DateFormatter()
           dateFormatter2.timeZone = NSTimeZone.system
           dateFormatter2.dateFormat = dateFormatted
           let finalDate = dateFormatter2.string(from: date)
           return finalDate
       }
    func dateFormatChange(inputDateStr:String,inputFormat:String,outputFromat:String) -> String
    {
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = inputFormat

        let resultDate: Date = dateFormatter.date(from: inputDateStr)!

        dateFormatter.dateFormat = outputFromat

        let resultDateStr = dateFormatter.string(from: resultDate)

        if(Debug.show_debug == true)
        {
            print("resultDate",resultDateStr)
        }

        
        return resultDateStr
        
        
    }
    func downloadPDF(linkString:String,linkName:String,completionHandler:@escaping () -> ())
    {
        
        
        
            
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
            let destinationFileUrl = documentsUrl.appendingPathComponent(linkName)
            print(linkString)
        let linkStringData = linkString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            //Create URL to the source file you want to download
        let fileURL = URL(string:linkStringData!)
            
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig)
            print(fileURL!)
            let request = URLRequest(url:fileURL!)
            
            let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                if let tempLocalUrl = tempLocalUrl, error == nil {
                    // Success
                    if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                        print(tempLocalUrl)
                        
                        print("Successfully downloaded. Status code: \(statusCode)")
                    }
                    
                    do {
                        try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    } catch (let writeError) {
                        print("Error creating a file \(destinationFileUrl) : \(writeError)")
                    }
                    
                    completionHandler()
                    
                    
                } else {
                    //print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as? Error);
                }
            }
            task.resume()
            
            
        
        
    }
    
    
    func checkPDFIsAvailable(linkName:String,  success:@escaping (String) -> Void,failure: @escaping () -> ())
    {
        
        
        print("linkName",linkName)
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        let filePath = url.appendingPathComponent(linkName)?.path
        let fileManager = FileManager.default
        
        //        cell.pdfFilepath = filePath!
        
        let pdfFilepath = filePath!
        print(filePath!)
        if fileManager.fileExists(atPath: filePath!) {
            
            print("FILE AVAILABLE")
            
            
            success(pdfFilepath)
            
            
        } else {
            
            print("FILE NOT AVAILABLE")
            
            failure()
            
            
            
        }
        
    }
    
    
}
class LoginDataModel: NSObject {
   
    var CustomerID = String()
    var CustomerCode = String()
    var CustomerName = String()
    
    var Mobile = String()
    var Email = String()
  
    var ID = String()
  
    var Name = String()
    var Employee_ID = String()
    var Company_Name = String()
    var Company_ID = String()
    var Login_Type = String()
    var IsHide = String()
    var Authkey = String()
   var Profile_Pic = String()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
         
        if str["ID"] is NSNull  || str["ID"] == nil{
            ID = ""
        }else{
           
            let empname2 = str["ID"]
            self.ID = (empname2?.description)!
             UserDefaults.standard.set(ID, forKey: "IDData")
            
        }
        
        if str["CustomerCode"] is NSNull  || str["CustomerCode"] == nil{
            CustomerCode = ""
        }else{
          
            let empname2 = str["CustomerCode"]
            self.CustomerCode = (empname2?.description)!
            UserDefaults.standard.set(CustomerCode, forKey: "CustomerCode")
        }
        
        
        if str["CustomerName"] is NSNull  || str["CustomerName"] == nil{
            CustomerName = ""
        }else{
          
            let empname2 = str["CustomerName"]
            self.CustomerName = (empname2?.description)!
        }
        
        if str["Mobile"] is NSNull  || str["Mobile"] == nil{
            Mobile = ""
        }else{

            let empname2 = str["Mobile"]
            self.Mobile = (empname2?.description)!
        }
        
        if str["Email_ID"] is NSNull  || str["Email_ID"] == nil{
            Email = ""
        }else{
         
            let empname2 = str["Email_ID"]
            self.Email = (empname2?.description)!
        }
       
      
       
       


        UserDefaults.standard.set(Email, forKey: "Email")
        UserDefaults.standard.set(Name, forKey: "FirstName")
        UserDefaults.standard.set(Mobile, forKey: "Mobile")
        
    }
    
}
class LoginModel: NSObject {
   
    var CustomerID = String()
    var CustomerCode = String()
    var CustomerName = String()
    
    var Mobile = String()
    var Email = String()
  
    var ID = String()
  
    var Name = String()
    var Employee_ID = String()
    var Company_Name = String()
    var Company_ID = String()
    var Login_Type = String()
    var IsHide = String()
    var Authkey = String()
   var Profile_Pic = String()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["ID"] is NSNull  || str["ID"] == nil{
            ID = ""
        }else{
           
            let empname2 = str["ID"]
            self.ID = (empname2?.description)!
             
            
        }
        if str["CustomerID"] is NSNull  || str["CustomerID"] == nil{
            CustomerID = ""
        }else{
           
            let empname2 = str["CustomerID"]
            self.CustomerID = (empname2?.description)!
            
             
        }
        if str["CustomerCode"] is NSNull  || str["CustomerCode"] == nil{
            CustomerCode = ""
        }else{
          
            let empname2 = str["CustomerCode"]
            self.CustomerCode = (empname2?.description)!
            UserDefaults.standard.set(CustomerID, forKey: "IDData")
        }
        
        
        if str["CustomerName"] is NSNull  || str["CustomerName"] == nil{
            CustomerName = ""
        }else{
          
            let empname2 = str["CustomerName"]
            self.CustomerName = (empname2?.description)!
        }
        
        if str["Mobile"] is NSNull  || str["Mobile"] == nil{
            Mobile = ""
        }else{

            let empname2 = str["Mobile"]
            self.Mobile = (empname2?.description)!
        }
        
        if str["Email_ID"] is NSNull  || str["Email_ID"] == nil{
            Email = ""
        }else{
         
            let empname2 = str["Email_ID"]
            self.Email = (empname2?.description)!
        }
       
      
       
       


        UserDefaults.standard.set(Email, forKey: "Email")
        UserDefaults.standard.set(Name, forKey: "FirstName")
        UserDefaults.standard.set(Mobile, forKey: "Mobile")
        
    }
    
}

class LoginSubChaModel: NSObject {
   
    var CustomerID = String()
    var CustomerCode = String()
    var CustomerName = String()
    
    var Mobile = String()
    var Email = String()
  
    var ID = String()
  
    var Name = String()
    var Employee_ID = String()
    var Company_Name = String()
    var Company_ID = String()
    var Login_Type = String()
    var IsHide = String()
    var Authkey = String()
   var Profile_Pic = String()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["ID"] is NSNull  || str["ID"] == nil{
            ID = ""
        }else{
           
            let empname2 = str["ID"]
            self.ID = (empname2?.description)!
             
            
        }
        if str["CustomerID"] is NSNull  || str["CustomerID"] == nil{
            CustomerID = ""
        }else{
           
            let empname2 = str["CustomerID"]
            self.CustomerID = (empname2?.description)!
            UserDefaults.standard.set(CustomerID, forKey: "IDData")
             
        }
        if str["CustomerCode"] is NSNull  || str["CustomerCode"] == nil{
            CustomerCode = ""
        }else{
          
            let empname2 = str["CustomerCode"]
            self.CustomerCode = (empname2?.description)!
            
        }
        
        
        if str["CustomerName"] is NSNull  || str["CustomerName"] == nil{
            CustomerName = ""
        }else{
          
            let empname2 = str["CustomerName"]
            self.CustomerName = (empname2?.description)!
        }
        
        if str["Mobile"] is NSNull  || str["Mobile"] == nil{
            Mobile = ""
        }else{

            let empname2 = str["Mobile"]
            self.Mobile = (empname2?.description)!
        }
        
        if str["Email_ID"] is NSNull  || str["Email_ID"] == nil{
            Email = ""
        }else{
         
            let empname2 = str["Email_ID"]
            self.Email = (empname2?.description)!
        }
       
      
       
       


        UserDefaults.standard.set(Email, forKey: "Email")
        UserDefaults.standard.set(Name, forKey: "FirstName")
        UserDefaults.standard.set(Mobile, forKey: "Mobile")
        
    }
    
}
public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}

//import Typesetter
//enum MyFont: String, TypesetterFont {
//   case Regular
//   case Bold
//
//   var name: String {
//       switch self {
//       case .Regular:
//           return "Helvetica"
//       case .Bold:
//           return "Helvetica-Bold"
//       }
//   }
//}

//@IBDesignable class MyLabel: UILabel {
//
//    private let typesetter = Typesetter(bundle: Bundle(for: MyLabel.self))
//
//    @IBInspectable var textStyle: String = MyFont.Regular.rawValue {
//        didSet {
//            if(textStyle == "Regular" || textStyle == "Bold"){
//            setupLabel()
//            }
//        }
//    }
//    @IBInspectable var fontSize: CGFloat = 16.0 {
//        didSet {
//            setupLabel()
//        }
//    }
//    @IBInspectable var fontName: String = MyFont.Regular.rawValue {
//        didSet {
//            setupLabel()
//        }
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupLabel()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setupLabel()
//    }
//
//    private func setupLabel() {
//        guard let font = MyFont(rawValue: fontName) else { return }
//        var fontNameData = String()
//        print(textStyle)
//        if(textStyle == "Bold"){
//         self.font = UIFont(name: "Helvetica-Bold", size: fontSize)
//        }else{
//          self.font = UIFont(name: "Helvetica", size: fontSize)
//        }
//        // self.font = UIFont(name: fontNameData, size: fontSize)
//    }
//
//}
//
//
//@IBDesignable class MyTextView: UITextView {
//
//    private let typesetter = Typesetter(bundle: Bundle(for: MyTextView.self))
//
//    @IBInspectable var textStyle: String = MyFont.Regular.rawValue {
//        didSet {
//            if(textStyle == "Regular" || textStyle == "Bold"){
//            setupLabel()
//            }
//        }
//    }
//    @IBInspectable var fontSize: CGFloat = 16.0 {
//        didSet {
//            setupLabel()
//        }
//    }
//    @IBInspectable var fontName: String = MyFont.Regular.rawValue {
//        didSet {
//            setupLabel()
//        }
//    }
//
////    override init(frame: CGRect) {
////        super.init(frame: frame)
////        setupLabel()
////    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setupLabel()
//    }
//
//    private func setupLabel() {
//        guard let font = MyFont(rawValue: fontName) else { return }
//        var fontNameData = String()
//        print(textStyle)
//        if(textStyle == "Bold"){
//         self.font = UIFont(name: "Helvetica-Bold", size: fontSize)
//        }else{
//          self.font = UIFont(name: "Helvetica", size: fontSize)
//        }
//        // self.font = UIFont(name: fontNameData, size: fontSize)
//    }
//
//}
class MessageCallServerModel : NSObject {
    var message = String("No Data Found !")
    func sealizeMessage(cell : [String:AnyObject]) -> String {
        
        
        
        let message1 = cell["Msg"]
        let message2 = cell["message"]
        let message3 = cell["msg"]
        let message4 = cell["Message"]
        let message5 = cell["DisplayMessage"]
        if message1 != nil {
            message = (message1?.description)!
        }
        if  message2 != nil {
            message = (message2?.description)!
        }
        if  message3 != nil {
            message = (message3?.description)!
        }
        if  message4 != nil {
            message = (message4?.description)!
        }
        if  message5 != nil {
            message = (message5?.description)!
        }
        return message
    }
}
class MessageCallServerModel1 : NSObject {
    var message = String("No Data Found !")
    func sealizeMessage(cell : [String:AnyObject]) -> String {
        
        
        
        let message1 = cell["DisplayMessage"]
        
        if message1 != nil {
            message = (message1?.description)!
        }
        
        
        return message
    }
}
class SuccessServerModel : NSObject {
    var message = String("success")
    func sealizeMessage(cell : [String:AnyObject]) -> String {
        
        
        
        let message1 = cell["status"]
        
        
        if message1 != nil {
            message = (message1?.description)!
        }
        
        
        return message
    }
}
class ProfileDataModel: NSObject {
    
    
    var Employee_Name = String()
    var Employee_ID = String()
    var Gender = String()
    var Email_ID_Official = String()
    var Profile_Pic_Path = String()
    var Mobile_Number = String()
    
    var Zone_Name = String()
    var Unit_Name = String()
    var Area_Name = String()
    var Designation = String()
    var Designation_Name = String()
    var Company_Name = String()
    
    
    
    
    
    
    
    
    func setDataInModel(dict:[String:AnyObject])
    {
        if dict["Company_Name"] is NSNull || dict["Company_Name"] == nil{
            Company_Name = ""
        }else{
            Company_Name = (dict["Company_Name"] as? String)!
            
        }
        if dict["Employee_ID"] is NSNull || dict["Employee_ID"] == nil{
            Employee_ID = ""
        }else{
            Employee_ID = (dict["Employee_ID"] as? String)!
            
        }
        
        
        if dict["Employee_Name"] is NSNull || dict["Employee_Name"] == nil{
            Employee_Name = ""
        }else{
            Employee_Name = (dict["Employee_Name"] as? String)!
            UserDefaults.standard.set(Employee_Name, forKey: "empName")
        }
        
        if dict["Gender"] is NSNull || dict["Gender"] == nil{
            Gender = ""
        }else{
            Gender = (dict["Gender"] as? String)!
        }
        if dict["Email_ID"] is NSNull || dict["Email_ID"] == nil{
            Email_ID_Official = ""
        }else{
            Email_ID_Official = (dict["Email_ID"] as? String)!
        }
        if dict["Profile_Pic_Path"] is NSNull || dict["Profile_Pic_Path"] == nil{
            Profile_Pic_Path = ""
        }else{
            Profile_Pic_Path = (dict["Profile_Pic_Path"] as? String)!
        }
        if dict["Mobile_Number"] is NSNull || dict["Mobile_Number"] == nil{
            Mobile_Number = ""
        }else{
            Mobile_Number = (dict["Mobile_Number"] as? String)!
            UserDefaults.standard.set(Mobile_Number, forKey: "mobileData")
        }
        if dict["Designation_Name"] is NSNull || dict["Designation_Name"] == nil{
            Designation_Name = ""
        }else{
            Designation_Name = dict["Designation_Name"] as! String
        }
        
        if dict["Designation"] is NSNull || dict["Designation"] == nil{
            Designation = ""
        }else{
            Designation = dict["Designation"] as! String
        }
        
        if dict["Company_Name"] is NSNull || dict["Company_Name"] == nil{
            Area_Name = ""
        }else{
            Area_Name = dict["Company_Name"] as! String
        }
        if dict["Plant_Name"] is NSNull || dict["Plant_Name"] == nil{
            Zone_Name = ""
        }else{
            Zone_Name = dict["Plant_Name"] as! String
        }
        
        if dict["Unit_Name"] is NSNull || dict["Unit_Name"] == nil{
            Unit_Name = ""
        }else{
            Unit_Name = dict["Unit_Name"] as! String
        }
        
        print(Mobile_Number)
        
        self.saveProfileData(Employee_Name: Employee_Name as! String, Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Zone_Name: Zone_Name, Unit_Name: Unit_Name, Area_Name: Area_Name, Designation: Designation,Employee_Id :Employee_ID,Designation_Name : Designation_Name)
        
    }
    func saveProfileData (Employee_Name:String,Gender:String,Email_ID_Official:String,Profile_Pic_Path:String,Mobile_Number:String,Zone_Name:String,Unit_Name:String,Area_Name:String,Designation:String,Employee_Id:String,Designation_Name : String){
        
//        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
//
//        let tasks = Profile(context: context)
//
//        tasks.employee_Name = Employee_Name
//        //         tasks.sBU_ID = SBU_ID
//        //         tasks.sBU_Name = SBU_Name
//        //         tasks.department = Department
//        //         tasks.department_Name = Department_Name
//        //         tasks.designation = Designation
//        tasks.gender = Gender
//        tasks.mobile_Number = Mobile_Number
//        tasks.email_ID_Official = Email_ID_Official
//        tasks.profile_Pic_Path = Profile_Pic_Path
//        //tasks.designation_Name = Designation_Name
//        tasks.employeeId = Employee_Id
//        tasks.zone_Name = Zone_Name
//        tasks.unit_Name = Unit_Name
//        tasks.area_Name = Area_Name
//        tasks.designation = Designation
//        tasks.designation_Name = Designation_Name
//
//        (UIApplication.shared.delegate as! AppDelegate).saveContext()
//
//
        
        
    }
    
}


