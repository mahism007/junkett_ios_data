//
//  NoDataTableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 02/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class NoDataTableViewCell: UITableViewCell {

    @IBOutlet weak var imgNoData: UIImageView!
    @IBOutlet weak var textViewNoData: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
