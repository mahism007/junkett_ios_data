//
//  UpdateLocationViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 04/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import EzPopup
import GooglePlaces

class UpdateLocationViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource ,CLLocationManagerDelegate,GMSMapViewDelegate{


    
    @IBOutlet weak var tableView: UITableView!
    var marker = GMSMarker()
    var refreshControl = UIRefreshControl()
    let locationManager = CLLocationManager()
private var placesClient: GMSPlacesClient!
    override func viewDidLoad() {
        super.viewDidLoad()
        
       placesClient = GMSPlacesClient.shared()
    
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()


           
        }



    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            self.tabBarController?.tabBar.isHidden = true
              self.navigationController?.navigationBar.isHidden = false
        self.title = "Update Location"

                
                
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.view.backgroundColor = .clear
       
        self.navigationController?.navigationBar.tintColor = .black
        if(MoveStruct.action == true){
            MoveStruct.action = false
            self.tableView.reloadData()
            
        }
    }
    
   
    @IBAction func btnCreateClicked(_ sender: Any) {
        
        self.submitData()
  
      
    }
    // Present the Autocomplete view controller when the button is pressed.
    @IBAction func autocompleteClicked(_ sender: UIButton) {
      let autocompleteController = GMSAutocompleteViewController()
      autocompleteController.delegate = self

      // Specify the place data types to return.
      let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
        UInt(GMSPlaceField.placeID.rawValue))!
      autocompleteController.placeFields = fields

      // Specify a filter.
      let filter = GMSAutocompleteFilter()
      filter.type = .address
      autocompleteController.autocompleteFilter = filter

      // Display the autocomplete view controller.
      present(autocompleteController, animated: true, completion: nil)
    }

    // Add a button to the view.
    func makeButton() {
      let btnLaunchAc = UIButton(frame: CGRect(x: 5, y: 150, width: 300, height: 35))
      btnLaunchAc.backgroundColor = .blue
      btnLaunchAc.setTitle("Launch autocomplete", for: .normal)
      btnLaunchAc.addTarget(self, action: #selector(autocompleteClicked), for: .touchUpInside)
      self.view.addSubview(btnLaunchAc)
    }
    func submitData(){

           self.startLoadingPK(view: self.view)
           
           DispatchQueue.global(qos: .background).async {
           

           let parameter = [
    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
      "UserID": UserDefaults.standard.string(forKey: "UserID")!,

                 "Latitude":self.lat,
                 "Longitude":self.long,
                 "Address":self.currentAddress


             
           ] as [String : Any]
           print(parameter)
             WebServices.sharedInstances.sendPostRequest(url: URLConstants.UpdateCurrentLocation, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                 
                 if let response = response["response"]{
                  
                     let objectmsg = MessageCallServerModel1()
                     
                     let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      let objSucess = SuccessServerModel()
                       let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                     if(statusStringsuccess == "200"){
                     
                    self.stopLoadingPK(view: self.view)
                    self.showSuccess(strMessage: "Location Updated Successfully.")
                    self.navigationController?.popToRootViewController(animated: true)
                     
                     
                 }else{
                      self.stopLoadingPK(view: self.view)
                     self.showError(strMessage: msg)
                 }
                }
                 
             }) { (err) in
                self.stopLoadingPK(view: self.view)
                 print(err.description)
             }

           }
     }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Mark: Marker methods
     func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
//         print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
//         reverseGeocoding(marker: marker)
//     print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
     }
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
         print("didBeginDragging")
     }
     func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
         print("didDrag")
     }
    var lat = String()
    var long = String()
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let locValue:CLLocationCoordinate2D = manager.location!.coordinate
         print("locations = \(locValue.latitude) \(locValue.longitude)")
//        lat = locValue.latitude.description
//        long = locValue.longitude.description
      //  marker.position = locValue
        
        
           // reverseGeocoding(marker: marker)
     }
     func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
      //  print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
         
         marker.position = position.target
         
         
             reverseGeocoding(marker: marker)
         print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
        lat = marker.position.latitude.description
        long = marker.position.longitude.description
     }
     //Mark: Reverse GeoCoding
     var currentAddress = String()
     func reverseGeocoding(marker: GMSMarker) {
         let geocoder = GMSGeocoder()
         let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
         
         
         
         geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
             if let address = response?.firstResult() {
                 let lines = address.lines! as [String]
                 
                 print("Response is = \(address)")
                 print("Response is = \(lines)")
                 let gmsAddress: GMSAddress = address
                 print("\ncoordinate.latitude=\(gmsAddress.coordinate.latitude)")
                 print("coordinate.longitude=\(gmsAddress.coordinate.longitude)")
                 print("thoroughfare=\(gmsAddress.thoroughfare)")
                 print("locality=\(gmsAddress.locality)")
                 print("subLocality=\(gmsAddress.subLocality)")
                 print("administrativeArea=\(gmsAddress.administrativeArea)")
                 print("postalCode=\(gmsAddress.postalCode)")
                 print("country=\(gmsAddress.country)")
                 print("lines=\(gmsAddress.lines)")
                self.currentAddress = lines.joined(separator: "\n")
                if(gmsAddress.locality != nil){
                    self.city = gmsAddress.locality!
                }
                if(gmsAddress.administrativeArea != nil){
                    self.state = gmsAddress.administrativeArea!
                }
                if(gmsAddress.postalCode != nil){
                    self.postal = gmsAddress.postalCode!
                }
                if(gmsAddress.country != nil){
                    self.country = gmsAddress.country!
                }

               // self.view.makeToastHZL(self.currentAddress)
                 
             }
            self.cellDataLocation.textViewLocation.text = self.currentAddress
            marker.title = self.currentAddress
            marker.map = self.cellDataLocation.mapView
         }
     }
    var city = String()
    var country = String()
    var postal = String()
    var state = String()

    var cellDataLocation : UpdateLocationTableViewCell!
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLocation", for: indexPath) as! UpdateLocationTableViewCell
            cellDataLocation = cell
            self.locationManager.requestAlwaysAuthorization()

            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()

            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
                let camera = GMSCameraPosition.camera(withLatitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!, zoom: 15.0)
                cell.mapView.camera = camera
                
                marker.position = CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
                marker.isDraggable = true
                marker.map = cell.mapView
                cell.mapView.delegate = self
                marker.isDraggable = true
                reverseGeocoding(marker: marker)
                marker.map = cell.mapView
                marker.icon = UIImage(named: "pin")
                cell.mapView.delegate = self
            }
            

            return cell
        }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! ButtonUpTableViewCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
          if(indexPath.section == 0){
            return UITableView.automaticDimension
          }else{

              return 60
          }

        
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UpdateLocationViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name)")
    print("Place ID: \(place.placeID)")
    print("Place attributions: \(place.attributions)")
    dismiss(animated: true, completion: nil)
  }

  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }

  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    dismiss(animated: true, completion: nil)
  }

  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }

  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }

}
