//
//  CustomMarkerView.swift
//  googlMapTutuorial2
//
//  Created by Muskan on 12/17/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import Foundation
import UIKit

class CustomMarkerView: UIView {
    var img: UIImage!
    var borderColor: UIColor!
    
    init(frame: CGRect, image: UIImage, borderColor: UIColor, tag: Int) {
        super.init(frame: frame)
        self.img=image
        self.borderColor=borderColor
        self.tag = tag
        setupViews()
    }
    
    func setupViews() {
        
        let imgViewAll = UIImageView(image: UIImage(named: "picker"))
        imgViewAll.frame=CGRect(x: 0, y: 0, width: 40, height: 60)
        imgViewAll.clipsToBounds=true
        
        
        
        let imgView = UIImageView(image: img)
        imgView.frame=CGRect(x: 5, y: 5, width: 30, height: 30)
        imgView.layer.cornerRadius = 15

        imgView.clipsToBounds=true

        imgViewAll.addSubview(imgView)
        self.addSubview(imgViewAll)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}









