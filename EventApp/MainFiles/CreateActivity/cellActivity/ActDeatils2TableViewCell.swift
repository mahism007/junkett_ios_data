//
//  ActDeatils2TableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 08/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ActDeatils2TableViewCell: UITableViewCell {
    @IBOutlet weak var imgPro3: UIImageView!
        @IBOutlet weak var imgPro2: UIImageView!
    @IBOutlet weak var imgPro1: UIImageView!
    
    @IBOutlet weak var btnData: UIButton!
    
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var btnShow: UIButton!
    @IBOutlet weak var width1: NSLayoutConstraint!
    @IBOutlet weak var width2: NSLayoutConstraint!
    @IBOutlet weak var width3: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
