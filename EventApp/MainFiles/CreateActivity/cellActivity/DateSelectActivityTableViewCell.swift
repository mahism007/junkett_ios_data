//
//  DateSelectActivityTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 03/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class DateSelectActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var lblRepeat: UILabel!
    @IBOutlet weak var txtEndTime: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtEventName: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
