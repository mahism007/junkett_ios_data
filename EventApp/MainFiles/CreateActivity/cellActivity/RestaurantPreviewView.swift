//
//  RestaurantPreviewView.swift
//  googlMapTutuorial2
//
//  Created by Muskan on 12/17/17.
//  Copyright © 2017 akhil. All rights reserved.
//

import Foundation
import UIKit

class RestaurantPreviewView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor=UIColor.red
        self.clipsToBounds=true
        self.layer.masksToBounds=true
        setupViews()
    }
    
    func setData(data: ActivityNearModel , img : UIImage) {
//        lblTitle.text = title
//        imgView.image = img
//        lblPrice.text = "$\(price)"
        imgView.lblCat.text = data.categoryNames!
              if(data.mateCount! != nil){
        if(data.mateCount == "0" || data.mateCount == "1"){
            imgView.lblMatesCount.text = data.mateCount! + " Mate"
        }else{
            imgView.lblMatesCount.text = data.mateCount! + " Mates"
        }
        }
        let datee = dateFormatChange(inputDateStr: data.startDateTime!, inputFormat: "yyyy-MM-dd'T'hh:mm:ss", outputFromat: "hh:mm a")
        imgView.lblTime.text = datee
        if(data.isRepeated == "0"){
        imgView.lblDay.text = "No Repeat"
        }
        imgView.imgProfile.image = img
        imgView.lblTitle.text = data.title

        imgView.widthCat.constant = imgView.lblCat.intrinsicContentSize.width + 10
         imgView.widthTime.constant = imgView.lblTime.intrinsicContentSize.width + 10
        imgView.widthDay.constant = imgView.lblDay.intrinsicContentSize.width + 10
    }
    func dateFormatChange(inputDateStr:String,inputFormat:String,outputFromat:String) -> String
    {
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = inputFormat

        let resultDate: Date = dateFormatter.date(from: inputDateStr)!

        dateFormatter.dateFormat = outputFromat

        let resultDateStr = dateFormatter.string(from: resultDate)

        if(Debug.show_debug == true)
        {
            print("resultDate",resultDateStr)
        }

        
        return resultDateStr
        
        
    }
    func setupViews() {
        containerView.backgroundColor = .clear
        addSubview(containerView)
        containerView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive=true
        containerView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive=true
        

        
        addSubview(imgView)
        imgView.leftAnchor.constraint(equalTo: leftAnchor).isActive=true
        imgView.topAnchor.constraint(equalTo: topAnchor).isActive=true
        imgView.rightAnchor.constraint(equalTo: rightAnchor).isActive=true
        imgView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive=true
        

    }
    
    let containerView: UIView = {
        let v=UIView()
        v.backgroundColor = .clear
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()
    
    let imgView: ActivityView = {
        let allViewsInXibArray1 = Bundle.main.loadNibNamed("ActivityView", owner: self, options: nil)
        let v = allViewsInXibArray1?.first as! ActivityView
    
        v.translatesAutoresizingMaskIntoConstraints=false
        return v
    }()

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
