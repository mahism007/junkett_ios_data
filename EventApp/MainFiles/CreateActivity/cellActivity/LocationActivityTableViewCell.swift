//
//  LocationActivityTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 03/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import GoogleMaps
class LocationActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtLocation: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
