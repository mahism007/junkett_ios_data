//
//  ActDetailTopTableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 07/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ActDetailTopTableViewCell: UITableViewCell {
    @IBOutlet weak var lblArt: UILabel!
   
    
   
    @IBOutlet weak var btnFavourite: UIButton!
    
    //    @IBOutlet weak var btnActivity: UIButton!
    @IBOutlet weak var viewOverlay: UIView!
    @IBOutlet weak var imgCollect: UIImageView!
    
    
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var widthDate: NSLayoutConstraint!
    

    @IBOutlet weak var widthCategory: NSLayoutConstraint!
    @IBOutlet weak var lblCategory: UILabel!
    
    
    @IBOutlet weak var viewShare: UIView!

    @IBOutlet weak var widthDay: NSLayoutConstraint!
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgLoc: UIImageView!
    @IBOutlet weak var lblLoc: UILabel!
    
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var viewDate: UIView!
 
       @IBOutlet weak var imgPro1: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewDay.layer.cornerRadius = 5
        viewCategory.layer.cornerRadius = 5
        viewDate.layer.cornerRadius = 5
        
        viewShare.layer.cornerRadius = 10
        viewShare.layer.borderWidth = 1.0
        viewShare.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
