//
//  ButtonUpTableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 05/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ButtonUpTableViewCell: UITableViewCell {

    @IBOutlet weak var btnUpdate: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnUpdate.clipsToBounds = true
        btnUpdate.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
