//
//  CreateActivityViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 03/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import EzPopup
import ImagePicker
import DateTimePicker
import SDWebImage
class CreateActivityViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource ,CLLocationManagerDelegate,GMSMapViewDelegate,ImagePickerDelegate,DateTimePickerDelegate{

    @IBOutlet weak var widthImg2: NSLayoutConstraint!
    
    @IBOutlet weak var imgCat2: UIImageView!
    @IBOutlet weak var imgCat1: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    var marker = GMSMarker()
    var refreshControl = UIRefreshControl()
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(catSelect.count > 0){
            marker.icon = UIImage(named: "pin")
            
            if(catSelect.count == 1){
                   let urlShow = catSelect[0].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                if let url = NSURL(string: urlShow!) {
                 imgCat1.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
                   // imgCat1.roundCorners([.bottomRight], radius: 20)
                   // imgCat2.isHidden = true
                    widthImg2.constant = 0
                    self.title = catSelect[0].Name
                }
            }else {
                 imgCat2.isHidden = false
                widthImg2.constant = 100
                  let urlShow1 = catSelect[0].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
               if let url1 = NSURL(string: urlShow1!) {
                imgCat1.sd_setImage(with: url1 as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
                }
                   let urlShow2 = catSelect[1].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                if let url2 = NSURL(string: urlShow2!) {
                 imgCat2.sd_setImage(with: url2 as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
                    imgCat1.roundCorners([.bottomRight], radius: 20)
                    imgCat2.roundCorners([.bottomRight], radius: 20)
                    self.title = catSelect[1].Name
                }
                }
            }
        
         print(UserDefaults.standard.string(forKey: "SessionID")!)
    
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.GenderData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "Gender")), object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.RepeatData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "Repeat")), object: nil)
      
                NotificationCenter.default.addObserver(self, selector: #selector(self.showDatareload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "showDatareload")), object: nil)

           
        }
        @objc func showDatareload(){
            
            cellDateSelect.txtDate.text = RegisterStruct.startDate
            cellDateSelect.txtEndDate.text = RegisterStruct.endDate
          //  cellData.lblInterstedIn.text = interest
        }
    @objc func GenderData(){
        cellOther.lblGender.text = RegisterStruct.gender
        cellOther.lblGender.textColor = .black
       }
    @objc func RepeatData(){
        cellDateSelect.lblRepeat.text = RegisterStruct.repeatType
         
       }
    @IBAction func btnCoverClicked(_ sender: UIButton) {

        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        config.allowMultiplePhotoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.delegate = self

        present(imagePicker, animated: true, completion: nil)
    }

var ageRange = String()

    @IBAction func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
        cellOther.lblAge.text = "Age Range : " + String(Int(rangeSlider.lowerValue * 100)) + "-" + String(Int(rangeSlider.upperValue * 100))
        ageRange = String(Int(rangeSlider.lowerValue * 100)) + "-" + String(Int(rangeSlider.upperValue * 100))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            self.tabBarController?.tabBar.isHidden = true
              self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.topItem?.title = ""

                
                
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.view.backgroundColor = .clear
       
        self.navigationController?.navigationBar.tintColor = .white
        if(MoveStruct.action == true){
            MoveStruct.action = false
            self.tableView.reloadData()
            
        }
    }
    
    func timePick(){
                let min = Date().addingTimeInterval(-60 * 60 * 24 * 4)
                let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
                let picker = DateTimePicker.create(minimumDate: min, maximumDate: max)
                
                // customize your picker
                picker.timeInterval = DateTimePicker.MinuteInterval.ten
        //        picker.locale = Locale(identifier: "en_GB")

                picker.todayButtonTitle = ""
                picker.is12HourFormat = true
                picker.dateFormat = "hh:mm aa"
               picker.isTimePickerOnly = true
                picker.includesMonth = true
                picker.includesSecond = false
                picker.highlightColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
                picker.doneButtonTitle = "!! DONE !!"
                picker.doneBackgroundColor = UIColor(red: 255.0/255.0, green: 138.0/255.0, blue: 138.0/255.0, alpha: 1)
                picker.customFontSetting = DateTimePicker.CustomFontSetting(selectedDateLabelFont: .boldSystemFont(ofSize: 20))
                picker.completionHandler = { date in
                    let formatter = DateFormatter()
                    formatter.dateFormat = "hh:mm aa"
                    self.title = formatter.string(from: date)
                }
                picker.delegate = self

                picker.show()
    }
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        
        if(popInt == 1){
            cellDateSelect.txtTime.text = picker.selectedDateString
        }else{
            cellDateSelect.txtEndTime.text = picker.selectedDateString
        }
    }
    
    @IBAction func btnCreateClicked(_ sender: Any) {
        
        self.submitData()
  
      
    }
    func submitData(){

           self.startLoadingPK(view: self.view)
           
           DispatchQueue.global(qos: .background).async {
           
            let startDate = RegisterStruct.startDate1 + " " + self.cellDateSelect.txtTime.text!
              let endDate = RegisterStruct.endDate1 + " " + self.cellDateSelect.txtEndTime.text!
           let parameter = [
    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
      "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                 "Title":"",
                 "Description":self.cellOther.textViewDesc.text!,
                 "StartDateTime":startDate,
                 "EndDateTime":endDate,
                 "Latitude":self.lat,
                 "Longitude":self.long,
                 "PlaceName":self.placename,
                 "IsRepeated": "0",
                 "RepeateMode":"One Day",
                 "ActivityCategoryCSV":RegisterStruct.categoryList,"Address":self.cellDataLocation.txtLocation.text!,"City":self.city,"State":self.state,"Country":self.country,"Pincode":self.postal,
                 "Gender":RegisterStruct.gender,"AgeRange":self.ageRange

             
           ] as [String : Any]
           print(parameter)
             WebServices.sharedInstances.sendPostRequest(url: URLConstants.ActivityCreate, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                 
                 if let response = response["response"]{
                  
                     let objectmsg = MessageCallServerModel1()
                     
                     let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      let objSucess = SuccessServerModel()
                       let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                     if(statusStringsuccess == "200"){
                     
                    self.stopLoadingPK(view: self.view)
                    self.showSuccess(strMessage: msg)
                    self.navigationController?.popToRootViewController(animated: true)
                     
                     
                 }else{
                      self.stopLoadingPK(view: self.view)
                     self.showError(strMessage: msg)
                 }
                }
                 
             }) { (err) in
                self.stopLoadingPK(view: self.view)
                 print(err.description)
             }

           }
     }
    
    @IBAction func btnRepeatClicked(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "RepeatSelectViewController") as! RepeatSelectViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 300 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnStartDateActivtyClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        ZIVC.popInt = 1
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnEndDateActivtyClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        ZIVC.popInt = 2
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    var startTime = String()
    var endTime = String()
     var popInt = Int()
    @IBAction func btnStartTimeActivtyClicked(_ sender: UIButton) {
        popInt = 1
        timePick()
    }
    @IBAction func btnEndTimeActivtyClicked(_ sender: UIButton) {
        popInt = 2
        timePick()
    }
    @IBAction func btnChooseActivtyClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivitySelectViewController") as! CreateActivitySelectViewController
        mainVC.intShow = 0
        self.navigationController?.pushViewController(mainVC, animated: true)
        
        
        
        
    }
    @IBAction func btnGender(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "GenderSelectViewController") as! GenderSelectViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 300 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //Mark: Marker methods
     func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
//         print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
//         reverseGeocoding(marker: marker)
//     print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
     }
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
         print("didBeginDragging")
     }
     func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
         print("didDrag")
     }
    var lat = String()
    var long = String()
     func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         let locValue:CLLocationCoordinate2D = manager.location!.coordinate
         print("locations = \(locValue.latitude) \(locValue.longitude)")
        lat = locValue.latitude.description
        long = locValue.longitude.description
      //  marker.position = locValue
        
        
           // reverseGeocoding(marker: marker)
     }
     func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
      //  print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
         
         marker.position = position.target
         
         
             reverseGeocoding(marker: marker)
         print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
        lat = marker.position.latitude.description
        long = marker.position.longitude.description
     }
     //Mark: Reverse GeoCoding
     var currentAddress = String()
     func reverseGeocoding(marker: GMSMarker) {
         let geocoder = GMSGeocoder()
         let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
         
         
         
         geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
             if let address = response?.firstResult() {
                 let lines = address.lines! as [String]
                 
                 print("Response is = \(address)")
                 print("Response is = \(lines)")
                 let gmsAddress: GMSAddress = address
                 print("\ncoordinate.latitude=\(gmsAddress.coordinate.latitude)")
                 print("coordinate.longitude=\(gmsAddress.coordinate.longitude)")
                 print("thoroughfare=\(gmsAddress.thoroughfare)")
                 print("locality=\(gmsAddress.locality)")
                 print("subLocality=\(gmsAddress.subLocality)")
                 print("administrativeArea=\(gmsAddress.administrativeArea)")
                 print("postalCode=\(gmsAddress.postalCode)")
                 print("country=\(gmsAddress.country)")
                 print("lines=\(gmsAddress.lines)")
                
                self.currentAddress = lines.joined(separator: "\n")
                if(gmsAddress.thoroughfare != nil){
                    self.placename = gmsAddress.thoroughfare!
                }else if(gmsAddress.subLocality != nil){
                    self.placename = gmsAddress.subLocality!
                }
                
                if(gmsAddress.locality != nil){
                    self.city = gmsAddress.locality!
                }
                if(gmsAddress.administrativeArea != nil){
                    self.state = gmsAddress.administrativeArea!
                }
                if(gmsAddress.postalCode != nil){
                    self.postal = gmsAddress.postalCode!
                }
                if(gmsAddress.country != nil){
                    self.country = gmsAddress.country!
                }
               // self.view.makeToastHZL(self.currentAddress)
                 
             }
            marker.title = self.currentAddress
            marker.map = self.cellDataLocation.mapView
         }
     }
    var placename = String()
    var city = String()
    var country = String()
    var postal = String()
    var state = String()
    var cellDataCover : CoverActivityTableViewCell!
    var cellDateSelect : DateSelectActivityTableViewCell!
    var cellDataLocation : LocationActivityTableViewCell!
    var cellOther : OtherActivityTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1){
            if(selectedActivityModel.count > 0){
                return 2
            }
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCover", for: indexPath) as! CoverActivityTableViewCell
        cellDataCover = cell
      
       
        return cell
        }else if(indexPath.section == 1){
            if(selectedActivityModel.count > 0){
                if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellAct", for: indexPath) as! UITableViewCell
            return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cellSelectActivity", for: indexPath) as! CreateActivitySelectTableViewCell
                       cell.imgMessage.image = UIImage(named: "1")
                             cell.imgMessage.layer.cornerRadius = 10
                         cell.btnTap.tag = indexPath.row

                         
                         let urlString = selectedActivityModel[0].listActivityPicsArray[0].Img
                         let imageView = UIImageView()
                         
                         let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                         if NSURL(string: selectedActivityModel[0].listActivityPicsArray[0].Img) != nil {
                             
                             SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                                 (receivedSize :Int, ExpectedSize :Int) in
                                 
                             }, completed: {
                                 (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                                 if(image != nil) {
                                     
                                     cell.imgMessage.image = image
                                     //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                     
                                 }else{
                                     imageView.image =  UIImage(named: "placed")

                                     cell.imgMessage.image = UIImage(named: "placed")
                                     // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                 }
                             })
                         }else{
                             imageView.image =  UIImage(named: "placed")
                            
                             cell.imgMessage.image = UIImage(named: "placed")
                             //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                         }
                         
                         cell.lblTitle.text
                             = selectedActivityModel[0].Title
                         cell.textViewMessage.text
                         = selectedActivityModel[0].Description
                    return cell
                }
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellAct", for: indexPath) as! UITableViewCell
                return cell
            }
        }else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! DateSelectActivityTableViewCell
            cellDateSelect = cell
            return cell
        }else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLocation", for: indexPath) as! LocationActivityTableViewCell
            cellDataLocation = cell
            self.locationManager.requestAlwaysAuthorization()

            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()

            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
            
            let camera = GMSCameraPosition.camera(withLatitude: 26.8034, longitude: 75.8178, zoom: 15.0)
            cell.mapView.camera = camera
            
            marker.position = CLLocationCoordinate2D(latitude: 26.8034, longitude: 75.8178)
            marker.isDraggable = true
            marker.map = cell.mapView
            cell.mapView.delegate = self
            marker.isDraggable = true
            reverseGeocoding(marker: marker)
            marker.map = cell.mapView
            
            cell.mapView.delegate = self
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOther", for: indexPath) as! OtherActivityTableViewCell
            if(cell.lblGender.text == ""){
                cell.lblGender.text = "Gender"
                cell.lblGender.textColor = .darkGray
            }
            if(ageRange == ""){
                cell.lblAge.text = "Age Range : " + String(Int(cell.rangeSlider.minimumValue * 100)) + "-" + String(Int(cell.rangeSlider.minimumValue * 100))
                ageRange = String(Int(cell.rangeSlider.minimumValue * 100)) + "-" + String(Int(cell.rangeSlider.minimumValue * 100))
            }
            cellOther = cell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
          if(indexPath.section == 0){
         
        
         
          return 119
          }else if(indexPath.section == 1){
              if(selectedActivityModel.count > 0){
                if(indexPath.row == 1){
                    return UITableView.automaticDimension
                }
              }
              return 50
          }else if(indexPath.section == 2){

              return 284
          }else if(indexPath.section == 3){

              return 455
          }else{

              return 497
          }

        
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
      imagePicker.dismiss(animated: true, completion: nil)
    }

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
       
       
      /*
      guard images.count > 0 else { return }

      let lightboxImages = images.map {
        return LightboxImage(image: $0)
      }

      let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
      imagePicker.present(lightbox, animated: true, completion: nil)
       */
    }

    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        cellDataCover.imgCover.image = images[0]
        
        
      imagePicker.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
