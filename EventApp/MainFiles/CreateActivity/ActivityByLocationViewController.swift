////
////  ActivityByLocationViewController.swift
////  EventApp
////
////  Created by Bunga Mahesh on 06/09/20.
////  Copyright © 2020 Bunga Mahesh. All rights reserved.
////
//
import UIKit
import GoogleMaps
import GooglePlaces
import CoreData
import SDWebImage
struct MyPlace {
    var name: String
    var lat: Double
    var long: Double

}
//
//
class ActivityByLocationViewController:CommonClassViewController, CLLocationManagerDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    let currentLocationMarker = GMSMarker()
    var locationManager = CLLocationManager()
    var chosenPlace: MyPlace?
    
    let customMarkerWidth: Int = 50
    let customMarkerHeight: Int = 70
    
    let previewDemoData = [(title: "Sweat", img: #imageLiteral(resourceName: "1"), price: 10), (title: "Meat", img: #imageLiteral(resourceName: "2"), price: 8), (title: "Chicken", img: #imageLiteral(resourceName: "defaulticon"), price: 12)]
    
   
    @IBOutlet weak var viewRound1: RoundedCornerView!
    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewRound1.roundCorners([.topLeft,.topRight], radius: 20)
  
        self.view.sendSubviewToBack(self.mapView)
      collectionView.layer.cornerRadius = 20
       
        
//
//
//        collectionView.layer.masksToBounds = false
//        collectionView.layer.shadowColor = UIColor.green.cgColor
//
//        collectionView.layer.shadowOpacity = 0.5
//
//        self.view.backgroundColor = UIColor.white
        mapView.delegate=self
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
//
      //  setupViews()
                actPreviewView=RestaurantPreviewView(frame: CGRect(x: 0, y: 0, width: 150, height: 200))
        collectionView.delegate = self
        collectionView.dataSource = self
        initGoogleMaps()
        
        txtFieldSearch.delegate=self
    }
    @IBAction func btnFilterClicked(_ sender: Any) {
                   let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "FilterSearchViewController") as! FilterSearchViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    var actPopularDB:[ActivityPopularListModel] = []
     var actPopularAPI = ActPopularLocModelDataAPI()
    @objc func getActivityData(){
 
               var para = [String:String]()
               
               let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
               "UserID": UserDefaults.standard.string(forKey: "UserID")!,
               "PN":"0",
        "TDate":FilterStruct.endDateFo,
              "FDate":FilterStruct.startDateFo,
              "Gender":FilterStruct.gender,
              "AgeRange":FilterStruct.ageRange,
              "CategoryCSV":FilterStruct.categoryList]
               
               para = parameter.filter { $0.value != ""}
               
               actPopularAPI.serviceCalling(obj: self, parameter: para ) { (dict) in
                   
                   self.actPopularDB = dict as! [ActivityPopularListModel]
                  if(self.actPopularDB.count > 0 ){
                     // self.tableView.reloadData()
                    self.getActivityDataLocal(search: self.txtFieldSearch.text!)
                  }
                   
               }
           }
    
       var actNearDB:[ActivityNearListModel] = []
        var actNearAPI = ActNearLocModelDataAPI()
       @objc func getActivityNearData(){
    
                  var para = [String:String]()
                  
                  let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                                   "UserID": UserDefaults.standard.string(forKey: "UserID")!,"Type":"10000000",
                  "PN":"0",
        "TDate":FilterStruct.endDateFo,
              "FDate":FilterStruct.startDateFo,
              "Gender":FilterStruct.gender,
              "AgeRange":FilterStruct.ageRange,
              "CategoryCSV":FilterStruct.categoryList]
                  
                  para = parameter.filter { $0.value != ""}
                  
                  actNearAPI.serviceCalling(obj: self, parameter: para ) { (dict) in
                      
                      self.actNearDB = dict as! [ActivityNearListModel]
                     if(self.actNearDB.count > 0 ){
                        // self.tableView.reloadData()
                       self.getActivityNearDataLocal(search: self.txtFieldSearch.text!)
                     }
                      
                  }
              }
    
    
    
    var actDBLocal : [ActivityPopularModel] = []
       @objc func getActivityDataLocal(search : String) {
           if(search != ""){
                
                         
                                 self.actDBLocal = [ActivityPopularModel]()
                                 let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                 
                                                    let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ActivityPopularModel")
                        
                              myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                        
                                                   // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                                 
                                                    do{
                                                       let results = try moc.fetch(myRequest)
                                                        actDBLocal = results as! [ActivityPopularModel]
                                                        
                                                        self.collectionView.reloadData()
                                                        for result in results
                                                        {
                                                            print(result)
                                                        }
                                                     
                                                    } catch let error{
                                                        print(error)
                                                    }

            
           }else{
              self.actDBLocal = [ActivityPopularModel]()
                              do {
                      
                                  self.actDBLocal = try context.fetch(ActivityPopularModel.fetchRequest())
                                  //self.refresh.endRefreshing()
                                 
                                 if(self.actDBLocal.count > 0) {
                                    self.collectionView.reloadData()
                                 }else{
                                    // getReportedDirectoryData()
                                    self.collectionView.reloadData()
                                 }
                                 // self.tableView.reloadData()
                      
                              } catch {
                                  print("Fetching Failed")
                              }
              
            //  self.tableView.reloadData()
           }
          }
    
    var actNearDBLocal : [ActivityNearModel] = []
            @objc func getActivityNearDataLocal(search : String) {
                if(search != ""){
                     
                              
                                      self.actNearDBLocal = [ActivityNearModel]()
                                      let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                      
                                                         let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ActivityNearModel")
                             
                                   myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                             
                                                        // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                                      
                                                         do{
                                                            let results = try moc.fetch(myRequest)
                                                             actNearDBLocal = results as! [ActivityNearModel]
                                                              self.setupNEW()
                                                             for result in results
                                                             {
                                                                 print(result)
                                                             }
                                                          
                                                         } catch let error{
                                                             print(error)
                                                         }

                 
                }else{
                   self.actNearDBLocal = [ActivityNearModel]()
                                   do {
                           
                                       self.actNearDBLocal = try context.fetch(ActivityNearModel.fetchRequest())
                                       //self.refresh.endRefreshing()
                                      
                                      if(self.actNearDBLocal.count > 0) {
                                         self.setupNEW()
                                      }else{
                                        self.setupNEW()
                                         // getReportedDirectoryData()
                                      }
                                      // self.tableView.reloadData()
                           
                                   } catch {
                                       print("Fetching Failed")
                                   }
                   
                 //  self.tableView.reloadData()
                }
               }
      
      
      
      
    
    
    func setupNEW(){
         self.mapView.delegate=self
         
         self.locationManager.delegate = self
         self.locationManager.requestWhenInUseAuthorization()
         self.locationManager.startUpdatingLocation()
         self.locationManager.startMonitoringSignificantLocationChanges()
         
         
        self.collectionView.reloadData()
         self.showPartyMarkers(lat: self.locationManager.location!.coordinate.latitude, long: self.locationManager.location!.coordinate.longitude)
    }
    @IBAction func textEditChanged(_ sender: UITextField) {
        seachh = txtFieldSearch.text!
         self.getActivityDataLocal(search: txtFieldSearch.text!)
        self.getActivityNearDataLocal(search: txtFieldSearch.text!)
    }
   
           override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(true)
getActivityData()
            getActivityNearData()
            txtFieldSearch.text = seachh
             self.navigationController?.navigationBar.isHidden = true
    
               self.tabBarController?.tabBar.isHidden = false
           }
    
    @IBAction func btnDrowClicked(_ sender: UIButton) {
        
        
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ActivityAllNavViewController") as! ActivityAllNavViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = CATransitionType.fade
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        view.window!.layer.add(transition, forKey: kCATransition)
        mainVC.modalPresentationStyle = .fullScreen
        self.present(mainVC, animated: false, completion: nil)
    }
    //MARK: textfield
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        
        let filter = GMSAutocompleteFilter()
        autoCompleteController.autocompleteFilter = filter
        
        self.locationManager.startUpdatingLocation()
        self.present(autoCompleteController, animated: true, completion: nil)
        return false
    }
    
    // MARK: GOOGLE AUTO COMPLETE DELEGATE
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude
        
        showPartyMarkers(lat: lat, long: long)
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 17.0)
        mapView.camera = camera
        txtFieldSearch.text=place.formattedAddress
        chosenPlace = MyPlace(name: place.formattedAddress!, lat: lat, long: long)
        let marker=GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = "\(place.name)"
        marker.snippet = "\(place.formattedAddress!)"
        marker.map = mapView
        
        self.dismiss(animated: true, completion: nil) // dismiss after place selected
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initGoogleMaps() {
        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 17.0)
        self.mapView.camera = camera
        self.mapView.delegate = self
        
        self.mapView.animate(to: camera)
         
         self.showPartyMarkers(lat: self.locationManager.location!.coordinate.latitude, long: self.locationManager.location!.coordinate.longitude)
      //  self.mapView.isMyLocationEnabled = true
    }
    ////
    
    func loadNiB() -> ActivityView {
        let infoWindow = UINib(nibName: "ActivityView", bundle: nil).instantiate(withOwner: self, options: nil).first as! ActivityView
        return infoWindow
    }
    private var infoWindow = ActivityView()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        var markerData : NSDictionary?
////        if let data = marker.userData! as? NSDictionary {
////            markerData = data
////        }
//        locationMarker = marker
//        infoWindow.removeFromSuperview()
//        infoWindow = loadNiB()
//        guard let location = locationMarker?.position else {
//            print("locationMarker is nil")
//            return false
//        }
//        // Pass the spot data to the info window, and set its delegate to self
////        infoWindow.spotData = markerData
////        infoWindow.delegate = self
//        // Configure UI properties of info window
//        infoWindow.alpha = 0.9
//        infoWindow.layer.cornerRadius = 12
//        infoWindow.layer.borderWidth = 2
//        infoWindow.layer.borderColor = UIColor(hexString: "19E698").cgColor
//
//
////        let address = markerData!["address"]!
////        let rate = markerData!["rate"]!
////        let fromTime = markerData!["fromTime"]!
////        let toTime = markerData!["toTime"]!
//
////        infoWindow.addressLabel.text = address as? String
////        infoWindow.priceLabel.text = "$\(String(format:"%.02f", (rate as? Float)!))/hr"
////        infoWindow.availibilityLabel.text = "\(convertMinutesToTime(minutes: (fromTime as? Int)!)) - \(convertMinutesToTime(minutes: (toTime as? Int)!))"
//        // Offset the info window to be directly above the tapped marker
//        infoWindow.center = mapView.projection.point(for: location)
//        infoWindow.center.y = infoWindow.center.y - 82
//        self.view.addSubview(infoWindow)
//        return false
//    }
    
    // MARK: CLLocation Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while getting location \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.delegate = nil
        locationManager.stopUpdatingLocation()
        let location = locations.last
        let lat = (location?.coordinate.latitude)!
        let long = (location?.coordinate.longitude)!
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 17.0)
        
        self.mapView.animate(to: camera)
        
        showPartyMarkers(lat: lat, long: long)
    }
    
    // MARK: GOOGLE MAP DELEGATE
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {




        guard let customMarkerView = marker.iconView as? CustomMarkerView else { return false }
        //let img = customMarkerView.img!
        let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: customMarkerWidth, height: customMarkerHeight), image: UIImage(), borderColor: UIColor.clear, tag: customMarkerView.tag)

        marker.iconView = customMarker

        return false
    }

    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let customMarkerView = marker.iconView as? CustomMyMarker else { return nil }
//        let data = previewDemoData[customMarkerView.tag]
        
        

        
        actPreviewView.setData(data : self.actNearDBLocal[customMarkerView.tag],img:imgArray[customMarkerView.tag])
        
        
        //customMarkerView.backgroundColor = .clear

//        infoWindow = loadNiB()
        return actPreviewView
    }
  
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        guard let customMarkerView = marker.iconView as? CustomMyMarker else { return }
        
        let tag = customMarkerView.tag
        restaurantTapped(tag: tag)
    }
    
    func mapView(_ mapView: GMSMapView, didCloseInfoWindowOf marker: GMSMarker) {
        guard let customMarkerView = marker.iconView as? CustomMyMarker else { return }
        let img = customMarkerView.img!
        let allViewsInXibArray1 = Bundle.main.loadNibNamed("CustomMyMarker", owner: self, options: nil)
        let customMarker = allViewsInXibArray1?.first as! CustomMyMarker
        customMarker.img.image = img.image
        customMarker.img.clipsToBounds = true
        customMarker.img.layer.cornerRadius = 15
        customMarker.tag = customMarkerView.tag
       // customMarkerView.backgroundColor = .clear
        marker.iconView = customMarker
    }
    

    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    var imgArray : [UIImage] = []
    func showPartyMarkers(lat: Double, long: Double) {
        mapView.clear()
        if(actNearDBLocal.count > 0){
            imgArray  = []
        for i in 0...actNearDBLocal.count - 1 {
           
            let marker=GMSMarker()
            var imageView = UIImageView()
           
                
                var urlString = ""

                 urlString = self.actNearDBLocal[i].img!

                    let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    if NSURL(string: urlShow!) != nil {
                        
                        SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                            (receivedSize :Int, ExpectedSize :Int) in
                            
                        }, completed: {
                            (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                            if(image != nil) {
                                
                                imageView.image = image
                                self.imgArray.append(image!)
//                                let customMarker = CustomMarkerView(frame: CGRect(x: 0, y: 0, width: self.customMarkerWidth, height: self.customMarkerHeight), image:UIImage(named: "ProfileSample")! , borderColor: UIColor.darkGray, tag: i)
                                
                                let allViewsInXibArray1 = Bundle.main.loadNibNamed("CustomMyMarker", owner: self, options: nil)
                                let customMarker = allViewsInXibArray1?.first as! CustomMyMarker
                                customMarker.img.image = image!
                                customMarker.img.clipsToBounds = true
                                customMarker.img.layer.cornerRadius = 15
                                customMarker.tag = i
                                     marker.iconView=customMarker
                                  
                                    
                                
                                     
                                  marker.position = CLLocationCoordinate2D(latitude: Double(self.actNearDBLocal[i].latitude!)!, longitude: Double(self.actNearDBLocal[i].longitude!)! )
                                     
                                marker.map = self.mapView
                                //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                
                            }
                        })
                    }

        }
        }
    }
    
    @objc func btnMyLocationAction() {
        let location: CLLocation? = mapView.myLocation
        if location != nil {
            mapView.animate(toLocation: (location?.coordinate)!)
        }
    }
    
    @objc func restaurantTapped(tag: Int) {
//        let v=DetailsVC()
//        v.passedData = previewDemoData[tag]
//        self.navigationController?.pushViewController(v, animated: true)
    }
    
    func setupTextField(textField: UITextField, img: UIImage){
        textField.leftViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 20, height: 20))
        imageView.image = img
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 30, height: 30))
        paddingView.addSubview(imageView)
        textField.leftView = paddingView
    }
    
  

    
    let txtFieldSearch: UITextField = {
        let tf=UITextField()
        tf.borderStyle = .roundedRect
        tf.backgroundColor = .white
        tf.layer.borderColor = UIColor.darkGray.cgColor
        tf.placeholder="Search for a location"
        tf.translatesAutoresizingMaskIntoConstraints=false
        return tf
    }()
    
    let btnMyLocation: UIButton = {
        let btn=UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(#imageLiteral(resourceName: "map_Pin"), for: .normal)
        btn.layer.cornerRadius = 25
        btn.clipsToBounds=true
        btn.tintColor = UIColor.gray
        btn.imageView?.tintColor=UIColor.gray
        btn.addTarget(self, action: #selector(btnMyLocationAction), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints=false
        return btn
    }()
    
    var actPreviewView: RestaurantPreviewView = {
        let v=RestaurantPreviewView()
       
        return v
    }()
    
    
    @objc func tapCellCollect(_ sender: UITapGestureRecognizer) {
          

          
          let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.collectionView)
          let indexPath = self.collectionView.indexPathForItem(at: buttonPosition)
          
        
                       let storyboard = UIStoryboard(name: "Event", bundle: nil)
                   let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ActivityPageViewController") as! ActivityPageViewController
        mainVC.actID = actDBLocal[(indexPath?.row)!].id!
           mainVC.modalPresentationStyle = .fullScreen
           self.navigationController?.pushViewController(mainVC, animated: true)
          
      }
    
    
        // MARK: - Card Collection Delegate & DataSource
          
          func numberOfSections(in collectionView: UICollectionView) -> Int {
              return 1
          }
          
          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return actDBLocal.count
          }
          
          func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! MessageCollectionViewCell
            
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCellCollect(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
      cell.imgCollect.layer.cornerRadius = 10
       cell.viewOverlay.layer.cornerRadius = 10
      let datee = dateFormatChange(inputDateStr: actDBLocal[indexPath.row].startDateTime!, inputFormat: "yyyy-MM-dd'T'hh:mm:ss", outputFromat: "hh:mm a")
      cell.lblDate.text = datee
      cell.lblCategory.text = actDBLocal[indexPath.row].categoryNames
      if(actDBLocal[indexPath.row].isRepeated == "0"){
      cell.lblDay.text = "No Repeat"
      }
      
      cell.lblTitle.text = actDBLocal[indexPath.row].title
      if(actDBLocal[indexPath.row].mateCount! != nil){
      if(actDBLocal[indexPath.row].mateCount == "0" || actDBLocal[indexPath.row].mateCount == "1"){
          cell.lblMatesCount.text = actDBLocal[indexPath.row].mateCount! + " Mate"
      }else{
          cell.lblMatesCount.text = actDBLocal[indexPath.row].mateCount! + " Mates"
      }
      }
      cell.widthDate.constant = cell.lblDate.intrinsicContentSize.width + 10
       cell.widthCategory.constant = cell.lblCategory.intrinsicContentSize.width + 10
      cell.widthDay.constant = cell.lblDay.intrinsicContentSize.width + 10
      cell.viewDay.layer.cornerRadius = 5
      cell.viewCategory.layer.cornerRadius = 5
      cell.viewDate.layer.cornerRadius = 5
      cell.imgPro1.layer.cornerRadius = 3
             cell.imgPro3.layer.cornerRadius = 3
             cell.imgPro2.layer.cornerRadius = 3

      
      var urlString = ""
     
      let fullImgArray = actDBLocal[indexPath.row].mateImg?.components(separatedBy: ",")
      if(fullImgArray != nil){
      if(fullImgArray!.count > 0){
          if(fullImgArray?.count == 1){
              if let url = NSURL(string: fullImgArray![0]) {
       cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
              }
              cell.imgPro2.isHidden = true
              cell.imgPro1.isHidden = true
              cell.width1.constant = 10
              cell.width2.constant = 10
      }
          
          
              if(fullImgArray?.count == 2){
                         if let url = NSURL(string: fullImgArray![0]) {
                  cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                         }
                  if let url = NSURL(string: fullImgArray![1]) {
           cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                  }
             
                             cell.imgPro1.isHidden = true
                           
                             cell.width1.constant = 10
          }
          
              if(fullImgArray?.count == 3){
                         if let url = NSURL(string: fullImgArray![0]) {
                  cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                         }
                  if let url = NSURL(string: fullImgArray![1]) {
           cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                  }
                         if let url = NSURL(string: fullImgArray![2]) {
                  cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                         }
          }
      }
      }
      
      
      
      
      
      
       urlString = actDBLocal[indexPath.row].actImg!

          let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
          if NSURL(string: urlShow!) != nil {
              
              SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                  (receivedSize :Int, ExpectedSize :Int) in
                  
              }, completed: {
                  (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                  if(image != nil) {
                      
                      cell.imgCollect.image = image
                      //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                      
                  }else{
                     

                      cell.imgCollect.image = UIImage(named: "ProfileSample")
                      // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                  }
              })
          }else{
           
             
              cell.imgCollect.image = UIImage(named: "ProfileSample")
              //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
          }
     
      
      
        return cell
    }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
             {
                
                
               
                 return CGSize(width: 160, height: 240)
             }
          func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    //          let character = items[(indexPath as NSIndexPath).row]
    //          let alert = UIAlertController(title: character.name, message: nil, preferredStyle: .alert)
    //          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    //          present(alert, animated: true, completion: nil)
          }

          
          // MARK: - UIScrollViewDelegate
          
          func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
              let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
              let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
              let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
              currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
          }
          
        fileprivate var currentPage: Int = 0 {
               didSet {
    //               let character = self.items[self.currentPage]
    //               self.infoLabel.text = character.name.uppercased()
    //               self.detailLabel.text = character.movie.uppercased()
               }
           }
           
           fileprivate var pageSize: CGSize {
               let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
               var pageSize = layout.itemSize
               if layout.scrollDirection == .horizontal {
                   pageSize.width += layout.minimumLineSpacing
               } else {
                   pageSize.height += layout.minimumLineSpacing
               }
               return pageSize
           }
}




