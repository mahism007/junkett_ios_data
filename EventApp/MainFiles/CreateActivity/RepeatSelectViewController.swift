//
//  RepeatSelectViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 15/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class RepeatSelectViewController: CommonClassViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    var tags: [String] = ["Everday","Weekly","Monthly"]
var tagsCondition: [Bool] = [false,false,false]
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       UserDefaults.standard.set(1, forKey: "page")
   let gradientLayer = CAGradientLayer()
          gradientLayer.frame = self.view.bounds
 gradientLayer.colors = [UIColor.colorwithHexString("F1A602", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("DA8901", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("C26900", alpha: 1.0)?.cgColor]
          self.view.layer.insertSublayer(gradientLayer, at: 0)
       
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
         
        
//         cell.collectionViewHeight.constant = cell.InterestCollectionView.collectionViewLayout.collectionViewContentSize.height;
       
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        
        NotificationCenter.default.post(name:NSNotification.Name.init("Repeat"), object: nil)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func btnTypeClicked(_ sender: UIButton) {
         tagsCondition  = [false,false,false]
        self.tagsCondition[sender.tag] = true
        RegisterStruct.repeatType = self.tags[sender.tag]
        collectionView.reloadData()
      
    }
 
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.tags.count > 0){
            return self.tags.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

        cell.btnType.tag = indexPath.row
        cell.lblType.text = self.tags[indexPath.row]
        cell.lblType.textColor = .white
        cell.viewType.backgroundColor = .clear
        cell.viewType.layer.borderWidth = 1.0
        cell.viewType.layer.cornerRadius = 10
        if(self.tagsCondition[indexPath.row] == true){
            cell.viewType.layer.borderColor = UIColor.systemRed.cgColor
        }else{
            cell.viewType.layer.borderColor = UIColor.white.cgColor
        }
       
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if(tags.count > 0){
        let label = UILabel(frame: CGRect.zero)
        label.text = tags[indexPath.item]
        label.sizeToFit()
        return CGSize(width: 50 + label.frame.width, height: 40)
        }else{
               return CGSize(width: 50, height: 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("it worked")
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

