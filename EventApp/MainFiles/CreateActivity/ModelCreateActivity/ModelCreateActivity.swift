//
//  ModelCreateActivity.swift
//  EventApp
//
//  Created by Bunga Mahesh on 20/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import Foundation

class ActivityModel: NSObject {

var CBy = String()

var CreatedBy = String()

var StartDateTime = String()

var PlaceName = String()

var Gender = String()

var Latitude = String()

var Title = String()

var Longitude = String()

var AgeRange = String()

var FullAddress = String()

var IsRepeated = String()

var ID = String()

var EndDateTime = String()

var CategoryNames = String()

var Description = String()
     var listActivityPicsArray = [ImgModel]()
    var listProfilePicsArray = [ImgModel]()
    var listMatesProfilePic = [ImgModel]()
func setDataInModel(cell:[String:AnyObject])
{

    if let data1 = cell["ActivityPicsArray"] as? [AnyObject]
    {
        
        var dataArray1: [ImgModel] = []
        if(data1.count  >  0){
            for i in 0..<data1.count
            {
                
                if let str = data1[i] as? [String:AnyObject]
                {
                    let object1 = ImgModel()
                    object1.setDataInModel(cell: str)
                    dataArray1.append(object1)
                }
                
            }
            listActivityPicsArray = dataArray1
            
        }
        
    }
    if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
    {
        
        var dataArray1: [ImgModel] = []
        if(data1.count  >  0){
            for i in 0..<data1.count
            {
                
                if let str = data1[i] as? [String:AnyObject]
                {
                    let object1 = ImgModel()
                    object1.setDataInModel(cell: str)
                    dataArray1.append(object1)
                }
                
            }
            listProfilePicsArray = dataArray1
            
        }
        
    }
    if let data1 = cell["MatesProfilePic"] as? [AnyObject]
    {
        
        var dataArray1: [ImgModel] = []
        if(data1.count  >  0){
            for i in 0..<data1.count
            {
                
                if let str = data1[i] as? [String:AnyObject]
                {
                    let object1 = ImgModel()
                    object1.setDataInModel(cell: str)
                    dataArray1.append(object1)
                }
                
            }
            listMatesProfilePic = dataArray1
            
        }
        
    }
    
    
    
    
if cell["CBy"] is NSNull || cell["CBy"] == nil {
 self.CBy = ""
}else{
let app = cell["CBy"]
self.CBy = (app?.description)!
}

if cell["CreatedBy"] is NSNull || cell["CreatedBy"] == nil {
 self.CreatedBy = ""
}else{
let app = cell["CreatedBy"]
self.CreatedBy = (app?.description)!
}

if cell["StartDateTime"] is NSNull || cell["StartDateTime"] == nil {
 self.StartDateTime = ""
}else{
let app = cell["StartDateTime"]
self.StartDateTime = (app?.description)!
}

if cell["PlaceName"] is NSNull || cell["PlaceName"] == nil {
 self.PlaceName = ""
}else{
let app = cell["PlaceName"]
self.PlaceName = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["Title"] is NSNull || cell["Title"] == nil {
 self.Title = ""
}else{
let app = cell["Title"]
self.Title = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}

if cell["AgeRange"] is NSNull || cell["AgeRange"] == nil {
 self.AgeRange = ""
}else{
let app = cell["AgeRange"]
self.AgeRange = (app?.description)!
}

if cell["FullAddress"] is NSNull || cell["FullAddress"] == nil {
 self.FullAddress = ""
}else{
let app = cell["FullAddress"]
self.FullAddress = (app?.description)!
}

if cell["IsRepeated"] is NSNull || cell["IsRepeated"] == nil {
 self.IsRepeated = ""
}else{
let app = cell["IsRepeated"]
self.IsRepeated = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["EndDateTime"] is NSNull || cell["EndDateTime"] == nil {
 self.EndDateTime = ""
}else{
let app = cell["EndDateTime"]
self.EndDateTime = (app?.description)!
}

if cell["CategoryNames"] is NSNull || cell["CategoryNames"] == nil {
 self.CategoryNames = ""
}else{
let app = cell["CategoryNames"]
self.CategoryNames = (app?.description)!
}

if cell["Description"] is NSNull || cell["Description"] == nil {
 self.Description = ""
}else{
let app = cell["Description"]
self.Description = (app?.description)!
}

}
}
class ImgModel: NSObject {

var Img = String()



func setDataInModel(cell:[String:AnyObject])
{

if cell["Img"] is NSNull || cell["Img"] == nil {
 self.Img = ""
}else{
let app = cell["Img"]
self.Img = (app?.description)!
}


}
}


class ActivitySelectDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CreateActivitySelectViewController,parameter : [String:Any],url : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: url , parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){

                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ActivityModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = ActivityModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                     
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.tableView.reloadData()
                       // obj.showSuccess(strMessage: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ActivitySelectDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:CreateActivitySelectViewController,parameter:[String:String] , url : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        //obj.tableView.isHidden = false
                        //obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ActivityModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ActivityModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No "+status+" hazard found" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                }
                
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
