//
//  ActivityView.swift
//  EventApp
//
//  Created by Bunga Mahesh on 06/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ActivityView: UIView {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblMatesCount: UILabel!
    @IBOutlet weak var widthCat: NSLayoutConstraint!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var widthTime: NSLayoutConstraint!
    @IBOutlet weak var widthDay: NSLayoutConstraint!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
  
 
}
