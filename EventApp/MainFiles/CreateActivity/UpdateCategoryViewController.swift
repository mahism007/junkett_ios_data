//
//  UpdateCategoryViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 06/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class UpdateCategoryViewController: CommonClassViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.string(forKey: "SessionID")!)
        print(UserDefaults.standard.string(forKey: "UserID")!)
       // catSelect = []
        CategoryData()
        self.view.backgroundColor = .white
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
        
//
       
        // Do any additional setup after loading the view.
    }
           override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(true)
            
                     self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.navigationBar.isHidden = false
              self.navigationController?.navigationBar.topItem?.title = ""
                    //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
            self.title = "Update Category"
                      
                      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                      self.navigationController?.navigationBar.shadowImage = UIImage()
                      self.navigationController?.navigationBar.isTranslucent = true
                      self.navigationController?.view.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .black

                   
               
           }
   
    @IBAction func btnNextClicked(_ sender: UIButton) {
      //  catSelect = []
        var ii = ""
          var iii = 0
        for i in 0...categoryListDB.count - 1 {
            if(categoryListDB[i].boolSelected == true){
                
               //catSelect.append(categoryListDB[i])
                ii = categoryListDB[i].ID + "," + ii
                 iii = iii + 1
            }
            
        }
        if(iii > 0){
        ii.remove(at: ii.index(before: ii.endIndex))
           
        }
        if(ii != ""){
            submitData(csv: ii)
        }
        
        
        

        
        
        
    }
    func submitData(csv : String){

           self.startLoadingPK(view: self.view)
           
           DispatchQueue.global(qos: .background).async {
           

           let parameter = [
    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
      "UserID": UserDefaults.standard.string(forKey: "UserID")!,

      "UserCategoryCSV": csv


             
           ] as [String : Any]
           print(parameter)
             WebServices.sharedInstances.sendPostRequest(url: URLConstants.UpdateUserCategory, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                 
                 if let response = response["response"]{
                  
                     let objectmsg = MessageCallServerModel1()
                     
                     let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      let objSucess = SuccessServerModel()
                       let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                     if(statusStringsuccess == "200"){
                     
                    self.stopLoadingPK(view: self.view)
                    self.showSuccess(strMessage: msg)
                    self.navigationController?.popToRootViewController(animated: true)
                     
                     
                 }else{
                      self.stopLoadingPK(view: self.view)
                     self.showError(strMessage: msg)
                 }
                }
                 
             }) { (err) in
                self.stopLoadingPK(view: self.view)
                 print(err.description)
             }

           }
     }
    @IBAction func btnTypeClicked(_ sender: UIButton) {
 
        if(categoryListDB.count > 0){
            

            
            
                if(categoryListDB[sender.tag].boolSelected == true){
                categoryListDB[sender.tag].boolSelected = false
                }else if(categoryListDB[sender.tag].boolSelected == false){
                 categoryListDB[sender.tag].boolSelected = true
                   
                }
                

        }
       
        
        
        collectionView.reloadData()
    
    }
    var categortSelect : [CategoryCountryListModel] = []
    var categoryListAPI = CategoryListModelDataAPI()
           
           func CategoryData()  {
           
               
             categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
                   categoryListDB = dict as! [CategoryCountryListModel]
                self.collectionView.reloadData()
                if(categoryListDB.count > 0){
                    
                    for i in 0...categoryListDB.count - 1 {
                        for j in 0...self.categortSelect.count - 1 {
                            if(categoryListDB[i].ID == self.categortSelect[j].ID){
                            categoryListDB[i].boolSelected = true
                            break;
                        }
                           
                       }
                        
                    }
                      self.collectionView.reloadData()


                }
                
                
                
                
                
                   
               }
               
               
           }
   func numberOfSections(in collectionView: UICollectionView) -> Int {
               
               return 1
           }
           
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               if(categoryListDB.count > 0){
                   return categoryListDB.count
               }
               return 0
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

               cell.btnType.tag = indexPath.row
               cell.lblType.text = categoryListDB[indexPath.row].Name
               cell.lblType.textColor = .white
            cell.viewType.backgroundColor = UIColor.init(hexString: categoryListDB[indexPath.row].ColorCode)
    //           cell.viewType.layer.borderWidth = 1.0
    //           cell.viewType.layer.cornerRadius = 10
               if(categoryListDB[indexPath.row].boolSelected == true){
                   cell.viewType.backgroundColor = UIColor.init(hexString: categoryListDB[indexPath.row].ColorCode)
                cell.lblType.textColor = .white
               }else{
                   cell.viewType.backgroundColor = UIColor.white
                cell.lblType.textColor = .darkGray
               }
           
                 let urlShow = categoryListDB[indexPath.row].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
              if let url = NSURL(string: urlShow!) {
               cell.imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
              }
              cell.imgView.roundCorners([.topLeft,.bottomLeft], radius: 10)
               return cell
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               
               return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
           {
               if(categoryListDB.count > 0){
               let label = UILabel(frame: CGRect.zero)
               label.text = categoryListDB[indexPath.item].Name
               label.sizeToFit()
                let screen = (UIScreen.main.bounds.width - 60) / 2
               // 50 + label.frame.width
               return CGSize(width: screen , height: 60)
               }else{
                      return CGSize(width: 40, height: 60)
               }
           }
           
           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               
               print("it worked")
               //print(indexPath.item)
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
               
               
           }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

