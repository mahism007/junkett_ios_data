//
//  UpdateSettingViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 20/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class UpdateSettingViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        
       
        
        let objectData111 = profileDisplay(name: "Show Notification", data: "1")
        dataArrayMessage.append(objectData111)
        let objectData121 = profileDisplay(name: "Show Location", data: "1")
        dataArrayMessage.append(objectData121)
        let objectData131 = profileDisplay(name: "Show Email ID", data: "1")
        dataArrayMessage.append(objectData131)
        let objectData141 = profileDisplay(name: "Show Mobile No", data: "1")
        dataArrayMessage.append(objectData141)
        let objectData151 = profileDisplay(name: "Show Gender", data: "1")
        dataArrayMessage.append(objectData151)
        let objectData161 = profileDisplay(name: "Show Date of Birth", data: "1")
        dataArrayMessage.append(objectData161)
        self.tableView.reloadData()
    }
    

    
    
    @IBAction func btnUpdateSetting(_ sender: UIButton) {
       submitData()
    }
    func submitData(){

          self.startLoadingPK(view: self.view)
          
          DispatchQueue.global(qos: .background).async {
          var url = String()
            var parameter : [String:String] = [:]
            url = URLConstants.UpdateSetting
             parameter = [     "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
               "UserID": UserDefaults.standard.string(forKey: "UserID")!,
               "ShowNotification": self.dataArrayMessage[0].data,
                "ShowMyLocation": self.dataArrayMessage[1].data,
                "ShowMyEmail": self.dataArrayMessage[2].data,
                "ShowMyMobile":self.dataArrayMessage[3].data,
                "ShowMyGender":self.dataArrayMessage[4].data,
                "ShowDOB":self.dataArrayMessage[5].data] as [String : String]
              

          print(parameter)
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameter, successHandler: { (dict:[String : AnyObject]) in
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                    self.stopLoading()
                    
                       self.showSuccess(strMessage: msg)
                    
                }else{
                   self.stopLoading()
                    self.showError(strMessage: msg)
                }
               }
                
            }) { (err) in
               self.stopLoadingPK(view: self.view)
                print(err.description)
            }

          }
    }
    @IBAction func btnSwitchChanged(_ sender: UISwitch) {
        
        if(sender.isOn == true){
            dataArrayMessage[sender.tag].data = "1"
        }else{
            dataArrayMessage[sender.tag].data = "0"
        }
    }
    
    var dataArrayMessage : [profileDisplay] = []
    
         override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(true)
              self.tabBarController?.tabBar.isHidden = true
             self.navigationController?.navigationBar.isHidden = false
    //  self.navigationController?.navigationBar.topItem?.title = ""
             //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
          //  self.navigationController?.navigationBar.isTranslucent = true
               self.title = "Update Setting"
               

                   
               
           }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

            return dataArrayMessage.count
      
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArrayMessage[indexPath.row].name
            cell.lblData.isHidden = true
            cell.imgArrow.isHidden = true
             cell.switcher.isHidden = false
        cell.switcher.tag = indexPath.row
        
        if(dataArrayMessage[indexPath.row].data == "1"){
            cell.switcher.isOn = true
        }else{
            cell.switcher.isOn = false
        }
        
        
        

            return cell
       
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

            return 60
        
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
