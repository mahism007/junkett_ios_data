//
//  CreateActivitySelectViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 20/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SDWebImage
var selectedActivityModel : [ActivityModel] = []
class CreateActivitySelectViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
 
   var intShow = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityList()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
    var activityApi = ActivitySelectDataAPI()
    var activityDB : [ActivityModel] = []
    var activityLoadMoreApi = ActivitySelectDataAPILoadMore()
     var activityLoadMoreDB : [ActivityModel] = []
    var GCMToken = String()
    func ActivityList()  {

          var  paramCategory : [String:String] = [:]
        var url = String()
         if(intShow == 0 || intShow == 1){
          paramCategory = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
         "UserID": UserDefaults.standard.string(forKey: "UserID")!,"Type":"AllActivities",
         "PN":"0"
                               ] as [String : String]
            url = URLConstants.ActivityListFilter
         }else if(intShow == 2){
              paramCategory = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
             "UserID": UserDefaults.standard.string(forKey: "UserID")!,"ToUserID":UserDefaults.standard.string(forKey: "UserID")!,
             "PN":"0"
                                   ] as [String : String]
            
           url = URLConstants.ActivityFavoriteList
         }else{
              paramCategory = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
             "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                   ] as [String : String]
            
           url = URLConstants.MyActivityInviteList
         }
    
       
           
        activityApi.serviceCalling(obj: self,  parameter : paramCategory, url: url ) { (dict) in
            
           
            self.activityDB = [ActivityModel]()
            self.activityDB = dict as! [ActivityModel]
            self.tableView.reloadData()
               
           }
           
           
       }
    func ActivityListLoadMore(Id:String)  {
        var  paramCategory : [String:String] = [:]
        var url = String()
        if(intShow == 0 || intShow == 1){
         paramCategory = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
        "UserID": UserDefaults.standard.string(forKey: "UserID")!,"Type":"AllActivities",
        "PN":Id
                              ] as [String : String]
              url = URLConstants.ActivityListFilter
        }else{
             paramCategory = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
            "UserID": UserDefaults.standard.string(forKey: "UserID")!,"ToUserID":UserDefaults.standard.string(forKey: "UserID")!,
            "PN":Id
                                  ] as [String : String]
            
            url = URLConstants.ActivityFavoriteList
        }
    
       
           
        activityLoadMoreApi.serviceCalling(obj: self,  parameter : paramCategory, url: url ) { (dict) in
            
           
            
            
            self.activityLoadMoreDB = [ActivityModel]()
            self.activityLoadMoreDB = dict as! [ActivityModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.activityDB.append(contentsOf: self.activityLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
               
           }
           
           
       }
  
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
                   self.tabBarController?.tabBar.isHidden = true
                  self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.topItem?.title = ""
                  //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
            if(intShow == 0){
                    self.title = "Select Activity"
            }else if(intShow == 1){
                self.title = "My Activity"
            }else if(intShow == 2){
                self.title = "My Favourite Activity"
            }else if(intShow == 3){
                self.title = "Invited Activity"
            }
                    
                    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                    self.navigationController?.navigationBar.shadowImage = UIImage()
                    self.navigationController?.navigationBar.isTranslucent = true
                    self.navigationController?.view.backgroundColor = .clear
  self.navigationController?.navigationBar.tintColor = .black
        }

    
    @IBAction func btnTapClicked(_ sender: UIButton) {
        if(intShow == 0){
        selectedActivityModel.append(self.activityDB[sender.tag])
        MoveStruct.action = true
        self.navigationController?.popViewController(animated: true)
        }else{
                           let storyboard = UIStoryboard(name: "Event", bundle: nil)
                       let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ActivityPageViewController") as! ActivityPageViewController
            mainVC.actID = self.activityDB[sender.tag].ID
               mainVC.modalPresentationStyle = .fullScreen
               self.navigationController?.pushViewController(mainVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
 var count = 0
       var reach = Reachability()!
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(activityDB.count > 0){
        return activityDB.count
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(activityDB.count == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
            if(reach.connection == .none){
                    cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
                cell.textViewNoData.text = "No Internet Connection"
            }else{
                    cell.imgNoData.image = #imageLiteral(resourceName: "noData")
                
                cell.textViewNoData.text = "No Activities Available"
            }

            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CreateActivitySelectTableViewCell
        
            cell.imgMessage.image = UIImage(named: "1")
            cell.imgMessage.layer.cornerRadius = 10
        cell.btnTap.tag = indexPath.row

        
        let urlString = self.activityDB[indexPath.row].listActivityPicsArray[0].Img
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if NSURL(string: self.activityDB[indexPath.row].listActivityPicsArray[0].Img) != nil {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    
                    cell.imgMessage.image = image
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")

                    cell.imgMessage.image = UIImage(named: "placed")
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
           
            cell.imgMessage.image = UIImage(named: "placed")
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        cell.lblTitle.text
            = self.activityDB[indexPath.row].Title
        cell.textViewMessage.text
        = self.activityDB[indexPath.row].Description
        
        self.data = self.activityDB[indexPath.section].ID
        self.lastObject = self.activityDB[indexPath.section].ID

        cell.selectionStyle = .none

        if ( self.data ==  self.lastObject && indexPath.section == self.activityDB.count - 1)
        {
            if(intShow != 3){
            count = count + 1
            self.ActivityListLoadMore( Id: String(count))
            }
            
        }
        return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        
     if(activityDB.count > 0){
    
        return UITableView.automaticDimension
     }
     return self.view.frame.height - 150
    }
  
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


