//
//  ShareDataViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 08/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ShareDataViewController: UIViewController {

    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var shareImg: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
         shareImg.layer.cornerRadius = 50
        btnCancel.backgroundColor = UIColor.init(hexString: "FF1493", alpha: 0.5)
        btnCancel.layer.borderColor = UIColor.init(hexString: "FF1493", alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.cornerRadius = 20
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnCancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSahreWithOtherApps(_ sender: Any) {
        dismiss(animated: true, completion: nil)
             NotificationCenter.default.post(name:NSNotification.Name.init("ShareWithOther"), object: nil)
          
    }
    @IBAction func btnSahreWithFriends(_ sender: Any) {
        dismiss(animated: true, completion: nil)
          NotificationCenter.default.post(name:NSNotification.Name.init("ShareWithFriends"), object: nil)
          
    }
    @IBAction func btnSahreLink(_ sender: Any) {
        UIPasteboard.general.string = "TEXT"
          dismiss(animated: true, completion: nil)
                  NotificationCenter.default.post(name:NSNotification.Name.init("ShareWith"), object: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
