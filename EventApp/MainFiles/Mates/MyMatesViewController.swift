//
//  MyMatesViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 06/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SDWebImage
class MyMatesViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource , UICollectionViewDelegate, UICollectionViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    var suggestedMatesDB:[SuggestedMatesModel] = []
     var suggestedMatesAPI = SuggestMatesModelDataAPI()
    var refreshControl = UIRefreshControl()
//    var defaultOptions = SwipeOptions()
//    var isSwipeRightEnabled = true
//    var buttonDisplayMode: ButtonDisplayMode = .imageOnly
//    var buttonStyle: ButtonStyle = .circular
    var usesTallCells = false
   var matesReqCount = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
  @objc func getSuggesetedData(){
  
            
        
          
             var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                        "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                        "PN":"0"
                                     ]
           
             
             
             
             suggestedMatesAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                 
                 self.suggestedMatesDB = dict as! [SuggestedMatesModel]
                if(self.suggestedMatesDB.count > 0 ){
                    self.tableView.reloadData()
                }
                 
             }
         }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
getSuggesetedData()
                   self.tabBarController?.tabBar.isHidden = true
                  self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.topItem?.title = ""
                  //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
          self.title = "Mates"
                    
                    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                    self.navigationController?.navigationBar.shadowImage = UIImage()
                    self.navigationController?.navigationBar.isTranslucent = true
                    self.navigationController?.view.backgroundColor = .clear
          self.navigationController?.navigationBar.tintColor = .black
        }

    
    @IBAction func btnSeeAllClicked(_ sender: UIButton) {
                    let storyboard = UIStoryboard(name: "Event", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "AllSuggestedMatesViewController") as! AllSuggestedMatesViewController
        
        mainVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        

        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.cellData.collectionView)
        let indexPath = self.self.cellData.collectionView.indexPathForItem(at: buttonPosition)
        
      
                     let storyboard = UIStoryboard(name: "Event", bundle: nil)
                 let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        mainVC.toSeeID = suggestedMatesDB[(indexPath?.row)!].ID
         mainVC.modalPresentationStyle = .fullScreen
         self.navigationController?.pushViewController(mainVC, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
 
       
    func numberOfSections(in tableView: UITableView) -> Int {
        
        

         
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
             return 1
        }else{

            return 1
        }
        
      
    }
    var reach = Reachability()!
    var cellData : SuggestedMatesTableViewCell!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//if(suggestedMatesDB.count == 0){
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
//    if(reach.connection == .none){
//            cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
//        cell.textViewNoData.text = "No Internet Connection"
//    }else{
//            cell.imgNoData.image = #imageLiteral(resourceName: "noData")
//        cell.textViewNoData.text = "No suggestedMatess Available"
//    }
//
//    return cell
//}else{
    
    if(indexPath.section == 0){
 
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = "Mates Request"
            cell.lblData.text = matesReqCount
            cell.switcher.isHidden = true
            cell.lblData.isHidden = false
            cell.imgArrow.isHidden = false

            return cell
        
    }else{
    
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSuggest", for: indexPath) as! SuggestedMatesTableViewCell
        cellData = cell
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.reloadData()
        if(suggestedMatesDB.count > 0){
            cell.collectionView.isHidden = false
            cell.viewHide.isHidden = true
            cell.img.isHidden = true
            cell.lbl.isHidden = true
            cell.lbl.text = ""
        }else{
            cell.collectionView.isHidden = true
            cell.viewHide.isHidden = false
            cell.img.isHidden = false
            cell.lbl.isHidden = false
            cell.img.image = #imageLiteral(resourceName: "ProfileSample")
            cell.lbl.text = "No Suggested Members"
        }
        
        
        

        
        
        
        return cell
    }
        
        }
   // }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
             return 60
        }else{
         if(suggestedMatesDB.count > 0){
        
            return 250
         }
            return 90
        }
        
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Card Collection Delegate & DataSource
            
            func numberOfSections(in collectionView: UICollectionView) -> Int {
                return 1
            }
            
            func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
                if(suggestedMatesDB.count > 0){
                
                    return suggestedMatesDB.count
                 }
                return 0
            }
            
            func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! SuggestMatesCollectionViewCell
      //          let character = items[(indexPath as NSIndexPath).row]
      //          cell.image.image = UIImage(named: character.imageName)
              cell.imgCollect.layer.cornerRadius = 10
               cell.viewOverlay.layer.cornerRadius = 10
                cell.lblName.text = suggestedMatesDB[indexPath.row].FName + " " +  suggestedMatesDB[indexPath.row].LName
                if let url = NSURL(string: suggestedMatesDB[indexPath.row].listFromProfilePic[0].Img) {
                 cell.imgCollect.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "ProfileSample"))
                    let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
                    cell.addGestureRecognizer(TapGesture)
                    cell.isUserInteractionEnabled = true
                    TapGesture.numberOfTapsRequired = 1
                }
                return cell
            }
            func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
               {
                  
                  
                 
                   return CGSize(width: 160, height: 240)
               }
            func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //          let character = items[(indexPath as NSIndexPath).row]
      //          let alert = UIAlertController(title: character.name, suggestedMates: nil, preferredStyle: .alert)
      //          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
      //          present(alert, animated: true, completion: nil)
            }

            
            // MARK: - UIScrollViewDelegate
            
            func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
                let layout = cellData.collectionView.collectionViewLayout as! UPCarouselFlowLayout
                let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
                let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
                currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
            }
            
          fileprivate var currentPage: Int = 0 {
                 didSet {
      //               let character = self.items[self.currentPage]
      //               self.infoLabel.text = character.name.uppercased()
      //               self.detailLabel.text = character.movie.uppercased()
                 }
             }
             
             fileprivate var pageSize: CGSize {
                 let layout = cellData.collectionView.collectionViewLayout as! UPCarouselFlowLayout
                 var pageSize = layout.itemSize
                 if layout.scrollDirection == .horizontal {
                     pageSize.width += layout.minimumLineSpacing
                 } else {
                     pageSize.height += layout.minimumLineSpacing
                 }
                 return pageSize
             }

}
