//
//  BlockedUserViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 09/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreData
class BlockedUserViewController:  CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var txtSearch: DesignableUITextField!
    var BlockedDB:[BlockMatesModel] = []
    var BlockedAPI = BlockMatesModelDataAPI()
    var StrNav = String()
    var BlockedLoadMoreDB : [BlockMatesModel] = []
    var BlockedAPILoadMore = BlockModelDataAPILoadMore()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getBlockedData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getBlockedData()
                 self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.navigationBar.isHidden = false
          self.navigationController?.navigationBar.topItem?.title = ""
                //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
        self.title = "Blocked Mates"
                  
                  self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                  self.navigationController?.navigationBar.shadowImage = UIImage()
                  self.navigationController?.navigationBar.isTranslucent = true
                  self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .black
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getBlockedData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
   
    var ReqId = String()
    //  let ActionTakenAlertVC = RACPopupViewController.instantiate()
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        
        
    }
    
    
    @IBAction func btnBlockClicked(_ sender: UIButton) {
        
        
//                  let storyboard = UIStoryboard(name: "Event", bundle: nil)
//                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
//        mainVC.toSeeID = self.BlockedDB[sender.tag].ID
//
//        self.navigationController?.pushViewController(mainVC, animated: true)
        
        
    }

    @IBAction func txtSearchEditChnaged(_ sender: UITextField) {
        self.getBlockDataLocal(search : self.txtSearch.text!)
    }
    
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        
        
                       let storyboard = UIStoryboard(name: "Event", bundle: nil)
                   let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
          mainVC.toSeeID = self.BlockedDB[sender.tag].ID
           mainVC.modalPresentationStyle = .fullScreen
           self.navigationController?.pushViewController(mainVC, animated: true)
      }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var param = [String:String]()
    var flag = String()
    var flagUpcomingDelivered = String()
    @objc func getBlockedData(){
        

        param = [
                 "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                 "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                 "PN":String(0)]
 
        print(param)
        
        
        BlockedAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.BlockedDB = dict as! [BlockMatesModel]
            self.refreshControl.endRefreshing()
            self.getBlockDataLocal(search : self.txtSearch.text!)
            
        }
        refreshControl.endRefreshing()
        }
    
    
    var count = 0
func getBlockedDataLoadMore(ID : String){

          param = [
                    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                    "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                    "PN":String(count)]
            
        
        
         
           BlockedAPILoadMore.serviceCalling(obj: self,  param: param ) { (dict) in
               
               self.BlockedLoadMoreDB =  [BlockMatesModel]()
               self.BlockedLoadMoreDB = dict as! [BlockMatesModel]
               
               switch dict.count {
               case 0:
                   break;
               default:
                   self.BlockedDB.append(contentsOf: self.BlockedLoadMoreDB)
                   self.getBlockDataLocal(search : self.txtSearch.text!)
                   break;
               }
               
           }
       }

    
    
    var blockDBLocal : [BLockedUserModel] = []
            @objc func getBlockDataLocal(search : String) {
                if(search != ""){
                     
                              
                                      self.blockDBLocal = [BLockedUserModel]()
                                      let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                      
                                                         let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BLockedUserModel")
                             
                                   myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                             
                                                        // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                                      
                                                         do{
                                                            let results = try moc.fetch(myRequest)
                                                             blockDBLocal = results as! [BLockedUserModel]
                                                            self.tableView.reloadData()
                                                             for result in results
                                                             {
                                                                 print(result)
                                                             }
                                                          
                                                         } catch let error{
                                                             print(error)
                                                         }

                 
                }else{
                   self.blockDBLocal = [BLockedUserModel]()
                                   do {
                           
                                       self.blockDBLocal = try context.fetch(BLockedUserModel.fetchRequest())
                                       //self.refresh.endRefreshing()
                                      
                                      if(self.blockDBLocal.count > 0) {
                                        self.tableView.reloadData()
                                      }else{
                                        self.tableView.reloadData()
                                         // getReportedDirectoryData()
                                      }
                                      // self.tableView.reloadData()
                           
                                   } catch {
                                       print("Fetching Failed")
                                   }
                   
                 //  self.tableView.reloadData()
                }
               }
    
    
    
   
  
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(BlockedDB.count > 0){
           
                return blockDBLocal.count
            }
            return 1
    }
    var message = String()
   var reach = Reachability()!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            
        
        if(BlockedDB.count == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
            if(reach.connection == .none){
                    cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
                cell.textViewNoData.text = "No Internet Connection"
            }else{
                    cell.imgNoData.image = #imageLiteral(resourceName: "noData")
                cell.textViewNoData.text = "No Blocked User(s)"
            }

            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllBlockDataTableViewCell
            cell.lblName.text = blockDBLocal[indexPath.row].name!
            if let url = NSURL(string: blockDBLocal[indexPath.row].img!) {
             cell.imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "ProfileSample"))
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnUnBlock.tag = indexPath.row
            cell.imgProfile.layer.cornerRadius = 10
            
          
            
            self.data = String(self.blockDBLocal[indexPath.row].id!)
            self.lastObject = String(self.blockDBLocal[indexPath.row].id!)
            
            if ( self.data ==  self.lastObject && indexPath.row == self.BlockedDB.count - 1)
            {
                count = count + 1
             self.getBlockedDataLoadMore(ID: "")
                
            }
        return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if(BlockedDB.count > 0){
            return 70
        }
        return self.view.frame.height - 150
        }
        
    
    
    
}
