//
//  AllInviteDataTableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 09/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class AllInviteDataTableViewCell:  UITableViewCell {
    @IBOutlet weak var btnInvite: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
