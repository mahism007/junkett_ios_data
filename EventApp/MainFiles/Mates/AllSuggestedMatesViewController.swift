//
//  AllSuggestedMatesViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 06/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class AllSuggestedMatesViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var SuggestedDB:[SuggestedMatesModel] = []
    var SuggestedAPI = SuggestAllMatesModelDataAPI()
    var StrNav = String()
    var suggestedLoadMoreDB : [SuggestedMatesModel] = []
    var SuggestedAPILoadMore = SuggestAllMatesModelDataAPILoadMore()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getSuggestedData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getSuggestedData()
                 self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.navigationBar.isHidden = false
          self.navigationController?.navigationBar.topItem?.title = ""
                //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
        self.title = "Suggested Mates"
                  
                  self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                  self.navigationController?.navigationBar.shadowImage = UIImage()
                  self.navigationController?.navigationBar.isTranslucent = true
                  self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .black
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getSuggestedData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
   
    var ReqId = String()
    //  let ActionTakenAlertVC = RACPopupViewController.instantiate()
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        
        
    }
    
    
    @IBAction func btnSendMessageClicked(_ sender: UIButton) {
        
        
                  let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
        mainVC.toSeeID = self.SuggestedDB[sender.tag].ID
 
        self.navigationController?.pushViewController(mainVC, animated: true)
        
        
    }

    
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        
        
                       let storyboard = UIStoryboard(name: "Event", bundle: nil)
                   let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
          mainVC.toSeeID = self.SuggestedDB[sender.tag].ID
           mainVC.modalPresentationStyle = .fullScreen
           self.navigationController?.pushViewController(mainVC, animated: true)
      }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var param = [String:String]()
    var flag = String()
    var flagUpcomingDelivered = String()
    @objc func getSuggestedData(){
        

        param = [
                 "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                 "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                 "PN":String(0)]
 
        print(param)
        
        
        SuggestedAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.SuggestedDB = dict as! [SuggestedMatesModel]
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
            
        }
        refreshControl.endRefreshing()
        }
    
    
    var count = 0
func getSuggestedDataLoadMore(ID : String){

          param = [
                    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                    "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                    "PN":String(count)]
            
        
        
         
           SuggestedAPILoadMore.serviceCalling(obj: self,  param: param ) { (dict) in
               
               self.suggestedLoadMoreDB =  [SuggestedMatesModel]()
               self.suggestedLoadMoreDB = dict as! [SuggestedMatesModel]
               
               switch dict.count {
               case 0:
                   break;
               default:
                   self.SuggestedDB.append(contentsOf: self.suggestedLoadMoreDB)
                   self.tableView.reloadData()
                   break;
               }
               
           }
       }

    
   
  
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(SuggestedDB.count > 0){
           
                return SuggestedDB.count
            }
            return 1
    }
    var message = String()
   var reach = Reachability()!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            
        
        if(SuggestedDB.count == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
            if(reach.connection == .none){
                    cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
                cell.textViewNoData.text = "No Internet Connection"
            }else{
                    cell.imgNoData.image = #imageLiteral(resourceName: "noData")
                cell.textViewNoData.text = "No Messages Available"
            }

            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AllSuggestedDataTableViewCell
            cell.lblName.text = SuggestedDB[indexPath.row].FName + " " +  SuggestedDB[indexPath.row].LName
            if let url = NSURL(string: SuggestedDB[indexPath.row].listFromProfilePic[0].Img) {
             cell.imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "ProfileSample"))
            }
            cell.btnProfile.tag = indexPath.row
            cell.btnSendMsg.tag = indexPath.row
            cell.imgProfile.layer.cornerRadius = 10
            
          
            
            self.data = String(self.SuggestedDB[indexPath.row].ID)
            self.lastObject = String(self.SuggestedDB[indexPath.row].ID)
            
            if ( self.data ==  self.lastObject && indexPath.row == self.SuggestedDB.count - 1)
            {
                count = count + 1
             self.getSuggestedDataLoadMore(ID: "")
                
            }
        return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if(SuggestedDB.count > 0){
            return 70
        }
        return self.view.frame.height - 150
        }
        
    
    
    
}
