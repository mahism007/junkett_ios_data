//
//  NearByTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 16/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class NearByTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,SquareFlowLayoutDelegate {
    func shouldExpandItem(at indexPath: IndexPath) -> Bool {
        return false
    }
    
@IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
      //self.setupLayout()
             // self.items = self.createItems()
              let flowLayout = SquareFlowLayout()
              flowLayout.flowDelegate = self
              self.collectionView.collectionViewLayout = flowLayout
             
              
//              NotificationCenter.default.addObserver(self, selector: #selector(ViewController.rotationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
          }
          
         var actNearDBLocal : [ActivityNearModel] = []
         var objMessage : MessageViewController!
         func configureCell(actDB : [ActivityNearModel] ,obj:MessageViewController){
             actNearDBLocal = actDB
             objMessage = obj
             self.collectionView.reloadData()
         }

       

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK: - Card Collection Delegate & DataSource
      
      func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
      }
      
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actNearDBLocal.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellNear", for: indexPath) as! NearbyCollectionViewCell
//          let character = items[(indexPath as NSIndexPath).row]
//          cell.image.image = UIImage(named: character.imageName)
        //cell.imgNear.roundCorners([.allCorners], radius: 10)
        //cell.viewImg.roundCorners([.topRight, .topLeft], radius: 10)
        
        let fullImgArray = actNearDBLocal[indexPath.row].img?.components(separatedBy: ",")
        if(fullImgArray != nil){
        if(fullImgArray!.count > 0){
            if(fullImgArray?.count == 1){
                if let url = NSURL(string: fullImgArray![0]) {
         cell.imgPro.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                }

        }
            }
        }
        let fullArray = actNearDBLocal[indexPath.row].actImg?.components(separatedBy: ",")
        if(fullArray != nil){
        if(fullArray!.count > 0){
            if(fullImgArray?.count == 1){
                if let url = NSURL(string: fullArray![0]) {
         cell.imgNear.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                }

        }
            }
        }
 
               if let url = NSURL(string: fullImgArray![0]) {
        cell.imgPro.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
               }
        cell.lblCategory.text = actNearDBLocal[indexPath.row].categoryNames
        cell.widthCategory.constant = cell.lblCategory.intrinsicContentSize.width + 10
        cell.viewCategory.layer.cornerRadius = 5
        cell.heightImg.constant = cell.viewNear.frame.height / 2
        cell.heightImg.constant = cell.viewNear.frame.height / 2
        cell.imgPro.layer.cornerRadius = 3
          return cell
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
         {



              return CGSize(width: self.collectionView.bounds.size.width, height: self.collectionView.bounds.size.height)
         }
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//          let character = items[(indexPath as NSIndexPath).row]
//          let alert = UIAlertController(title: character.name, message: nil, preferredStyle: .alert)
//          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//          present(alert, animated: true, completion: nil)
      }

 

}


//{
//          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! MessageCollectionViewCell
//
//        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCellCollect(_:)))
//        cell.addGestureRecognizer(TapGesture)
//        cell.isUserInteractionEnabled = true
//        TapGesture.numberOfTapsRequired = 1
//  cell.imgCollect.layer.cornerRadius = 10
//   cell.viewOverlay.layer.cornerRadius = 10
//  let datee = dateFormatChange(inputDateStr: actDBLocal[indexPath.row].startDateTime!, inputFormat: "yyyy-MM-dd'T'hh:mm:ss", outputFromat: "hh:mm a")
//  cell.lblDate.text = datee
//  cell.lblCategory.text = actDBLocal[indexPath.row].categoryNames
//  if(actDBLocal[indexPath.row].isRepeated == "0"){
//  cell.lblDay.text = "No Repeat"
//  }
//
//  cell.lblTitle.text = actDBLocal[indexPath.row].title
//  if(actDBLocal[indexPath.row].mateCount! != nil){
//  if(actDBLocal[indexPath.row].mateCount == "0" || actDBLocal[indexPath.row].mateCount == "1"){
//      cell.lblMatesCount.text = actDBLocal[indexPath.row].mateCount! + " Mate"
//  }else{
//      cell.lblMatesCount.text = actDBLocal[indexPath.row].mateCount! + " Mates"
//  }
//  }
//  cell.widthDate.constant = cell.lblDate.intrinsicContentSize.width + 10
//   cell.widthCategory.constant = cell.lblCategory.intrinsicContentSize.width + 10
//  cell.widthDay.constant = cell.lblDay.intrinsicContentSize.width + 10
//  cell.viewDay.layer.cornerRadius = 5
//  cell.viewCategory.layer.cornerRadius = 5
//  cell.viewDate.layer.cornerRadius = 5
//  cell.imgPro1.layer.cornerRadius = 3
//         cell.imgPro3.layer.cornerRadius = 3
//         cell.imgPro2.layer.cornerRadius = 3
//
//
//  var urlString = ""
//
//  let fullImgArray = actDBLocal[indexPath.row].mateImg?.components(separatedBy: ",")
//  if(fullImgArray != nil){
//  if(fullImgArray!.count > 0){
//      if(fullImgArray?.count == 1){
//          if let url = NSURL(string: fullImgArray![0]) {
//   cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//          }
//          cell.imgPro2.isHidden = true
//          cell.imgPro1.isHidden = true
//          cell.width1.constant = 10
//          cell.width2.constant = 10
//  }
//
//
//          if(fullImgArray?.count == 2){
//                     if let url = NSURL(string: fullImgArray![0]) {
//              cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//                     }
//              if let url = NSURL(string: fullImgArray![1]) {
//       cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//              }
//
//                         cell.imgPro1.isHidden = true
//
//                         cell.width1.constant = 10
//      }
//
//          if(fullImgArray?.count == 3){
//                     if let url = NSURL(string: fullImgArray![0]) {
//              cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//                     }
//              if let url = NSURL(string: fullImgArray![1]) {
//       cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//              }
//                     if let url = NSURL(string: fullImgArray![2]) {
//              cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
//                     }
//      }
//  }
//  }
//
//
//
//
//
//
//   urlString = actDBLocal[indexPath.row].actImg!
//
//      let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//      if NSURL(string: urlShow!) != nil {
//
//          SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
//              (receivedSize :Int, ExpectedSize :Int) in
//
//          }, completed: {
//              (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
//              if(image != nil) {
//
//                  cell.imgCollect.image = image
//                  //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
//
//              }else{
//
//
//                  cell.imgCollect.image = UIImage(named: "ProfileSample")
//                  // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
//              }
//          })
//      }else{
//
//
//          cell.imgCollect.image = UIImage(named: "ProfileSample")
//          //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
//      }
//
//
//
//    return cell
//}
