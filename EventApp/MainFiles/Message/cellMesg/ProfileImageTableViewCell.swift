//
//  ProfileImageTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 23/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ProfileImageTableViewCell: UITableViewCell {
    @IBOutlet weak var viewDown: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    let collectionViewCellHeightCoefficient: CGFloat = 1.5
    let collectionViewCellWidthCoefficient: CGFloat = 1.5
    let priceButtonCornerRadius: CGFloat = 10
    let gradientFirstColor = UIColor.init(hexString: "ff8181").cgColor
    let gradientSecondColor = UIColor.init(hexString: "a81382").cgColor
    let cellsShadowColor = UIColor.init(hexString: "2a002a").cgColor
    let productCellIdentifier = "cellProfile"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
   
        viewDown.layer.cornerRadius = 20
        
      
            }
    var images : [String] = []
    public func configureCollectionView(imagesAll : [String]) {
        images = imagesAll
                let gravitySliderLayout = GravitySliderFlowLayout(with: CGSize(width: collectionView.frame.size.height * collectionViewCellWidthCoefficient, height: collectionView.frame.size.height * collectionViewCellHeightCoefficient))
                collectionView.collectionViewLayout = gravitySliderLayout
                collectionView.dataSource = self
                collectionView.delegate = self
            }
            
           
            
            private func configureProductCell(_ cell: ProfileImageCollectionViewCell, for indexPath: IndexPath) {
                cell.clipsToBounds = false
                let gradientLayer = CAGradientLayer()
                gradientLayer.frame = cell.bounds
                gradientLayer.colors = [gradientFirstColor, gradientSecondColor]
                gradientLayer.cornerRadius = 0
                gradientLayer.masksToBounds = true
                cell.layer.insertSublayer(gradientLayer, at: 0)
                
                cell.layer.shadowColor = cellsShadowColor
                cell.layer.shadowOpacity = 0.2
                cell.layer.shadowRadius = 20
                cell.layer.shadowOffset = CGSize(width: 0.0, height: 30)
                if(images.count > 0){
                if let url = NSURL(string: images[indexPath.row]) {
                 cell.imgProfile.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                }
                }else{
                    cell.imgProfile.image = #imageLiteral(resourceName: "ProfileSample")
                }
                
                
              
            }
            
//            private func animateChangingTitle(for indexPath: IndexPath) {
//                UIView.transition(with: productTitleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
//                    self.productTitleLabel.text = self.titles[indexPath.row % self.titles.count]
//                }, completion: nil)
//                UIView.transition(with: productSubtitleLabel, duration: 0.3, options: .transitionCrossDissolve, animations: {
//                    self.productSubtitleLabel.text = self.subtitles[indexPath.row % self.subtitles.count]
//                }, completion: nil)
//                UIView.transition(with: priceButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
//                    self.priceButton.setTitle(self.prices[indexPath.row % self.prices.count], for: .normal)
//                }, completion: nil)
//            }
            
            @IBAction func didPressPriceButton(_ sender: Any) {
                
            }
 



    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension ProfileImageTableViewCell: UICollectionViewDataSource {
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if(images.count > 0){
            return images.count
                
            }
            return 1
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellIdentifier, for: indexPath) as! ProfileImageCollectionViewCell
               self.configureProductCell(cell, for: indexPath)
               return cell
           }
           
           
       }

       extension ProfileImageTableViewCell: UICollectionViewDelegate {
           func scrollViewDidScroll(_ scrollView: UIScrollView) {
               let locationFirst = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x, y: collectionView.center.y + scrollView.contentOffset.y)
               let locationSecond = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x + 20, y: collectionView.center.y + scrollView.contentOffset.y)
               let locationThird = CGPoint(x: collectionView.center.x + scrollView.contentOffset.x - 20, y: collectionView.center.y + scrollView.contentOffset.y)
               
               if let indexPathFirst = collectionView.indexPathForItem(at: locationFirst), let indexPathSecond = collectionView.indexPathForItem(at: locationSecond), let indexPathThird = collectionView.indexPathForItem(at: locationThird), indexPathFirst.row == indexPathSecond.row && indexPathSecond.row == indexPathThird.row && indexPathFirst.row != pageControl.currentPage {
                   pageControl.currentPage = indexPathFirst.row % images.count
                   
               }
           }
       }
