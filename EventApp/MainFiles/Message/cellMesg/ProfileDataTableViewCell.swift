//
//  ProfileDataTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ProfileDataTableViewCell: UITableViewCell {

    @IBOutlet weak var btnData: UIButton!
    @IBOutlet weak var switcher: UISwitch!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
