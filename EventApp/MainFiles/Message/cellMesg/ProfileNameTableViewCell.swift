//
//  ProfileNameTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DTGradientButton
class ProfileNameTableViewCell: UITableViewCell {

    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnMates: UIButton!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        
             btnMessage.layer.cornerRadius = 20
             btnMessage.layer.masksToBounds = true

               
                    btnMates.layer.cornerRadius = 20
                    btnMates.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
