//
//  ProfileDetailsTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ProfileDetailsTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var cardBio: CardView!
    @IBOutlet weak var heightText: NSLayoutConstraint!
    @IBOutlet weak var viewHide: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var textViewDetails: UITextView!
    @IBOutlet weak var btnShowMore: UIButton!
    @IBOutlet weak var btnShowProfile: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewDetails: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
               collectionView.dataSource = self
    }
    var images : [String] = []
    func configurecell(image : [String]){
        images = image
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
           
           return 1
       }
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
        return images.count
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellImage", for: indexPath) as! ProfileImageCollectionViewCell
        if let url = NSURL(string: images[indexPath.row]) {
         cell.imgProfile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
        }
        cell.imgProfile.layer.cornerRadius = 10
           return cell
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           
           return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
           return CGSize(width: 40, height: 40)
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
           print("it worked")
           //print(indexPath.item)
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
           
           
       }

}
