//
//  CategoryDataTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class CategoryDataTableViewCell: UITableViewCell ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var heightCollect: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
        heightCollect.constant = collectionView.collectionViewLayout.collectionViewContentSize.height;
    }
    var cateDB : [CategoryCountryListModel] = []
    var selcateDB : [CategoryCountryListModel] = []
    var intShowAll = Int()
    func configureCell(catDB : [CategoryCountryListModel],selectedCatDB : [CategoryCountryListModel],intShow : Int){
        cateDB = catDB
        selcateDB = selectedCatDB
        self.collectionView.reloadData()
        intShowAll = intShow
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
           
           return 1
       }
       
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           if(cateDB.count > 0){
               return cateDB.count
           }
           return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

           cell.btnType.tag = indexPath.row
           cell.lblType.text = cateDB[indexPath.row].Name
           cell.lblType.textColor = .white
        
           cell.viewType.backgroundColor = UIColor.white
                   cell.lblType.textColor = .darkGray
        if(intShowAll == 1){
        if(selcateDB.count > 0){
            for i in 0...selcateDB.count - 1{
                if(cateDB[indexPath.row].ID == selcateDB[i].ID ){
                       cell.viewType.backgroundColor = UIColor.init(hexString: cateDB[indexPath.row].ColorCode)
                    cell.lblType.textColor = .white
                    break;
                }
            }
        }
        }else{
               cell.viewType.backgroundColor = UIColor.init(hexString: cateDB[indexPath.row].ColorCode)
            cell.lblType.textColor = .white
        }
        

        
        
//           cell.viewType.layer.borderWidth = 1.0
//           cell.viewType.layer.cornerRadius = 10
//           if(categoryListDB[indexPath.row].boolSelected == true){
//               cell.viewType.layer.borderColor = UIColor.systemRed.cgColor
//           }else{
//               cell.viewType.layer.borderColor = UIColor.white.cgColor
//           }
       
             let urlShow = cateDB[indexPath.row].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
          if let url = NSURL(string: urlShow!) {
           cell.imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
          }
          cell.imgView.roundCorners([.topLeft,.bottomLeft], radius: 10)
           return cell
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           
           return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
           if(cateDB.count > 0){
           let label = UILabel(frame: CGRect.zero)
           label.text = cateDB[indexPath.item].Name
           label.sizeToFit()
            let screen = (UIScreen.main.bounds.width - 60) / 2
           // 50 + label.frame.width
           return CGSize(width: screen , height: 60)
           }else{
                  return CGSize(width: 40, height: 60)
           }
       }
       
       func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           
           print("it worked")
           //print(indexPath.item)
           
           
       }
       
       func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
           
           
       }
}
