//
//  NearbyCollectionViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 16/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class NearbyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var heightImg: NSLayoutConstraint!
    @IBOutlet weak var textViewNear: UITextView!
    @IBOutlet weak var imgNear: UIImageView!
    @IBOutlet weak var viewNear: UIView!
    @IBOutlet weak var viewCategory: UIView!
     @IBOutlet weak var widthCategory: NSLayoutConstraint!
     @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgPro: UIImageView!
}
