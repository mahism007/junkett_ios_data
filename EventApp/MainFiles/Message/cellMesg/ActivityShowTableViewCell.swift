//
//  ActivityShowTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 16/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SDWebImage
class ActivityShowTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
@IBOutlet weak var collectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
      //self.setupLayout()
             // self.items = self.createItems()
              
              self.currentPage = 0
              
//              NotificationCenter.default.addObserver(self, selector: #selector(ViewController.rotationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
          }
    func dateFormatChange(inputDateStr:String,inputFormat:String,outputFromat:String) -> String
    {
        
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = inputFormat

        let resultDate: Date = dateFormatter.date(from: inputDateStr)!

        dateFormatter.dateFormat = outputFromat

        let resultDateStr = dateFormatter.string(from: resultDate)

        if(Debug.show_debug == true)
        {
            print("resultDate",resultDateStr)
        }

        
        return resultDateStr
        
        
    }
    var actModelDB : [ActivityPopularModel] = []
    var cnt = Int()
    func configure(actDB : [ActivityPopularModel]){
        actModelDB = actDB
       
        self.collectionView.reloadData()
    }
         
    fileprivate var currentPage: Int = 0 {
           didSet {
//               let character = self.items[self.currentPage]
//               self.infoLabel.text = character.name.uppercased()
//               self.detailLabel.text = character.movie.uppercased()
           }
       }
       
       fileprivate var pageSize: CGSize {
           let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
           var pageSize = layout.itemSize
           if layout.scrollDirection == .horizontal {
               pageSize.width += layout.minimumLineSpacing
           } else {
               pageSize.height += layout.minimumLineSpacing
           }
           return pageSize
       }
       
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    // MARK: - Card Collection Delegate & DataSource
      
      func numberOfSections(in collectionView: UICollectionView) -> Int {
          return 1
      }
      
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actModelDB.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarouselCollectionViewCell", for: indexPath) as! ActivityShowCollectionViewCell
//          let character = items[(indexPath as NSIndexPath).row]
//          cell.image.image = UIImage(named: character.imageName)
        cell.btnActivity.tag = indexPath.row
        cell.imgCollect.layer.cornerRadius = 10
         cell.viewOverlay.layer.cornerRadius = 10
        let datee = dateFormatChange(inputDateStr: actModelDB[indexPath.row].startDateTime!, inputFormat: "yyyy-MM-dd'T'hh:mm:ss", outputFromat: "hh:mm a")
        cell.lblDate.text = datee
        cell.lblCategory.text = actModelDB[indexPath.row].categoryNames
        if(actModelDB[indexPath.row].isRepeated == "0"){
        cell.lblDay.text = "No Repeat"
        }
        cell.textView.text = actModelDB[indexPath.row].desc
        cell.lblTitle.text = actModelDB[indexPath.row].title
        if(actModelDB[indexPath.row].mateCount! != nil){
        if(actModelDB[indexPath.row].mateCount == "0" || actModelDB[indexPath.row].mateCount == "1"){
            cell.lblMatesCount.text = actModelDB[indexPath.row].mateCount! + " Mate"
        }else{
            cell.lblMatesCount.text = actModelDB[indexPath.row].mateCount! + " Mates"
        }
        }
        cell.widthDate.constant = cell.lblDate.intrinsicContentSize.width + 10
         cell.widthCategory.constant = cell.lblCategory.intrinsicContentSize.width + 10
        cell.widthDay.constant = cell.lblDay.intrinsicContentSize.width + 10
        cell.viewDay.layer.cornerRadius = 5
        cell.viewCategory.layer.cornerRadius = 5
        cell.viewDate.layer.cornerRadius = 5
        cell.imgPro1.layer.cornerRadius = 3
               cell.imgPro3.layer.cornerRadius = 3
               cell.imgPro2.layer.cornerRadius = 3
        cell.imgLoc.setImageColor(color: .white)
        cell.lblLoc.text = actModelDB[indexPath.row].fullAddress
        cell.lblCount.text = String(indexPath.row + 1) + "/" +  String(actModelDB.count)
        
        var urlString = ""
       
        let fullImgArray = actModelDB[indexPath.row].mateImg?.components(separatedBy: ",")
        if(fullImgArray != nil){
        if(fullImgArray!.count > 0){
            if(fullImgArray?.count == 1){
                if let url = NSURL(string: fullImgArray![0]) {
         cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                }
                cell.imgPro2.isHidden = true
                cell.imgPro1.isHidden = true
                cell.width1.constant = 10
                cell.width2.constant = 10
        }
            
            
                if(fullImgArray?.count == 2){
                           if let url = NSURL(string: fullImgArray![0]) {
                    cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                           }
                    if let url = NSURL(string: fullImgArray![1]) {
             cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                    }
               
                               cell.imgPro1.isHidden = true
                             
                               cell.width1.constant = 10
            }
            
                if(fullImgArray?.count == 3){
                           if let url = NSURL(string: fullImgArray![0]) {
                    cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                           }
                    if let url = NSURL(string: fullImgArray![1]) {
             cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                    }
                           if let url = NSURL(string: fullImgArray![2]) {
                    cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                           }
            }
        }
        }
        
        
        
        
        
        
         urlString = actModelDB[indexPath.row].actImg!

            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if NSURL(string: urlShow!) != nil {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        
                        cell.imgCollect.image = image
                        //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                        
                    }else{
                       

                        cell.imgCollect.image = UIImage(named: "ProfileSample")
                        // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
             
               
                cell.imgCollect.image = UIImage(named: "ProfileSample")
                //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
            }
       
        
        
          return cell
      }
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
         {
            
            
           
             return CGSize(width: 160, height: 240)
         }
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//          let character = items[(indexPath as NSIndexPath).row]
//          let alert = UIAlertController(title: character.name, message: nil, preferredStyle: .alert)
//          alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//          present(alert, animated: true, completion: nil)
      }

      
      // MARK: - UIScrollViewDelegate
      
      func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          let layout = self.collectionView.collectionViewLayout as! UPCarouselFlowLayout
          let pageSide = (layout.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
          let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
          currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
      }

}
