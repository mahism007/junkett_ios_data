//
//  HeaderTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 15/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
@IBOutlet weak var lblHeader: UILabel!
    
    @IBOutlet weak var heightDown: NSLayoutConstraint!
    @IBOutlet weak var downArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
