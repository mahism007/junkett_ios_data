//
//  UpdateLocationTableViewCell.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 06/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import GoogleMaps
class UpdateLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewLocation: UITextView!
    @IBOutlet weak var mapView: GMSMapView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
