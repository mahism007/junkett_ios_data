//
//  MessageCollectionViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 15/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class MessageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblMatesCount: UILabel!
    @IBOutlet weak var width3: NSLayoutConstraint!
    @IBOutlet weak var width2: NSLayoutConstraint!
    @IBOutlet weak var width1: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewOverlay: UIView!
    @IBOutlet weak var imgCollect: UIImageView!
    
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var lblDate: UILabel!
    
    @IBOutlet weak var widthDate: NSLayoutConstraint!
    
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var widthCategory: NSLayoutConstraint!
    @IBOutlet weak var lblCategory: UILabel!
    
    
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var widthDay: NSLayoutConstraint!
    @IBOutlet weak var lblDay: UILabel!
    
    @IBOutlet weak var imgPro3: UIImageView!
    @IBOutlet weak var imgPro2: UIImageView!
    @IBOutlet weak var imgPro1: UIImageView!
}
class SuggestMatesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewOverlay: UIView!
    @IBOutlet weak var imgCollect: UIImageView!
    @IBOutlet weak var lblName: UILabel!

}
