//
//  FilterSearchViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 08/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import GoogleMaps
import EzPopup
class FilterSearchViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource ,CLLocationManagerDelegate{

    
    
    @IBOutlet weak var tableView: UITableView!
    var marker = GMSMarker()
    var refreshControl = UIRefreshControl()
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        slectCate = FilterStruct.category
         print(UserDefaults.standard.string(forKey: "SessionID")!)
    CategoryData()
        myCatDB = categoryListDB
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.GenderData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "Gender")), object: nil)
                     NotificationCenter.default.addObserver(self, selector: #selector(self.showDatareload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "showDatareload")), object: nil)
    }
    @objc func showDatareload(){
        
        cellDateSelect.txtStartDate.text = RegisterStruct.startDate
        cellDateSelect.txtEndDate.text = RegisterStruct.endDate
        sDate = RegisterStruct.startDate1
        eDate = RegisterStruct.endDate1
        showSDate = RegisterStruct.startDate
        showEDate = RegisterStruct.endDate

      //  cellData.lblInterstedIn.text = interest
    }
    
    @IBAction func btnTypeClicked(_ sender: UIButton) {
        if(myCatDB[sender.tag].boolSelected == true){
            myCatDB[sender.tag].boolSelected = false
        }else{
            myCatDB[sender.tag].boolSelected = true
        }
        self.tableView.reloadData()
                if(myCatDB.count > 0){

                    slectCate = []
            for i in 0...myCatDB.count - 1 {
                if(myCatDB[i].boolSelected == true){
                    slectCate.append(myCatDB[i])
              
                }
                
            }

        }
        
        
    }
    @IBAction func filterClicked(_ sender: Any) {
        
        FilterStruct.ageRange = ""
        FilterStruct.startDateFo = sDate
        FilterStruct.endDateFo = eDate
        
        FilterStruct.startDate = showSDate
        FilterStruct.endDate = showEDate
        FilterStruct.category = slectCate
        if(slectCate.count > 0){
            var ii = ""
              var iii = 0
            for i in 0...slectCate.count - 1 {
                if(slectCate[i].boolSelected == true){
                    
                   //catSelect.append(categoryListDB[i])
                    ii = slectCate[i].ID + "," + ii
                     iii = iii + 1
                }
                
            }
            if(iii > 0){
            ii.remove(at: ii.index(before: ii.endIndex))
               
            }
            FilterStruct.categoryList = ii
        }
        FilterStruct.gender = genderSelect
        self.navigationController?.popViewController(animated: true)
        
    }
    var slectCate : [CategoryCountryListModel] = []
    @IBAction func resetClicked(_ sender: Any) {
        CategoryData()
        FilterStruct.ageRange = ""
        FilterStruct.startDateFo = ""
        FilterStruct.endDateFo = ""
        
        FilterStruct.startDate = ""
        FilterStruct.endDate = ""
        FilterStruct.category = []

            FilterStruct.categoryList = ""
        slectCate = []
        FilterStruct.gender = ""
        sDate = ""
        eDate = ""
        showSDate = ""
        showEDate = ""
        self.tableView.reloadData()
        
        
    }
    var sDate = String()
    var eDate = String()
    
    
    var showSDate = String()
    var showEDate = String()
    var myCatDB : [CategoryCountryListModel] = []
    var categoryListAPI = CategoryListModelDataAPI()
         
         func CategoryData()  {
         
             
           categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
                 categoryListDB = dict as! [CategoryCountryListModel]
            self.myCatDB = categoryListDB
                 self.tableView.reloadData()
                 
             }
             
             
         }
    @IBAction func btnStartDateClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        ZIVC.popInt = 1
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnEndDateClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
        ZIVC.popInt = 2
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    
    @IBAction func btnStartTimeClicked(_ sender: UIButton) {
    }
    @IBAction func btnEndTimeClicked(_ sender: UIButton) {
    }
    
    var genderSelect = String()
    @objc func GenderData(){
        cellOther.lblGender.text = RegisterStruct.gender
        cellOther.lblGender.textColor = .black
        genderSelect = RegisterStruct.gender
       }

    @IBAction func btnLocationClicked(_ sender: UIButton) {
                   let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "DrawMapRadiusViewController") as! DrawMapRadiusViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
           self.tabBarController?.tabBar.isHidden = true
  self.navigationController?.navigationBar.isHidden = true
            
        }

    @IBAction func btnGender(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "GenderSelectViewController") as! GenderSelectViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 300 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
var ageRange = String()
    @IBAction func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
        cellOther.lblAge.text = "Age Range : " + String(Int(rangeSlider.lowerValue * 100)) + "-" + String(Int(rangeSlider.upperValue * 100))
        ageRange = String(Int(rangeSlider.lowerValue * 100)) + "-" + String(Int(rangeSlider.upperValue * 100))
    }
    var cellDataCat : CategoryDataTableViewCell!
    var cellDateSelect : DateFilterTableViewCell!
    var cellTimeSelect : TimeFilterTableViewCell!
    var cellDataLocation : LocationFilterTableViewCell!
    var cellOther : OtherFilterTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

             return 25

     }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
           let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableViewCell
        if section == 0 {
            header.lblHeader.text = "CATEGORIES"

        }else if section == 1 {
            header.lblHeader.text = "DATE PERIOD"

        }
//        else if section == 2 {
//            header.lblHeader.text = "TIME PERIOD"
//
//        }else if section == 3 {
//            header.lblHeader.text = "LOCATION SERVICES"
//
//        }
        else if section == 2 {
            header.lblHeader.text = "PEOPLE"

        }

           
           
           
           return header
           
       }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCollect", for: indexPath) as! CategoryDataTableViewCell
        cellDataCat = cell
            cell.configureCell(catDB: self.myCatDB, selectedCatDB: slectCate, intShow: 1)
            cell.collectionView.reloadData()
           
       
        return cell
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDate", for: indexPath) as! DateFilterTableViewCell
            cellDateSelect = cell
            if(FilterStruct.startDate != ""){
                cell.txtStartDate.text = FilterStruct.startDate
                sDate = FilterStruct.startDateFo
                
            }
            if(FilterStruct.endDate != ""){
                
                cell.txtEndDate.text = FilterStruct.endDate
                
                eDate = FilterStruct.endDateFo
            }
            return cell
        }
//        else if(indexPath.section == 2){
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellTime", for: indexPath) as! TimeFilterTableViewCell
//            cellTimeSelect = cell
//            return cell
//        }else if(indexPath.section == 3){
//            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLocation", for: indexPath) as! LocationFilterTableViewCell
//            cellDataLocation = cell
//            return cell
//        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOther", for: indexPath) as! OtherFilterTableViewCell
            if(FilterStruct.gender == ""){
                cell.lblGender.text = "Gender"
                cell.lblGender.textColor = .darkGray
            }else{
                cell.lblGender.text = FilterStruct.gender
                cell.lblGender.textColor = .black
            }
            if(FilterStruct.ageRange == ""){
                cell.lblAge.text = "Age Range : " + String(Int(cell.rangeSlider.minimumValue * 100)) + "-" + String(Int(cell.rangeSlider.maximumValue * 100))
                ageRange = String(Int(cell.rangeSlider.minimumValue * 100)) + "-" + String(Int(cell.rangeSlider.maximumValue * 100))
            }else{
            let fullAgeArray = FilterStruct.ageRange.components(separatedBy: "-")
                cell.lblAge.text = FilterStruct.ageRange
                cell.rangeSlider.minimumValue = Double(Int(fullAgeArray[0])!)
                cell.rangeSlider.maximumValue = Double(Int(fullAgeArray[1])!)
                  ageRange = FilterStruct.ageRange
            }
            
            cellOther = cell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       if(indexPath.section == 1 || indexPath.section == 3){

              return 50
          }else if(indexPath.section == 2){

              return 270
       }else{
        var height : CGFloat = 0
        let dd = myCatDB.count / 2
        let ff = myCatDB.count % 2
        if(ff == 0){
            height = CGFloat(dd * 70)
        }else{
            height = CGFloat((dd+1) * 70)
        }
        
        
        return height
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


struct FilterStruct {

    static var gender = String()

    //
    static var ageRange = String()
    static var category = [CategoryCountryListModel]()
    static var categoryList = String()
    static var startDate = String()
    static var endDate = String()
    
    static var startDateFo = String()
    static var endDateFo = String()


}
