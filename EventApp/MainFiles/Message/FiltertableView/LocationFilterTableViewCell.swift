//
//  LocationFilterTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 09/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class LocationFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var btnLoc: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
