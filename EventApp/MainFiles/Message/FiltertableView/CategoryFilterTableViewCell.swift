//
//  CategoryFilterTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 09/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class CategoryFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: DynamicHeightCollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
