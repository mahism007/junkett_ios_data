//
//  ModelActivity.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 04/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import Foundation
import CoreData

class UpcomingActivityModel: NSObject {

var CDate = String()

var Latitude = String()

var Description = String()

var PlaceName = String()

var MonthRepeateDate = String()

var State = String()

var Pincode = String()

var UBy = String()

var Gender = String()

var CBy = String()

var City = String()

var AgeRange = String()

var UDate = String()

var ID = String()

var Address = String()

var StartDateTime = String()

var IsRepeated = String()

var RepeateMode = String()

var Title = String()

var EndDateTime = String()

var Country = String()

var Longitude = String()

var WeekRepeateDays = String()

func setDataInModel(cell:[String:AnyObject])
{

if cell["CDate"] is NSNull || cell["CDate"] == nil {
 self.CDate = ""
}else{
let app = cell["CDate"]
self.CDate = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["Description"] is NSNull || cell["Description"] == nil {
 self.Description = ""
}else{
let app = cell["Description"]
self.Description = (app?.description)!
}

if cell["PlaceName"] is NSNull || cell["PlaceName"] == nil {
 self.PlaceName = ""
}else{
let app = cell["PlaceName"]
self.PlaceName = (app?.description)!
}

if cell["MonthRepeateDate"] is NSNull || cell["MonthRepeateDate"] == nil {
 self.MonthRepeateDate = ""
}else{
let app = cell["MonthRepeateDate"]
self.MonthRepeateDate = (app?.description)!
}

if cell["State"] is NSNull || cell["State"] == nil {
 self.State = ""
}else{
let app = cell["State"]
self.State = (app?.description)!
}

if cell["Pincode"] is NSNull || cell["Pincode"] == nil {
 self.Pincode = ""
}else{
let app = cell["Pincode"]
self.Pincode = (app?.description)!
}

if cell["UBy"] is NSNull || cell["UBy"] == nil {
 self.UBy = ""
}else{
let app = cell["UBy"]
self.UBy = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["CBy"] is NSNull || cell["CBy"] == nil {
 self.CBy = ""
}else{
let app = cell["CBy"]
self.CBy = (app?.description)!
}

if cell["City"] is NSNull || cell["City"] == nil {
 self.City = ""
}else{
let app = cell["City"]
self.City = (app?.description)!
}

if cell["AgeRange"] is NSNull || cell["AgeRange"] == nil {
 self.AgeRange = ""
}else{
let app = cell["AgeRange"]
self.AgeRange = (app?.description)!
}

if cell["UDate"] is NSNull || cell["UDate"] == nil {
 self.UDate = ""
}else{
let app = cell["UDate"]
self.UDate = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["Address"] is NSNull || cell["Address"] == nil {
 self.Address = ""
}else{
let app = cell["Address"]
self.Address = (app?.description)!
}

if cell["StartDateTime"] is NSNull || cell["StartDateTime"] == nil {
 self.StartDateTime = ""
}else{
let app = cell["StartDateTime"]
self.StartDateTime = (app?.description)!
}

if cell["IsRepeated"] is NSNull || cell["IsRepeated"] == nil {
 self.IsRepeated = ""
}else{
let app = cell["IsRepeated"]
self.IsRepeated = (app?.description)!
}

if cell["RepeateMode"] is NSNull || cell["RepeateMode"] == nil {
 self.RepeateMode = ""
}else{
let app = cell["RepeateMode"]
self.RepeateMode = (app?.description)!
}

if cell["Title"] is NSNull || cell["Title"] == nil {
 self.Title = ""
}else{
let app = cell["Title"]
self.Title = (app?.description)!
}

if cell["EndDateTime"] is NSNull || cell["EndDateTime"] == nil {
 self.EndDateTime = ""
}else{
let app = cell["EndDateTime"]
self.EndDateTime = (app?.description)!
}

if cell["Country"] is NSNull || cell["Country"] == nil {
 self.Country = ""
}else{
let app = cell["Country"]
self.Country = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}

if cell["WeekRepeateDays"] is NSNull || cell["WeekRepeateDays"] == nil {
 self.WeekRepeateDays = ""
}else{
let app = cell["WeekRepeateDays"]
self.WeekRepeateDays = (app?.description)!
}

}
}


class UpcomingActivityModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: ActivityShowViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityUpcomming, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [UpcomingActivityModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = UpcomingActivityModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class ActivityPopularListModel: NSObject {

var CategoryNames = String()

var Title = String()

var StartDateTime = String()

var Latitude = String()

var CBy = String()

var Distance = String()

var ActivityShareLink = String()

var EndDateTime = String()

var CreatedBy = String()

var IsMember = String()

var ID = String()

var AgeRange = String()

var PlaceName = String()

var MyFavorite = String()

var Gender = String()

var Description = String()

var FullAddress = String()

var IsRepeated = String()

var Longitude = String()
    var MateCount = String()
var Img = String()
    var ActImg = String()

    var MatesImg = String()
    var listFromProfilePic = [ImgModel]()
    var listFromMatesProfilePic = [ImgModel]()
    var listFromActPic = [ImgModel]()
func setDataInModel(cell:[String:AnyObject])
{
        if let data1 = cell["ActivityPicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                if(dataArray1.count > 0){
    //            for i in 0...dataArray1.count - 1 {
    //                if(dataArray1.count == 1){
    //                    self.Img = dataArray1[i].Img
    //                }else{
    //                self.Img = dataArray1[i].Img + "," +  self.Img
    //                }
    //            }
                    self.ActImg = dataArray1[0].Img
                }
                
                  listFromActPic = dataArray1
                  
              }
              
          }
    if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
//            for i in 0...dataArray1.count - 1 {
//                if(dataArray1.count == 1){
//                    self.Img = dataArray1[i].Img
//                }else{
//                self.Img = dataArray1[i].Img + "," +  self.Img
//                }
//            }
                self.Img = dataArray1[0].Img
            }
            
              listFromProfilePic = dataArray1
              
          }
          
      }
    if let data1 = cell["MatesProfilePic"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
            for i in 0...dataArray1.count - 1 {
                if(dataArray1.count == 1){
                    self.MatesImg = dataArray1[i].Img
                }else{
                self.MatesImg = dataArray1[i].Img + "," +  self.MatesImg
                }
            }
            }
            
              listFromMatesProfilePic = dataArray1
              
          }
          
      }
    if cell["MateCount"] is NSNull || cell["MateCount"] == nil {
     self.MateCount = ""
    }else{
    let app = cell["MateCount"]
    self.MateCount = (app?.description)!
    }
if cell["CategoryNames"] is NSNull || cell["CategoryNames"] == nil {
 self.CategoryNames = ""
}else{
let app = cell["CategoryNames"]
self.CategoryNames = (app?.description)!
}

if cell["Title"] is NSNull || cell["Title"] == nil {
 self.Title = ""
}else{
let app = cell["Title"]
self.Title = (app?.description)!
}

if cell["StartDateTime"] is NSNull || cell["StartDateTime"] == nil {
 self.StartDateTime = ""
}else{
let app = cell["StartDateTime"]
self.StartDateTime = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["CBy"] is NSNull || cell["CBy"] == nil {
 self.CBy = ""
}else{
let app = cell["CBy"]
self.CBy = (app?.description)!
}

if cell["Distance"] is NSNull || cell["Distance"] == nil {
 self.Distance = ""
}else{
let app = cell["Distance"]
self.Distance = (app?.description)!
}

if cell["ActivityShareLink"] is NSNull || cell["ActivityShareLink"] == nil {
 self.ActivityShareLink = ""
}else{
let app = cell["ActivityShareLink"]
self.ActivityShareLink = (app?.description)!
}

if cell["EndDateTime"] is NSNull || cell["EndDateTime"] == nil {
 self.EndDateTime = ""
}else{
let app = cell["EndDateTime"]
self.EndDateTime = (app?.description)!
}

if cell["CreatedBy"] is NSNull || cell["CreatedBy"] == nil {
 self.CreatedBy = ""
}else{
let app = cell["CreatedBy"]
self.CreatedBy = (app?.description)!
}

if cell["IsMember"] is NSNull || cell["IsMember"] == nil {
 self.IsMember = ""
}else{
let app = cell["IsMember"]
self.IsMember = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["AgeRange"] is NSNull || cell["AgeRange"] == nil {
 self.AgeRange = ""
}else{
let app = cell["AgeRange"]
self.AgeRange = (app?.description)!
}

if cell["PlaceName"] is NSNull || cell["PlaceName"] == nil {
 self.PlaceName = ""
}else{
let app = cell["PlaceName"]
self.PlaceName = (app?.description)!
}

if cell["MyFavorite"] is NSNull || cell["MyFavorite"] == nil {
 self.MyFavorite = ""
}else{
let app = cell["MyFavorite"]
self.MyFavorite = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["Description"] is NSNull || cell["Description"] == nil {
 self.Description = ""
}else{
let app = cell["Description"]
self.Description = (app?.description)!
}

if cell["FullAddress"] is NSNull || cell["FullAddress"] == nil {
 self.FullAddress = ""
}else{
let app = cell["FullAddress"]
self.FullAddress = (app?.description)!
}

if cell["IsRepeated"] is NSNull || cell["IsRepeated"] == nil {
 self.IsRepeated = ""
}else{
let app = cell["IsRepeated"]
self.IsRepeated = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}
    self.saveActPathData( CategoryNames : CategoryNames,Title : Title,StartDateTime : StartDateTime,Latitude : Latitude,CBy : CBy, Distance : Distance,ActivityShareLink : ActivityShareLink,EndDateTime : EndDateTime,CreatedBy : CreatedBy,IsMember : IsMember,ID : ID,AgeRange : AgeRange,PlaceName : PlaceName,MyFavorite : MyFavorite,Gender : Gender,Description : Description,FullAddress : FullAddress,IsRepeated : IsRepeated,Longitude : Longitude,Img : Img,MatesImg : MatesImg,MateCount:MateCount,ActImg:ActImg)
}
    
    func saveActPathData( CategoryNames : String,Title : String,StartDateTime : String,Latitude : String,CBy : String, Distance : String,ActivityShareLink : String,EndDateTime : String,CreatedBy : String,IsMember : String,ID : String,AgeRange : String,PlaceName : String,MyFavorite : String,Gender : String,Description : String,FullAddress : String,IsRepeated : String,Longitude : String,Img : String,MatesImg : String,MateCount:String,ActImg:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = ActivityPopularModel(context: context)
        
        tasks.categoryNames = CategoryNames
        tasks.title = Title
        tasks.startDateTime = StartDateTime
        tasks.endDateTime = EndDateTime
        tasks.latitude = Latitude
        tasks.cBy = CBy
        tasks.distance = Distance
        tasks.actImg = ActImg
        tasks.activityShareLink = ActivityShareLink
        tasks.age = AgeRange
        tasks.placeName = PlaceName
        
        tasks.myFavorite = MyFavorite
        tasks.gender = Gender
        tasks.desc = Description
        
        tasks.fullAddress = FullAddress
        tasks.isRepeated = IsRepeated
        tasks.longitude = Longitude
        tasks.mateCount = MateCount
        tasks.isMember = IsMember
        tasks.id = ID
        tasks.createdBy = CreatedBy
        tasks.img = Img
        tasks.mateImg = MatesImg
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
}


class ActPopularModelDataAPI
{
    func deleteActPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ActivityPopularModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }

    var reachablty = Reachability()!
    
    func serviceCalling(obj: ActivityShowViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                    self.deleteActPathData()
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [ActivityPopularListModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = ActivityPopularListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ActPopularLocModelDataAPI
{
    func deleteActPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ActivityPopularModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }

    var reachablty = Reachability()!
    
    func serviceCalling(obj: ActivityByLocationViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                    self.deleteActPathData()
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [ActivityPopularListModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = ActivityPopularListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ActPopularLocDataModelDataAPI
{
    func deleteActPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ActivityPopularModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }

    var reachablty = Reachability()!
    
    func serviceCalling(obj: MessageViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                    self.deleteActPathData()
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [ActivityPopularListModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = ActivityPopularListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ActivityNearListModel: NSObject {

var CategoryNames = String()

var Title = String()

var StartDateTime = String()

var Latitude = String()

var CBy = String()

var Distance = String()

var ActivityShareLink = String()

var EndDateTime = String()

var CreatedBy = String()

var IsMember = String()

var ID = String()

var AgeRange = String()

var PlaceName = String()

var MyFavorite = String()

var Gender = String()

var Description = String()

var FullAddress = String()

var IsRepeated = String()

var Longitude = String()
    var MateCount = String()
var Img = String()
    var ActImg = String()

    var MatesImg = String()
    var listFromProfilePic = [ImgModel]()
    var listFromMatesProfilePic = [ImgModel]()
    var listFromActPic = [ImgModel]()
func setDataInModel(cell:[String:AnyObject])
{
        if let data1 = cell["ActivityPicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                if(dataArray1.count > 0){
    //            for i in 0...dataArray1.count - 1 {
    //                if(dataArray1.count == 1){
    //                    self.Img = dataArray1[i].Img
    //                }else{
    //                self.Img = dataArray1[i].Img + "," +  self.Img
    //                }
    //            }
                    self.ActImg = dataArray1[0].Img
                }
                
                  listFromActPic = dataArray1
                  
              }
              
          }
    if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
//            for i in 0...dataArray1.count - 1 {
//                if(dataArray1.count == 1){
//                    self.Img = dataArray1[i].Img
//                }else{
//                self.Img = dataArray1[i].Img + "," +  self.Img
//                }
//            }
                self.Img = dataArray1[0].Img
            }
            
              listFromProfilePic = dataArray1
              
          }
          
      }
    if let data1 = cell["MatesProfilePic"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
            for i in 0...dataArray1.count - 1 {
                if(dataArray1.count == 1){
                    self.MatesImg = dataArray1[i].Img
                }else{
                self.MatesImg = dataArray1[i].Img + "," +  self.MatesImg
                }
            }
            }
            
              listFromMatesProfilePic = dataArray1
              
          }
          
      }
    if cell["MateCount"] is NSNull || cell["MateCount"] == nil {
     self.MateCount = ""
    }else{
    let app = cell["MateCount"]
    self.MateCount = (app?.description)!
    }
if cell["CategoryNames"] is NSNull || cell["CategoryNames"] == nil {
 self.CategoryNames = ""
}else{
let app = cell["CategoryNames"]
self.CategoryNames = (app?.description)!
}

if cell["Title"] is NSNull || cell["Title"] == nil {
 self.Title = ""
}else{
let app = cell["Title"]
self.Title = (app?.description)!
}

if cell["StartDateTime"] is NSNull || cell["StartDateTime"] == nil {
 self.StartDateTime = ""
}else{
let app = cell["StartDateTime"]
self.StartDateTime = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["CBy"] is NSNull || cell["CBy"] == nil {
 self.CBy = ""
}else{
let app = cell["CBy"]
self.CBy = (app?.description)!
}

if cell["Distance"] is NSNull || cell["Distance"] == nil {
 self.Distance = ""
}else{
let app = cell["Distance"]
self.Distance = (app?.description)!
}

if cell["ActivityShareLink"] is NSNull || cell["ActivityShareLink"] == nil {
 self.ActivityShareLink = ""
}else{
let app = cell["ActivityShareLink"]
self.ActivityShareLink = (app?.description)!
}

if cell["EndDateTime"] is NSNull || cell["EndDateTime"] == nil {
 self.EndDateTime = ""
}else{
let app = cell["EndDateTime"]
self.EndDateTime = (app?.description)!
}

if cell["CreatedBy"] is NSNull || cell["CreatedBy"] == nil {
 self.CreatedBy = ""
}else{
let app = cell["CreatedBy"]
self.CreatedBy = (app?.description)!
}

if cell["IsMember"] is NSNull || cell["IsMember"] == nil {
 self.IsMember = ""
}else{
let app = cell["IsMember"]
self.IsMember = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["AgeRange"] is NSNull || cell["AgeRange"] == nil {
 self.AgeRange = ""
}else{
let app = cell["AgeRange"]
self.AgeRange = (app?.description)!
}

if cell["PlaceName"] is NSNull || cell["PlaceName"] == nil {
 self.PlaceName = ""
}else{
let app = cell["PlaceName"]
self.PlaceName = (app?.description)!
}

if cell["MyFavorite"] is NSNull || cell["MyFavorite"] == nil {
 self.MyFavorite = ""
}else{
let app = cell["MyFavorite"]
self.MyFavorite = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["Description"] is NSNull || cell["Description"] == nil {
 self.Description = ""
}else{
let app = cell["Description"]
self.Description = (app?.description)!
}

if cell["FullAddress"] is NSNull || cell["FullAddress"] == nil {
 self.FullAddress = ""
}else{
let app = cell["FullAddress"]
self.FullAddress = (app?.description)!
}

if cell["IsRepeated"] is NSNull || cell["IsRepeated"] == nil {
 self.IsRepeated = ""
}else{
let app = cell["IsRepeated"]
self.IsRepeated = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}
    self.saveActPathData( CategoryNames : CategoryNames,Title : Title,StartDateTime : StartDateTime,Latitude : Latitude,CBy : CBy, Distance : Distance,ActivityShareLink : ActivityShareLink,EndDateTime : EndDateTime,CreatedBy : CreatedBy,IsMember : IsMember,ID : ID,AgeRange : AgeRange,PlaceName : PlaceName,MyFavorite : MyFavorite,Gender : Gender,Description : Description,FullAddress : FullAddress,IsRepeated : IsRepeated,Longitude : Longitude,Img : Img,MatesImg : MatesImg,MateCount:MateCount,ActImg:ActImg)
}
    
    func saveActPathData( CategoryNames : String,Title : String,StartDateTime : String,Latitude : String,CBy : String, Distance : String,ActivityShareLink : String,EndDateTime : String,CreatedBy : String,IsMember : String,ID : String,AgeRange : String,PlaceName : String,MyFavorite : String,Gender : String,Description : String,FullAddress : String,IsRepeated : String,Longitude : String,Img : String,MatesImg : String,MateCount:String,ActImg:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = ActivityNearModel(context: context)
        
        tasks.categoryNames = CategoryNames
        tasks.title = Title
        tasks.startDateTime = StartDateTime
        tasks.endDateTime = EndDateTime
        tasks.latitude = Latitude
        tasks.cBy = CBy
        tasks.distance = Distance
        tasks.actImg = ActImg
        tasks.activityShareLink = ActivityShareLink
        tasks.age = AgeRange
        tasks.placeName = PlaceName
        
        tasks.myFavorite = MyFavorite
        tasks.gender = Gender
        tasks.desc = Description
        
        tasks.fullAddress = FullAddress
        tasks.isRepeated = IsRepeated
        tasks.longitude = Longitude
        tasks.mateCount = MateCount
        tasks.isMember = IsMember
        tasks.id = ID
        tasks.createdBy = CreatedBy
        tasks.img = Img
        tasks.mateImg = MatesImg
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
}
class ActNearLocModelDataAPI
{
    func deleteActPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ActivityNearModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }

    var reachablty = Reachability()!
    
    func serviceCalling(obj: ActivityByLocationViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                    self.deleteActPathData()
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [ActivityNearListModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = ActivityNearListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ActNearLocDataModelDataAPI
{
    func deleteActPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "ActivityNearModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }

    var reachablty = Reachability()!
    
    func serviceCalling(obj: MessageViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityListFilter, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                    self.deleteActPathData()
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [ActivityNearListModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = ActivityNearListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class MateListModel: NSObject {

var MateName = String()

var ID = String()


    var listFromProfilePic = [ImgModel]()

func setDataInModel(cell:[String:AnyObject])
{
        if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }

                
                  listFromProfilePic = dataArray1
                  
              }
              
          }

    if cell["MateName"] is NSNull || cell["MateName"] == nil {
     self.MateName = ""
    }else{
    let app = cell["MateName"]
    self.MateName = (app?.description)!
    }
if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}


}
    

}



class ActivityDetailsListModel: NSObject {

var CategoryNames = String()

var Title = String()

var StartDateTime = String()

var Latitude = String()

var CBy = String()

var Distance = String()

var ActivityShareLink = String()

var EndDateTime = String()

var CreatedBy = String()

var IsMember = String()

var ID = String()

var AgeRange = String()

var PlaceName = String()

var MyFavorite = String()

var Gender = String()

var Description = String()

var FullAddress = String()

var IsRepeated = String()

var Longitude = String()
    var MateCount = String()
var Img = String()
    var ActImg = String()

    var MatesImg = String()
    var listFromProfilePic = [ImgModel]()
    var listFromMatesProfilePic = [ImgModel]()
    var listFromActPic = [ImgModel]()
    var listMates = [MateListModel]()
func setDataInModel(cell:[String:AnyObject])
{
        if let data1 = cell["MateList"] as? [AnyObject]
          {
              
              var dataArray1: [MateListModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = MateListModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }

                
                  listMates = dataArray1
                  
              }
              
          }
    
    
    
        if let data1 = cell["ActivityPicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                if(dataArray1.count > 0){
    //            for i in 0...dataArray1.count - 1 {
    //                if(dataArray1.count == 1){
    //                    self.Img = dataArray1[i].Img
    //                }else{
    //                self.Img = dataArray1[i].Img + "," +  self.Img
    //                }
    //            }
                    self.ActImg = dataArray1[0].Img
                }
                
                  listFromActPic = dataArray1
                  
              }
              
          }
    if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
//            for i in 0...dataArray1.count - 1 {
//                if(dataArray1.count == 1){
//                    self.Img = dataArray1[i].Img
//                }else{
//                self.Img = dataArray1[i].Img + "," +  self.Img
//                }
//            }
                self.Img = dataArray1[0].Img
            }
            
              listFromProfilePic = dataArray1
              
          }
          
      }
    if let data1 = cell["MatesProfilePic"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
            if(dataArray1.count > 0){
            for i in 0...dataArray1.count - 1 {
                if(dataArray1.count == 1){
                    self.MatesImg = dataArray1[i].Img
                }else{
                self.MatesImg = dataArray1[i].Img + "," +  self.MatesImg
                }
            }
            }
            
              listFromMatesProfilePic = dataArray1
              
          }
          
      }
    if cell["MateCount"] is NSNull || cell["MateCount"] == nil {
     self.MateCount = ""
    }else{
    let app = cell["MateCount"]
    self.MateCount = (app?.description)!
    }
if cell["CategoryNames"] is NSNull || cell["CategoryNames"] == nil {
 self.CategoryNames = ""
}else{
let app = cell["CategoryNames"]
self.CategoryNames = (app?.description)!
}

if cell["Title"] is NSNull || cell["Title"] == nil {
 self.Title = ""
}else{
let app = cell["Title"]
self.Title = (app?.description)!
}

if cell["StartDateTime"] is NSNull || cell["StartDateTime"] == nil {
 self.StartDateTime = ""
}else{
let app = cell["StartDateTime"]
self.StartDateTime = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["CBy"] is NSNull || cell["CBy"] == nil {
 self.CBy = ""
}else{
let app = cell["CBy"]
self.CBy = (app?.description)!
}

if cell["Distance"] is NSNull || cell["Distance"] == nil {
 self.Distance = ""
}else{
let app = cell["Distance"]
self.Distance = (app?.description)!
}

if cell["ActivityShareLink"] is NSNull || cell["ActivityShareLink"] == nil {
 self.ActivityShareLink = ""
}else{
let app = cell["ActivityShareLink"]
self.ActivityShareLink = (app?.description)!
}

if cell["EndDateTime"] is NSNull || cell["EndDateTime"] == nil {
 self.EndDateTime = ""
}else{
let app = cell["EndDateTime"]
self.EndDateTime = (app?.description)!
}

if cell["CreatedBy"] is NSNull || cell["CreatedBy"] == nil {
 self.CreatedBy = ""
}else{
let app = cell["CreatedBy"]
self.CreatedBy = (app?.description)!
}

if cell["IsMember"] is NSNull || cell["IsMember"] == nil {
 self.IsMember = ""
}else{
let app = cell["IsMember"]
self.IsMember = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["AgeRange"] is NSNull || cell["AgeRange"] == nil {
 self.AgeRange = ""
}else{
let app = cell["AgeRange"]
self.AgeRange = (app?.description)!
}

if cell["PlaceName"] is NSNull || cell["PlaceName"] == nil {
 self.PlaceName = ""
}else{
let app = cell["PlaceName"]
self.PlaceName = (app?.description)!
}

if cell["MyFavorite"] is NSNull || cell["MyFavorite"] == nil {
 self.MyFavorite = ""
}else{
let app = cell["MyFavorite"]
self.MyFavorite = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["Description"] is NSNull || cell["Description"] == nil {
 self.Description = ""
}else{
let app = cell["Description"]
self.Description = (app?.description)!
}

if cell["FullAddress"] is NSNull || cell["FullAddress"] == nil {
 self.FullAddress = ""
}else{
let app = cell["FullAddress"]
self.FullAddress = (app?.description)!
}

if cell["IsRepeated"] is NSNull || cell["IsRepeated"] == nil {
 self.IsRepeated = ""
}else{
let app = cell["IsRepeated"]
self.IsRepeated = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}
  
}
    

}
class ActDetailModelDataAPI
{


    var reachablty = Reachability()!
    
    func serviceCalling(obj: ActivityPageViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ActivityDetail, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                  
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                                
                                var dataArray: [ActivityDetailsListModel] = []
                                
                                   
                                        
                                        if let str = data as? [String:AnyObject]
                                        {
                                            let object1 = ActivityDetailsListModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    //obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
