//
//  ModelProfile.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 30/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import Foundation
import CoreData

class ProfileDataListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: ProfileViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.GetMyProfile, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ProfileListModel] = []
                            
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ProfileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ProfileChatDataListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: ChatMessageViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
//       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.GetOthersProfile, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ProfileListModel] = []
                            
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ProfileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class ProfileDataDetailsListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: ProfileDetailsViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.GetMyProfile, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ProfileListModel] = []
                            
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ProfileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class ProfileDataListOtherModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: ProfileDetailsViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.GetOthersProfile, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ProfileListModel] = []
                            
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ProfileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class MessageSessionModel: NSObject {

var MessageDateTime = String()

var SendBy = String()

var ID = String()

var SendByID = String()

var Message = String()

var SendTo = String()

var SendToID = String()

var UnseenMessageCount = String()
    var listFromProfilePic = [ImgModel]()

func setDataInModel(cell:[String:AnyObject])
{
    if let data1 = cell["FromUserPic"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
              listFromProfilePic = dataArray1
              
          }
          
      }
if cell["MessageDateTime"] is NSNull || cell["MessageDateTime"] == nil {
 self.MessageDateTime = ""
}else{
let app = cell["MessageDateTime"]
self.MessageDateTime = (app?.description)!
}

if cell["SendBy"] is NSNull || cell["SendBy"] == nil {
 self.SendBy = ""
}else{
let app = cell["SendBy"]
self.SendBy = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["SendByID"] is NSNull || cell["SendByID"] == nil {
 self.SendByID = ""
}else{
let app = cell["SendByID"]
self.SendByID = (app?.description)!
}

if cell["Message"] is NSNull || cell["Message"] == nil {
 self.Message = ""
}else{
let app = cell["Message"]
self.Message = (app?.description)!
}

if cell["SendTo"] is NSNull || cell["SendTo"] == nil {
 self.SendTo = ""
}else{
let app = cell["SendTo"]
self.SendTo = (app?.description)!
}

if cell["SendToID"] is NSNull || cell["SendToID"] == nil {
 self.SendToID = ""
}else{
let app = cell["SendToID"]
self.SendToID = (app?.description)!
}

if cell["UnseenMessageCount"] is NSNull || cell["UnseenMessageCount"] == nil {
 self.UnseenMessageCount = ""
}else{
let app = cell["UnseenMessageCount"]
self.UnseenMessageCount = (app?.description)!
}

}
}


class MessageSessionModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: MessageNewViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ChattingSession, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [MessageSessionModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = MessageSessionModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class SuggestedMatesModel: NSObject {

var ID = String()

var FName = String()

var MName = String()

var LName = String()
    var Img = String()
        var listFromProfilePic = [ImgModel]()

    func setDataInModel(cell:[String:AnyObject])
    {
        if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                  listFromProfilePic = dataArray1
                self.Img = listFromProfilePic[0].Img
              }
              
          }

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["FName"] is NSNull || cell["FName"] == nil {
 self.FName = ""
}else{
let app = cell["FName"]
self.FName = (app?.description)!
}

if cell["MName"] is NSNull || cell["MName"] == nil {
 self.MName = ""
}else{
let app = cell["MName"]
self.MName = (app?.description)!
}

if cell["LName"] is NSNull || cell["LName"] == nil {
 self.LName = ""
}else{
let app = cell["LName"]
self.LName = (app?.description)!
}


}
    
    

}

class SuggestMatesModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: MyMatesViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.SuggestedMatesList, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [SuggestedMatesModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = SuggestedMatesModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class SuggestAllMatesModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: AllSuggestedMatesViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.SuggestedMatesList, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [SuggestedMatesModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = SuggestedMatesModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class SuggestAllMatesModelDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:AllSuggestedMatesViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
       // obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.SuggestedMatesList, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [SuggestedMatesModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = SuggestedMatesModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
class BlockMatesModel: NSObject {

var ID = String()

var FName = String()

var MName = String()

var LName = String()
    var Img = String()
        var listFromProfilePic = [ImgModel]()

    func setDataInModel(cell:[String:AnyObject])
    {
        if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                  listFromProfilePic = dataArray1
                self.Img = listFromProfilePic[0].Img
              }
              
          }

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["FName"] is NSNull || cell["FName"] == nil {
 self.FName = ""
}else{
let app = cell["FName"]
self.FName = (app?.description)!
}

if cell["MName"] is NSNull || cell["MName"] == nil {
 self.MName = ""
}else{
let app = cell["MName"]
self.MName = (app?.description)!
}

if cell["LName"] is NSNull || cell["LName"] == nil {
 self.LName = ""
}else{
let app = cell["LName"]
self.LName = (app?.description)!
}
                let fullName =  self.FName + " " +  self.LName
                self.saveActPathData( Names : fullName,id : self.ID,Img : self.Img,status : "")

}
    func saveActPathData( Names : String,id : String,Img : String,status : String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = BLockedUserModel(context: context)
        
        tasks.name = Names
        tasks.id = id
        tasks.img = Img
        tasks.status = status

        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
}
class BlockMatesModelDataAPI
{
    func deleteBlockPathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "BLockedUserModel")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    var reachablty = Reachability()!
    
    func serviceCalling(obj: BlockedUserViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.BlockUserList, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                self.deleteBlockPathData()
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [BlockMatesModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = BlockMatesModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class BlockModelDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:BlockedUserViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
       // obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.BlockUserList, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [BlockMatesModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = BlockMatesModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}


class InviteMatesModel: NSObject {

var ID = String()

var FName = String()

var MName = String()

var LName = String()
        var listFromProfilePic = [ImgModel]()

    func setDataInModel(cell:[String:AnyObject])
    {
        if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
          {
              
              var dataArray1: [ImgModel] = []
              if(data1.count  >  0){
                  for i in 0..<data1.count
                  {
                      
                      if let str = data1[i] as? [String:AnyObject]
                      {
                          let object1 = ImgModel()
                          object1.setDataInModel(cell: str)
                          dataArray1.append(object1)
                      }
                      
                  }
                  listFromProfilePic = dataArray1
                  
              }
              
          }

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["FName"] is NSNull || cell["FName"] == nil {
 self.FName = ""
}else{
let app = cell["FName"]
self.FName = (app?.description)!
}

if cell["MName"] is NSNull || cell["MName"] == nil {
 self.MName = ""
}else{
let app = cell["MName"]
self.MName = (app?.description)!
}

if cell["LName"] is NSNull || cell["LName"] == nil {
 self.LName = ""
}else{
let app = cell["LName"]
self.LName = (app?.description)!
}

}
}
class InviteMatesModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: InviteListViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.MyActivityInviteList, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]  as? [AnyObject]
                        {
                                
                                var dataArray: [InviteMatesModel] = []
                                if(data.count  >  0){
                                    for i in 0..<data.count
                                    {
                                        
                                        if let str = data[i] as? [String:AnyObject]
                                        {
                                            let object1 = InviteMatesModel()
                                            object1.setDataInModel(cell: str)
                                            dataArray.append(object1)
                                        }
                                        
                                    }
                            
                            
                            
                       
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
                       obj.showError(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                   }else{
                    obj.showError(strMessage: msg)
                    obj.stopLoading()
                }
                
                }
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class InviteModelDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:InviteListViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
       // obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyActivityInviteList, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [InviteMatesModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = InviteMatesModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}





class ChatMessageEmpLoadDataModel: NSObject {
    
    
    var ID = Int()
    
    var SenderName = String()
    var IsMe = String()
    var MePic = String()
    var OtherPic = String()
    var FromID = String()
    var ToID = String()
    var ToType = String()
    var Message = String()
    var Image = String()
    var CreatedDate = String()
    var DeletedStatus = String()
    var HUID = String()
    var ChatSessionID = String()
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["SenderName"] is NSNull || str["SenderName"] == nil{
            self.SenderName = ""
        }else{
            
            let ros = str["SenderName"]
            
            self.SenderName = (ros?.description)!
            
            
        }
        if str["IsMe"] is NSNull || str["IsMe"] == nil{
            self.IsMe = ""
        }else{
            
            let ros = str["IsMe"]
            
            self.IsMe = (ros?.description)!
            
            
        }
        if str["MePic"] is NSNull || str["MePic"] == nil{
            self.MePic = ""
        }else{
            
            let ros = str["MePic"]
            
            self.MePic = (ros?.description)!
            
            
        }
        
        
        if str["OtherPic"] is NSNull || str["OtherPic"] == nil{
            self.OtherPic = ""
        }else{
            
            let ros = str["OtherPic"]
            
            self.OtherPic = (ros?.description)!
            
            
        }
        
        if str["FromID"] is NSNull || str["FromID"] == nil{
            self.FromID = ""
        }else{
            
            let ros = str["FromID"]
            
            self.FromID = (ros?.description)!
            
            
        }
        if str["ToID"] is NSNull || str["ToID"] == nil{
            self.ToID = ""
        }else{
            
            let ros = str["ToID"]
            
            self.ToID = (ros?.description)!
            
            
        }
        if str["Message"] is NSNull || str["Message"] == nil{
            self.Message = ""
        }else{
            
            let ros = str["Message"]
            
            self.Message = (ros?.description)!
            
            
        }
        if str["Image"] is NSNull || str["Image"] == nil{
            self.Image = ""
        }else{
            
            let ros = str["Image"]
            
            self.Image = (ros?.description)!
            
            
        }
        if str["CreatedDate"] is NSNull || str["CreatedDate"] == nil{
            self.CreatedDate = ""
        }else{
            
            let ros = str["CreatedDate"]
            
            self.CreatedDate = (ros?.description)!
            
            
        }
        if str["DeletedStatus"] is NSNull || str["DeletedStatus"] == nil{
            self.DeletedStatus = ""
        }else{
            
            let ros = str["DeletedStatus"]
            
            self.DeletedStatus = (ros?.description)!
            
            
        }
      
        if str["HUID"] is NSNull || str["HUID"] == nil{
            self.HUID = ""
        }else{
            
            let ros = str["HUID"]
            
            self.HUID = (ros?.description)!
            
            
        }
        if str["ChatSessionID"] is NSNull || str["ChatSessionID"] == nil{
            self.ChatSessionID = ""
        }else{
            
            let ros = str["ChatSessionID"]
            
            self.ChatSessionID = (ros?.description)!
            
            
        }
    }
    
}



class ChatMessageEmpLoadInternalDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ChatMessageViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.ChattingMessage , parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ChatMessageEmpLoadDataModel] = []
                            if(data.count > 0){
                                
                                for i in 0..<data.count
                                {
                                    
                                    if let cell = data[i] as? [String:AnyObject]
                                    {
                                        let object = ChatMessageEmpLoadDataModel()
                                        object.setDataInModel(str: cell)
                                        dataArray.append(object)
                                    }
                                    
                                }
                            }else{
                                
                            }
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                
            }
            
            
        }
        else{
            
        }
        
        
    }
    
    
}
class ChatMessageEmpLoadDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:ChatMessageViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        //obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.ChattingMessage , parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                   //  obj.stopLoading()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                       
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [ChatMessageEmpLoadDataModel] = []
                            if(data.count > 0){
                                for i in 0..<data.count
                                {
                                    
                                    if let cell = data[i] as? [String:AnyObject]
                                    {
                                        let object = ChatMessageEmpLoadDataModel()
                                        object.setDataInModel(str: cell)
                                        dataArray.append(object)
                                    }
                                    
                                }
                            }else{
                              // obj.noDataFound(view : obj.tableView,message : msg)
                               // obj.stopLoading()
                            }
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                       //  obj.stopLoading()
                    }
                    else
                    {
                       
                      //  obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
            //  obj.stopLoading()
            }
            
            
        }
        else{
            
           // obj.stopLoading()
        }
        
        
    }
    
    
}
