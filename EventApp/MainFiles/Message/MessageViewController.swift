//
//  MessageViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 15/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreData
var seachh = String()
class   MessageViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    @IBOutlet weak var txtSearch: DesignableUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        CategoryData()
        CountryData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }

    
       var actPopularDB:[ActivityPopularListModel] = []
        var actPopularAPI = ActPopularLocDataModelDataAPI()
       @objc func getActivityData(){
    
                  var para = [String:String]()
                  
                  let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                  "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                  "PN":"0",
                  "TDate":FilterStruct.endDateFo,
                        "FDate":FilterStruct.startDateFo,
                        "Gender":FilterStruct.gender,
                        "AgeRange":FilterStruct.ageRange,
                        "CategoryCSV":FilterStruct.categoryList]
   
                  para = parameter.filter { $0.value != ""}
                  
                  actPopularAPI.serviceCalling(obj: self, parameter: para ) { (dict) in
                      
                      self.actPopularDB = dict as! [ActivityPopularListModel]
                     if(self.actPopularDB.count > 0 ){
                        // self.tableView.reloadData()
                       self.getActivityDataLocal(search: self.txtSearch.text!)
                     }
                      
                  }
              }
       
          var actNearDB:[ActivityNearListModel] = []
           var actNearAPI = ActNearLocDataModelDataAPI()
          @objc func getActivityNearData(){
       
                     var para = [String:String]()
                     
                     let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                                      "UserID": UserDefaults.standard.string(forKey: "UserID")!,"Type":"1000000",
                     "PN":"0",
            "TDate":FilterStruct.endDateFo,
                  "FDate":FilterStruct.startDateFo,
                  "Gender":FilterStruct.gender,
                  "AgeRange":FilterStruct.ageRange,
                  "CategoryCSV":FilterStruct.categoryList]
                     
                     para = parameter.filter { $0.value != ""}
                     
                     actNearAPI.serviceCalling(obj: self, parameter: para ) { (dict) in
                         
                         self.actNearDB = dict as! [ActivityNearListModel]
                        if(self.actNearDB.count > 0 ){
                           // self.tableView.reloadData()
                          self.getActivityNearDataLocal(search: self.txtSearch.text!)
                        }
                         
                     }
                 }

    
    
    
    var actDBLocal : [ActivityPopularModel] = []
          @objc func getActivityDataLocal(search : String) {
              if(search != ""){
                   
                            
                                    self.actDBLocal = [ActivityPopularModel]()
                                    let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                    
                                                       let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ActivityPopularModel")
                           
                                 myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                           
                                                      // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                                    
                                                       do{
                                                          let results = try moc.fetch(myRequest)
                                                           actDBLocal = results as! [ActivityPopularModel]
                                                            
                                                                self.tableView.reloadData()
                                                            
                                                           for result in results
                                                           {
                                                               print(result)
                                                           }
                                                        
                                                       } catch let error{
                                                           print(error)
                                                       }

               
              }else{
                 self.actDBLocal = [ActivityPopularModel]()
                                 do {
                         
                                     self.actDBLocal = try context.fetch(ActivityPopularModel.fetchRequest())
                                     //self.refresh.endRefreshing()
                                    
                                    if(self.actDBLocal.count > 0) {
                                       self.tableView.reloadData()
                                    }else{
                                        self.tableView.reloadData()
                                       // getReportedDirectoryData()
                                    }
                                    // self.tableView.reloadData()
                         
                                 } catch {
                                     print("Fetching Failed")
                                 }
                 
               //  self.tableView.reloadData()
              }
             }
    
    
    var actNearDBLocal : [ActivityNearModel] = []
          @objc func getActivityNearDataLocal(search : String) {
              if(search != ""){
                   
                            
                                    self.actNearDBLocal = [ActivityNearModel]()
                                    let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                                    
                                                       let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ActivityNearModel")
                           
                                 myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                           
                                                      // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                                    
                                                       do{
                                                          let results = try moc.fetch(myRequest)
                                                           actNearDBLocal = results as! [ActivityNearModel]
                                                            
                                                              self.tableView.reloadData()
                                                            
                                                           for result in results
                                                           {
                                                               print(result)
                                                           }
                                                        
                                                       } catch let error{
                                                           print(error)
                                                       }

               
              }else{
                 self.actNearDBLocal = [ActivityNearModel]()
                                 do {
                         
                                     self.actNearDBLocal = try context.fetch(ActivityNearModel.fetchRequest())
                                     //self.refresh.endRefreshing()
                                    
                                    if(self.actNearDBLocal.count > 0) {
                                       self.tableView.reloadData()
                                    }else{
                                        self.tableView.reloadData()
                                       // getReportedDirectoryData()
                                    }
                                    // self.tableView.reloadData()
                         
                                 } catch {
                                     print("Fetching Failed")
                                 }
                 
               //  self.tableView.reloadData()
              }
             }
    
    
    
    
    
  var categoryListAPI = CategoryListModelDataAPI()
       
       func CategoryData()  {
       
           
         categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
               categoryListDB = dict as! [CategoryCountryListModel]
               //self.InterestCollectionView.reloadData()
               
           }
           
           
       }
     var countryListAPI = CountryModelDataAPI()
     
     func CountryData()  {
       
         
       countryListAPI.serviceCalling( parameter : [:]  ) { (dict) in
             countryListDB = dict as! [CategoryCountryListModel]
             //self.InterestCollectionView.reloadData()
             
         }
         
         
     }
    var sea = String()
    @IBAction func textEditChanged(_ sender: UITextField) {
        seachh = txtSearch.text!
         self.getActivityDataLocal(search: txtSearch.text!)
        self.getActivityNearDataLocal(search: txtSearch.text!)
    }
    
    
    @IBAction func btnFilterClicked(_ sender: Any) {
                   let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "FilterSearchViewController") as! FilterSearchViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    
    @IBAction func btndismissToLocationClicked(_ sender: UIButton) {
        let transition = CATransition()
         transition.duration = 0.5
         transition.type = CATransitionType.fade
         transition.subtype = CATransitionSubtype.fromLeft
         transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
         view.window!.layer.add(transition, forKey: kCATransition)
         
         self.dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
                    
        txtSearch.text = seachh
                      
      getActivityData()
        getActivityNearData()
        
        
             self.tabBarController?.tabBar.isHidden = true
               self.navigationController?.navigationBar.isHidden = true
         

                 
                 
                 self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                 self.navigationController?.navigationBar.shadowImage = UIImage()
                 self.navigationController?.navigationBar.isTranslucent = true
                 self.navigationController?.view.backgroundColor = .clear
        
//         self.navigationController?.navigationBar.tintColor = .clear
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
         if(section == 0   ){
             
                 return 60
             
         }else {
             return 40
             
         }
     }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           
           let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableViewCell
        if section == 0 {
            header.lblHeader.text = "Popular Activities"
            header.downArrow.isHidden = false
            header.heightDown.constant = 15
        }else{
            header.lblHeader.text = "Nearby"
            header.downArrow.isHidden = true
            header.heightDown.constant = 0
        }

           
           
           
           return header
           
       }
       
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MessageTableViewCell
        
            cell.configureCell(actDB : actDBLocal ,obj:self)
            cell.collectionView.reloadData()
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNearby", for: indexPath) as! NearByTableViewCell
             cell.configureCell(actDB : actNearDBLocal ,obj:self)
             cell.collectionView.reloadData()
            
             return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 300
        }else{
            let wid = self.view.frame.width / 2
            var height = 0
            let dd = actNearDBLocal.count / 2
            let ff = actNearDBLocal.count % 2
                                    if(ff == 0){
                                        height = Int(CGFloat(CGFloat(dd) * wid))
                                    }else{
                                        height = Int(CGFloat(CGFloat((dd+1)) * wid))
                                    }
                                    
            
            
        return CGFloat(height)
        }
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
