//
//  SettingViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import EzPopup
class SettingViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        let objectData1 = profileDisplay(name: "Edit Profile", data: "")
              dataArray.append(objectData1)
        let objectData11 = profileDisplay(name: "Update Setting", data: "")
        dataArray.append(objectData11)
              let objectData2 = profileDisplay(name: "Terms and Condition", data: "")
              dataArray.append(objectData2)
              let objectData3 = profileDisplay(name: "Logout", data: "")
              dataArray.append(objectData3)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        
      
        
       
    }
   
    @IBAction func btnDeleteAccountClicked(_ sender: UIButton) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "DeleteAccountViewController") as! DeleteAccountViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 380 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnSettingDataClicked(_ sender: UIButton) {
        
        if(sender.tag == 3){

            
            let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
            let ZIVC = storyboard.instantiateViewController(withIdentifier: "LogoutViewController") as! LogoutViewController
                   
                   let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 380 )
            
                   popupVC.cornerRadius = 5
                   popupVC.canTapOutsideToDismiss = true
            
                   present(popupVC, animated: true, completion: nil)
            
            
            
            
        }else if(sender.tag == 0){
            let storyboard = UIStoryboard(name: "Register", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if(sender.tag == 1){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "UpdateSettingViewController") as! UpdateSettingViewController
            self.navigationController?.pushViewController(mainVC, animated: true)
        }
        
        
        
    }
    
    
    
    
    var dataArray : [profileDisplay] = []
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       self.tabBarController?.tabBar.isHidden = false
      
            self.navigationController?.navigationBar.isHidden = false
       //self.navigationController?.navigationBar.topItem?.title = "Setting"
            //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
              self.title = "Setting"
              
//              self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
//              self.navigationController?.navigationBar.shadowImage = UIImage()
//              self.navigationController?.navigationBar.isTranslucent = true
//              self.navigationController?.view.backgroundColor = .white
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if(section == 0){
            return dataArray.count
            
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArray[indexPath.row].name
            
            cell.switcher.isHidden = true
            cell.lblData.isHidden = true
            cell.imgArrow.isHidden = false
            cell.btnData.tag = indexPath.row
            return cell
        }else{
            let header = tableView.dequeueReusableCell(withIdentifier: "cellButton") as! HeadProfileTableViewCell
            
            return header
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 60
        }else {
            return 80
        }
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
