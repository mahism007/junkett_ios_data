//
//  ActivityShowViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 16/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreData
class ActivityShowViewController:  CommonClassViewController ,UITableViewDelegate, UITableViewDataSource ,UITextFieldDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var txtSearch: DesignableUITextField!
    var refreshControl = UIRefreshControl()
    
   var actPopularDB:[ActivityPopularListModel] = []
    var actPopularAPI = ActPopularModelDataAPI()
    override func viewDidLoad() {
        super.viewDidLoad()
        txtSearch.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
  var actDB:[UpcomingActivityModel] = []
   var actAPI = UpcomingActivityModelDataAPI()
    
    
    
    
    @objc func getActivityData(){
    
              
          

               var para = [String:String]()
               
               let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
               "UserID": UserDefaults.standard.string(forKey: "UserID")!,
               "PN":"0"]
               
               para = parameter.filter { $0.value != ""}
               
               actPopularAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                   
                   self.actPopularDB = dict as! [ActivityPopularListModel]
                  if(self.actPopularDB.count > 0 ){
                      self.tableView.reloadData()
                    self.getActivityDataLocal(search: self.txtSearch.text!)
                  }
                   
               }
           }
    
    @IBAction func textEditChanged(_ sender: UITextField) {
         self.getActivityDataLocal(search: txtSearch.text!)
    }
    @IBAction func txtSearchDataVlaueChanged(_ sender: DesignableUITextField) {
        self.getActivityDataLocal(search: txtSearch.text!)
    }
    var actDBLocal : [ActivityPopularModel] = []
    @objc func getActivityDataLocal(search : String) {
        if(search != ""){
             
                      
                              self.actDBLocal = [ActivityPopularModel]()
                              let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                              
                                                 let myRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ActivityPopularModel")
                     
                           myRequest.predicate = NSPredicate(format: "title CONTAINS[cd] %@ ",search)
                     
                                                // myRequest.predicate = NSPredicate(format: " name CONTAINS[cd] %@", searchText)
                              
                                                 do{
                                                    let results = try moc.fetch(myRequest)
                                                     actDBLocal = results as! [ActivityPopularModel]
                              
                                                     for result in results
                                                     {
                                                         print(result)
                                                     }
                                                  
                                                 } catch let error{
                                                     print(error)
                                                 }

                     self.tableView.reloadData()
        }else{
           self.actDBLocal = [ActivityPopularModel]()
                           do {
                   
                               self.actDBLocal = try context.fetch(ActivityPopularModel.fetchRequest())
                               //self.refresh.endRefreshing()
                              
                              if(self.actDBLocal.count > 0) {
                              }else{
                                 // getReportedDirectoryData()
                              }
                               self.tableView.reloadData()
                   
                           } catch {
                               print("Fetching Failed")
                           }
           
           self.tableView.reloadData()
        }
       }
    
    
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getActivityData()
       self.tabBarController?.tabBar.isHidden = false
      self.navigationController?.navigationBar.isHidden = true
        
    }
    
   
    @IBAction func btnCreateActivity(_ sender: Any) {
                    let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ChooseCategoryViewController") as! ChooseCategoryViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
        
        

    }
    
    @IBAction func btnActClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ActivityPageViewController") as! ActivityPageViewController
        mainVC.modalPresentationStyle = .fullScreen
        mainVC.actID = actDBLocal[sender.tag].id!
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
 var reach = Reachability()!
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            if(actDBLocal.count > 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActivityShowTableViewCell

                cell.configure(actDB: actDBLocal)
                cell.collectionView.reloadData()
        return cell
            }else{
                        let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
                if(reach.connection == .none){
                        cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
                    cell.textViewNoData.text = "No Internet Connection"
                }else{
                        cell.imgNoData.image = #imageLiteral(resourceName: "noData")
                    cell.textViewNoData.text = "No actPopulars Available"
                }

                return cell
            }
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellButton", for: indexPath) as! ActivityShowBtnTableViewCell
             
            
             return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return (self.view.frame.height / 2) + 100
        }else{
            return 50
        }
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
