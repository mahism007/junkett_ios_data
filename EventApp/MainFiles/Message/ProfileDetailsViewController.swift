//
//  ProfileDetailsViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ProfileDetailsViewController:CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
     
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        
        
        let objectData4 = profileDisplay(name: "My Activities", data: "")
        dataArray.append(objectData4)

        let objectData11 = profileDisplay(name: "Facebook", data: "")
        dataArraySocial.append(objectData11)
        let objectData12 = profileDisplay(name: "Instagram", data: "")
        dataArraySocial.append(objectData12)
        
       
    }
    var profileDB:[ProfileListModel] = []
     var profileAPI = ProfileDataListOtherModelDataAPI()
    var toSeeID = String()
    @objc func getProfileData(){
    
              
          
            
               var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                          "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                          "ToSeeUserID":toSeeID
                                       ]
             
     
               
               
               profileAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                   
                   self.profileDB = dict as! [ProfileListModel]
                   if(self.profileDB.count > 0 ){

                    self.catDB = self.profileDB[0].listCategory
                 

                    self.dataArray[0].data = self.profileDB[0].MyActivityCount

                    
                    self.dataArraySocial[0].data = self.profileDB[0].FacebookUserName
                    self.dataArraySocial[1].data = self.profileDB[0].InstagramUserName
                    self.tableView.reloadData()
                   }
                   
               }
           }
    var catDB : [CategoryCountryListModel] = []

    var dataArray : [profileDisplay] = []
    var dataArraySocial : [profileDisplay] = []
    var dataArrayMessage : [profileDisplay] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
                        self.tabBarController?.tabBar.isHidden = true
               self.navigationController?.navigationBar.isHidden = false
         self.navigationController?.navigationBar.topItem?.title = ""
               //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
 
                 
                 self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                 self.navigationController?.navigationBar.shadowImage = UIImage()
                 self.navigationController?.navigationBar.isTranslucent = true
                 self.navigationController?.view.backgroundColor = .clear
       self.navigationController?.navigationBar.tintColor = .black
      getProfileData()
        
    }
    @IBAction func btnAddMatesClicked(_ sender: UIButton) {
        
        self.startLoadingPK(view: self.view)
                    
                    DispatchQueue.global(qos: .background).async {
                    

                    let parameter = [
             "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
               "UserID": UserDefaults.standard.string(forKey: "UserID")!,
               "ToUserID":self.toSeeID

          


                      
                    ] as [String : Any]
                    print(parameter)
                      WebServices.sharedInstances.sendPostRequest(url: URLConstants.MateRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                          
                          if let response = response["response"]{
                           
                              let objectmsg = MessageCallServerModel1()
                              
                              let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                               let objSucess = SuccessServerModel()
                                let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                              if(statusStringsuccess == "200"){
                              
                             self.stopLoadingPK(view: self.view)
                             self.showSuccess(strMessage: "Request Sent")
                                self.getProfileData()
                              
                          }else{
                               self.stopLoadingPK(view: self.view)
                              self.showError(strMessage: msg)
                          }
                         }
                          
                      }) { (err) in
                         self.stopLoadingPK(view: self.view)
                          print(err.description)
                      }

                    }
    }

       
    @IBAction func btnSendMessageClicked(_ sender: UIButton) {
                  let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
        mainVC.proDB = profileDB[0]
        mainVC.toSeeID = toSeeID
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    @IBAction func btnShowProfileClicked(_ sender: UIButton) {
    }
    @IBAction func btnShowMoreClicked(_ sender: UIButton) {
        if(showMore == false){
            showMore = true
        }else{
            showMore = false
        }
        tableView.reloadData()
    }
    

    var showMore = false
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDetailsProClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ActivityPageViewController") as! ActivityPageViewController
        mainVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 ||  section == 4 || section == 5  ){
            
                return 40
            
        }else {
            return 0
            
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HeadProfileTableViewCell
         if(section == 2){
            header.lblHeader.text = "INFORMATION"
            
        }else if(section == 4){
            header.lblHeader.text = "INTEREST"
            
        }else if(section == 5){
            header.lblHeader.text = "SOCIAL NETWORK"
            
        }
        return header
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if(section == 3){
            return dataArray.count
            
        }else if(section == 5){
            return dataArraySocial.count
            
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileImageTableViewCell
        if(profileDB.count > 0){
             var img : [String] = []
             if(profileDB[0].listProfilePic.count > 0){
                 
                 for i in 0...profileDB[0].listProfilePic.count - 1 {
                     img.append(profileDB[0].listProfilePic[i].Img)
                 }
                 
                 
         
             }
            cell.nameLabel.text = profileDB[0].FName + " " +  profileDB[0].LName
            let datee = dateFormatChange(inputDateStr: self.profileDB[0].DOB, inputFormat: "MMM dd yyyy hh:mma", outputFromat: "dd MMM yyyy")
            cell.dateLabel.text = datee
             cell.configureCollectionView(imagesAll: img)
         }
       
        return cell
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellName", for: indexPath) as! ProfileNameTableViewCell
             if(self.profileDB.count > 0 ){
                cell.lblName.text = self.profileDB[0].FName + " " + self.profileDB[0].LName
                let year = dateFormatChange(inputDateStr: self.profileDB[0].DOB, inputFormat: "MMM  dd yyyy HH:mma", outputFromat: "yyyy")
                    let dateFormatter21 = DateFormatter()
                      dateFormatter21.timeZone = NSTimeZone.system
                      dateFormatter21.dateFormat = "yyyy"
                let todayyr = dateFormatter21.string(from: Date())
                cell.lblAge.text = String(Int(todayyr)! - Int(year)!) + " years"
                let uid = UserDefaults.standard.string(forKey: "UserID")!
                if(self.profileDB[0].MateRequestedBy == uid){
                    if(self.profileDB[0].MateRequesteStatus == "Pending"){
                        cell.btnMates.setTitle("CANCEL REQUEST", for: .normal)
                    }
                }else{
                    cell.btnMates.setTitle("ADD TO MATES", for: .normal)
                }
            }
            
             return cell
        }else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetails", for: indexPath)as! ProfileDetailsTableViewCell
            if(profileDB.count > 0){
                if(profileDB[0].listProfilePic.count > 0){
                    var img : [String] = []
                    for i in 0...profileDB[0].listProfilePic.count - 1 {
                        img.append(profileDB[0].listProfilePic[i].Img)
                    }
                    cell.collectionView.isHidden = false
                    cell.viewHide.isHidden = true
                    cell.img.isHidden = true
                    cell.lbl.isHidden = true
                    cell.lbl.text = ""
                    cell.collectionView.reloadData()
                    
            cell.configurecell(image: img)
                }else{
                    cell.collectionView.isHidden = true
                    cell.viewHide.isHidden = false
                    cell.img.isHidden = false
                    cell.lbl.isHidden = false
                    cell.lbl.text = "No Instagram Posts"
                }
                
               cell.textViewDetails.text = profileDB[0].BioDescription
                
             
                if(showMore == false){

                    cell.btnShowMore.setTitle("SHOW MORE", for: .normal)
                }else{

                    cell.btnShowMore.setTitle("SHOW LESS", for: .normal)
                }
                
                
                
                
            }else{
                cell.collectionView.isHidden = true
                cell.viewHide.isHidden = false
                cell.img.isHidden = false
                cell.lbl.isHidden = false
                cell.lbl.text = "No Instagram Posts"
                
            }
            
             return cell
        }else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArray[indexPath.row].name
            cell.lblData.text = dataArray[indexPath.row].data
            cell.switcher.isHidden = true
            cell.lblData.isHidden = false
            cell.imgArrow.isHidden = false
            return cell
        }else if(indexPath.section == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellCollect", for: indexPath) as! CategoryDataTableViewCell
            cell.configureCell(catDB: catDB, selectedCatDB: [], intShow: 0)
            cell.collectionView.reloadData()
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArraySocial[indexPath.row].name
            cell.lblData.text = dataArraySocial[indexPath.row].data
            cell.lblData.isHidden = false
            cell.imgArrow.isHidden = false
             cell.switcher.isHidden = true
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return (self.view.frame.height / 2)
        }else if(indexPath.section == 1){
            return 135
        }else if(indexPath.section == 2){
          if(showMore == true){
         return UITableView.automaticDimension
         }else{
            return 170
         }
        }else if(indexPath.section == 4){
              var height : CGFloat = 0
              let dd = catDB.count / 2
              let ff = catDB.count % 2
              if(ff == 0){
                  height = CGFloat(dd * 70)
              }else{
                  height = CGFloat((dd+1) * 70)
              }
              
              
              return height
        }else{
            return 60
        }
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
