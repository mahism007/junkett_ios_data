//
//  DrawMapRadiusViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 11/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
class DrawMapRadiusViewController: CommonClassViewController ,CLLocationManagerDelegate,GMSMapViewDelegate{
    
    @IBOutlet weak var radiusPicker: BalloonPickerView!
    @IBOutlet weak var lblLocRadius: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    var marker = GMSMarker()
     let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        let camera = GMSCameraPosition.camera(withLatitude: 26.8034, longitude: 75.8178, zoom: 15.0)
        mapView.camera = camera
        
        marker.position = CLLocationCoordinate2D(latitude: 26.8034, longitude: 75.8178)
        marker.isDraggable = true
        marker.map = mapView
        mapView.delegate = self
        marker.isDraggable = true
        reverseGeocoding(marker: marker)
        marker.map = mapView
        
        mapView.delegate = self
        drawCircle(position: marker.position, radius: 50)
        // Do any additional setup after loading the view.
    }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
                      self.tabBarController?.tabBar.isHidden = true
                 self.navigationController?.navigationBar.isHidden = false
           self.navigationController?.navigationBar.topItem?.title = ""
                 //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
                   
                   
                   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                   self.navigationController?.navigationBar.shadowImage = UIImage()
                   self.navigationController?.navigationBar.isTranslucent = true
                   self.navigationController?.view.backgroundColor = .clear
  
            
        }
    @IBAction func locationPcker(_ sender: BalloonPickerView) {
        drawCircle(position: marker.position, radius: Int(sender.value))
    }
    //Mark: Marker methods
         func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
    //         print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
    //         reverseGeocoding(marker: marker)
    //     print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
         }
        func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
             print("didBeginDragging")
         }
         func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
             print("didDrag")
         }
         func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
             let locValue:CLLocationCoordinate2D = manager.location!.coordinate
             print("locations = \(locValue.latitude) \(locValue.longitude)")
          //  marker.position = locValue
            
            
               // reverseGeocoding(marker: marker)
         }
         func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
          //  print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
             
             marker.position = position.target
             
             
                 reverseGeocoding(marker: marker)
             print("Position of marker is = \(marker.position.latitude),\(marker.position.longitude)")
         }
         //Mark: Reverse GeoCoding
         var currentAddress = String()
         func reverseGeocoding(marker: GMSMarker) {
             let geocoder = GMSGeocoder()
             let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
             
             
             
             geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                 if let address = response?.firstResult() {
                     let lines = address.lines! as [String]
                     
                     print("Response is = \(address)")
                     print("Response is = \(lines)")
                     
                    self.currentAddress = lines.joined(separator: "\n")
                   // self.view.makeToastHZL(self.currentAddress)
                     
                 }
                marker.title = self.currentAddress
                marker.map = self.mapView
                self.drawCircle(position: marker.position, radius: Int(self.radiusPicker.value))
             }
         }
    
    var circle = GMSCircle()
    func drawCircle(position: CLLocationCoordinate2D,radius : Int) {

        //var latitude = position.latitude
        //var longitude = position.longitude
        //var circleCenter = CLLocationCoordinate2DMake(latitude, longitude)
        circle.position = marker.position
        circle.radius = CLLocationDistance(radius)
        circle.strokeColor = UIColor.white
        circle.fillColor = UIColor(red: 0, green: 1.0, blue: 0, alpha: 0.5)
        circle.map = mapView
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
