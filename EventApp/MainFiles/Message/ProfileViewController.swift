//
//  ProfileViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 23/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import FBSDKLoginKit
class ProfileViewController:CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   var profileDB:[ProfileListModel] = []
    var profileAPI = ProfileDataListModelDataAPI()
    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        
        let objectData1 = profileDisplay(name: "Location", data: "")
        dataArray.append(objectData1)
        let objectData2 = profileDisplay(name: "Interest", data: "")
        dataArray.append(objectData2)
        let objectData3 = profileDisplay(name: "Mates", data: "")
        dataArray.append(objectData3)
        
        let objectData4 = profileDisplay(name: "My Activities", data: "")
        dataArray.append(objectData4)
        let objectData5 = profileDisplay(name: "Invities", data: "")
        dataArray.append(objectData5)
        let objectData7 = profileDisplay(name: "Favourites", data: "")
        dataArray.append(objectData7)
        let objectData6 = profileDisplay(name: "Blocked Users", data: "")
        dataArray.append(objectData6)
      
        
        
        let objectData11 = profileDisplay(name: "Facebook", data: "")
        dataArraySocial.append(objectData11)
        let objectData12 = profileDisplay(name: "Instagram", data: "")
        dataArraySocial.append(objectData12)
        
        let objectData111 = profileDisplay(name: "All", data: "")
        dataArrayMessage.append(objectData111)
        let objectData121 = profileDisplay(name: "Mates", data: "")
        dataArrayMessage.append(objectData121)
        let objectData131 = profileDisplay(name: "People from the same activity", data: "")
        dataArrayMessage.append(objectData131)
    }
      @objc func getProfileData(){
    
              
          
            
               var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                          "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                       ]
             
               
               
               
               profileAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                   
                   self.profileDB = dict as! [ProfileListModel]
                   if(self.profileDB.count > 0 ){
                    self.dataArray[0].data = self.profileDB[0].Address
                    var cat = ""
                    for i in 0...self.profileDB[0].listCategory.count - 1 {
                        if(i==0){
                           cat = self.profileDB[0].listCategory[i].Name
                        }else if(i == self.profileDB[0].listCategory.count - 1){
                            cat = cat + " and " + self.profileDB[0].listCategory[i].Name
                        }else{
                       cat = cat + "," + self.profileDB[0].listCategory[i].Name
                        }
                        self.selectedCat.append(self.profileDB[0].listCategory[i])
                    }
                    self.dataArray[1].data = cat
                    self.dataArray[2].data = self.profileDB[0].MyMatesCount
                    self.dataArray[3].data = self.profileDB[0].MyActivityCount
                    self.dataArray[4].data = self.profileDB[0].InvitesCount
                    self.dataArray[5].data = self.profileDB[0].FavoriteActivityCount
                    self.dataArray[6].data = self.profileDB[0].BlockedUserCount
                    
                    self.dataArraySocial[0].data = self.profileDB[0].FacebookUserName
                    self.dataArraySocial[1].data = self.profileDB[0].InstagramUserName
                    self.tableView.reloadData()
                   }
                   
               }
           }
    var selectedCat : [CategoryCountryListModel] = []
    @IBAction func btnMyProfileClicked(_ sender: UIButton) {
        
        
        
    }
    
    
    @IBAction func btnSocialConnect(_ sender: UIButton) {
        if(sender.tag == 0){
                    // 1
                    let loginManager = LoginManager()
                    
                    if let _ = AccessToken.current {
                        // Access token available -- user already logged in
                        // Perform log out
                        
                        // 2
                        loginManager.logOut()
            //            updateButton(isLoggedIn: false)
            //            updateMessage(with: nil)
                        
                    } else {
                        // Access token not available -- user already logged out
                        // Perform log in
                        
                        // 3
                        loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
                            
                            // 4
                            // Check for error
                            guard error == nil else {
                                // Error occurred
                                print(error!.localizedDescription)
                                return
                            }
                            
                            
                            // 5
                            // Check for cancel
                            guard let result = result, !result.isCancelled else {
                                print("User cancelled login")
                                return
                            }
                            
                            // Successfully logged in
                            // 6
                           // self?.updateButton(isLoggedIn: true)
                            
                            // 7
                            Profile.loadCurrentProfile { (profile, error) in
                                //self?.updateMessage(with: Profile.current?.name)
                                self!.dataArraySocial[0].data = (Profile.current?.name!)!
                                self!.tableView.reloadData()
                                self!.updateProfile(id: (Profile.current?.userID)!, name: (Profile.current?.name!)!, typee: "fb")
                              //  self?.LoginData(email: (Profile.current?.email!)!, tyep: "fb")
                            }
                        }
                    }
        }
    }
    func updateProfile(id : String,name : String,typee : String) {
         DispatchQueue.global(qos: .background).async {
            var parameter : [String : String] = [:]
            if(typee == "fb"){
                
          parameter = [
           "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
             "UserID": UserDefaults.standard.string(forKey: "UserID")!,
             "FacebookUserName": name,
             "FacebookUserID": id

                    
         ] as [String : String]
            }else{
                 parameter = [
                  "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                    "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                    "InstagramUserName": name,
                    "InstagramUserID": id

                           
                ] as [String : String]
            }
         WebServices.sharedInstances.sendPostRequest(url: URLConstants.UpdateProfile, parameters: parameter, successHandler: { (dict:[String : AnyObject]) in
              
              if let response = dict["response"]{
               
                  let objectmsg = MessageCallServerModel1()
                  
                  let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                   let objSucess = SuccessServerModel()
                    let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                  if(statusStringsuccess == "200"){
                  self.stopLoading()
                  self.getProfileData()
                    // self.showSuccess(strMessage: msg)
                  
              }else{
                 self.stopLoading()
                  //self.showError(strMessage: msg)
              }
             }
              
          }) { (err) in
              self.stopLoading()
              print(err.description)
          }
         }
         
     }
    @IBAction func btnProfileClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ProfileDetailsViewController") as! ProfileDetailsViewController
        mainVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    
    
    
    
    var dataArray : [profileDisplay] = []
    var dataArraySocial : [profileDisplay] = []
    var dataArrayMessage : [profileDisplay] = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       self.tabBarController?.tabBar.isHidden = false
      
         getProfileData()
    }
    
    @IBAction func btnDetailsProClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if((indexPath?.section)! == 2 && (indexPath?.row)! == 0 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
                    let mainVC  =  storyboard.instantiateViewController(withIdentifier: "UpdateLocationViewController") as! UpdateLocationViewController
            mainVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 1 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
                    let mainVC  =  storyboard.instantiateViewController(withIdentifier: "UpdateCategoryViewController") as! UpdateCategoryViewController
            mainVC.categortSelect = self.profileDB[0].listCategory
            mainVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 2 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
                    let mainVC  =  storyboard.instantiateViewController(withIdentifier: "MyMatesViewController") as! MyMatesViewController
            mainVC.matesReqCount = self.profileDB[0].MyMatesCount
            mainVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 3 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivitySelectViewController") as! CreateActivitySelectViewController
            mainVC.intShow = 1
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 4 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivitySelectViewController") as! CreateActivitySelectViewController
            mainVC.intShow = 3
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 5 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivitySelectViewController") as! CreateActivitySelectViewController
            mainVC.intShow = 2
            self.navigationController?.pushViewController(mainVC, animated: true)
        }else if((indexPath?.section)! == 2 && (indexPath?.row)! == 6 ){
            let storyboard = UIStoryboard(name: "Event", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "BlockedUserViewController") as! BlockedUserViewController
            self.navigationController?.pushViewController(mainVC, animated: true)
        }
        


    }
    var showMore = false
    @IBAction func btnShowMore(_ sender: Any) {
        if(showMore == false){
            showMore = true
        }else{
            showMore = false
        }
        tableView.reloadData()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2 || section == 3 || section == 4 || section == 5  ){
            
                return 40
            
        }else {
            return 0
            
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HeadProfileTableViewCell
         if(section == 2){
            header.lblHeader.text = "PROFILE"
            
        }else if(section == 3){
            header.lblHeader.text = "SOCIAL NETWORK"
            
        }else if(section == 4){
            header.lblHeader.text = "RECIEVE MESSAGE FROM"
            
        }else if(section == 5){
            header.lblHeader.text = "LOCATION"
            
        }
        return header
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if(section == 2){
            return dataArray.count
            
        }else if(section == 3){
            return dataArraySocial.count
            
        }else if(section == 4){
            return dataArrayMessage.count
            
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProfileImageTableViewCell
            if(profileDB.count > 0){
               
                cell.nameLabel.text = self.profileDB[0].FName
                cell.dateLabel.text = mydateFormat(dateFormat: "MMM dd yyyy hh:mma", dateFormatted: "dd MMM yyyy", strDate: self.profileDB[0].DOB)
                
                
                if(profileDB.count > 0){
                    var img : [String] = []
                    if(profileDB[0].listProfilePic.count > 0){
                        
                        for i in 0...profileDB[0].listProfilePic.count - 1 {
                            img.append(profileDB[0].listProfilePic[i].Img)
                        }
                        
                        
                
                    }
                    cell.configureCollectionView(imagesAll: img)
                }
                
                
            }else{
                cell.nameLabel.text = "---"
                cell.dateLabel.text = "---"
            }
            
        return cell
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetails", for: indexPath)as! ProfileDetailsTableViewCell
            if(profileDB.count > 0){
                if(profileDB[0].listProfilePic.count > 0){
                    var img : [String] = []
                    for i in 0...profileDB[0].listProfilePic.count - 1 {
                        img.append(profileDB[0].listProfilePic[i].Img)
                    }
                    cell.collectionView.isHidden = false
                    cell.viewHide.isHidden = true
                    cell.img.isHidden = true
                    cell.lbl.isHidden = true
                    cell.lbl.text = ""
                    
            cell.configurecell(image: img)
                }else{
                    cell.collectionView.isHidden = true
                    cell.viewHide.isHidden = false
                    cell.img.isHidden = false
                    cell.lbl.isHidden = false
                    cell.lbl.text = "No Instagram Posts"
                }
                
               cell.textViewDetails.text = profileDB[0].BioDescription
                
             
                if(showMore == false){

                    cell.btnShowMore.setTitle("SHOW MORE", for: .normal)
                }else{

                    cell.btnShowMore.setTitle("SHOW LESS", for: .normal)
                }
                
                
                
                
            }else{
                cell.collectionView.isHidden = true
                cell.viewHide.isHidden = false
                cell.img.isHidden = false
                cell.lbl.isHidden = false
                cell.lbl.text = "No Instagram Posts"
                
            }
            
             return cell
        }else if(indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArray[indexPath.row].name
            cell.lblData.text = dataArray[indexPath.row].data
            cell.switcher.isHidden = true
            cell.lblData.isHidden = false
            cell.imgArrow.isHidden = false

            return cell
        }else if(indexPath.section == 3){
            if(dataArray[indexPath.row].data == ""){
                
                                        let cell1 = tableView.dequeueReusableCell(withIdentifier: "cellProfileSocial", for: indexPath) as! ProfileDataTableViewCell
                            cell1.lblTitle.text = dataArraySocial[indexPath.row].name
                            
                            cell1.btnData.tag = indexPath.row
                            return cell1
            }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            
          
            
            cell.lblTitle.text = dataArraySocial[indexPath.row].name
            cell.lblData.text = dataArraySocial[indexPath.row].data
            cell.switcher.isHidden = true
            cell.lblData.isHidden = false
            cell.imgArrow.isHidden = false
            return cell
            }
        }else if(indexPath.section == 4){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = dataArrayMessage[indexPath.row].name
            cell.lblData.isHidden = true
            cell.imgArrow.isHidden = true
             cell.switcher.isHidden = false
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfile", for: indexPath) as! ProfileDataTableViewCell
            cell.lblTitle.text = "Show my location"
            cell.lblData.isHidden = true
            cell.imgArrow.isHidden = true
            cell.switcher.isHidden = false
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return (self.view.frame.height / 2)
        }else if(indexPath.section == 1){
            if(showMore == true){
            return UITableView.automaticDimension
            }else{
               return 170
            }
        }else{
            return 60
        }
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
public struct profileDisplay{
    var name: String
     var data: String
}
