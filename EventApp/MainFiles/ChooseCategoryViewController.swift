//
//  ChooseCategoryViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 23/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
var catSelect : [CategoryCountryListModel] = []
class ChooseCategoryViewController: CommonClassViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    
    
    @IBOutlet weak var btnNext: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.string(forKey: "SessionID")!)
        print(UserDefaults.standard.string(forKey: "UserID")!)
        catSelect = []
        CategoryData()
        self.view.backgroundColor = .white
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
        
//
       
        // Do any additional setup after loading the view.
    }
           override func viewWillAppear(_ animated: Bool) {
               super.viewWillAppear(true)
            
                     self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.navigationBar.isHidden = false
              self.navigationController?.navigationBar.topItem?.title = ""
                    //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
            self.title = "Choose Category"
                      
                      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                      self.navigationController?.navigationBar.shadowImage = UIImage()
                      self.navigationController?.navigationBar.isTranslucent = true
                      self.navigationController?.view.backgroundColor = .clear
            self.navigationController?.navigationBar.tintColor = .black

                   
               
           }
   
    @IBAction func btnNextClicked(_ sender: UIButton) {
        catSelect = []
        var ii = ""
          var iii = 0
        for i in 0...categoryListDB.count - 1 {
            if(categoryListDB[i].boolSelected == true){
                
               catSelect.append(categoryListDB[i])
                ii = categoryListDB[i].ID + ","
                 iii = iii + 1
            }
            
        }
        if(iii > 1){
        ii.remove(at: ii.index(before: ii.endIndex))
            RegisterStruct.categoryList = ii
        }
        if(catSelect.count == 0){
            self.showError(strMessage: "Please Select atleast one category")
        }else{
                    let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivityViewController") as! CreateActivityViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
        }
    }
    @IBAction func btnTypeClicked(_ sender: UIButton) {
 
        if(categoryListDB.count > 0){
            

            
            
                if(categoryListDB[sender.tag].boolSelected == true){
                categoryListDB[sender.tag].boolSelected = false
                }else if(categoryListDB[sender.tag].boolSelected == false){
                var ii = 0
                    for i in 0...categoryListDB.count - 1 {
                        if(categoryListDB[i].boolSelected == true){
                            ii = ii + 1
                           
                        }
                        
                    }
                    if(ii < 2){
                        categoryListDB[sender.tag].boolSelected = true
                    }else{
                        self.showError(strMessage: "Cannot add more than two categories")
                    }
                }
                

        }
       
        
        
        collectionView.reloadData()
    
    }
 
    var categoryListAPI = CategoryListModelDataAPI()
           
           func CategoryData()  {
           
               
             categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
                   categoryListDB = dict as! [CategoryCountryListModel]
                self.collectionView.reloadData()
                   //self.InterestCollectionView.reloadData()
                   
               }
               
               
           }
   func numberOfSections(in collectionView: UICollectionView) -> Int {
               
               return 1
           }
           
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               if(categoryListDB.count > 0){
                   return categoryListDB.count
               }
               return 0
           }
           
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
               
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

               cell.btnType.tag = indexPath.row
               cell.lblType.text = categoryListDB[indexPath.row].Name
               cell.lblType.textColor = .white
            cell.viewType.backgroundColor = UIColor.init(hexString: categoryListDB[indexPath.row].ColorCode)
    //           cell.viewType.layer.borderWidth = 1.0
    //           cell.viewType.layer.cornerRadius = 10
               if(categoryListDB[indexPath.row].boolSelected == true){
                   cell.viewType.backgroundColor = UIColor.init(hexString: categoryListDB[indexPath.row].ColorCode)
                cell.lblType.textColor = .white
               }else{
                   cell.viewType.backgroundColor = UIColor.white
                cell.lblType.textColor = .darkGray
               }
           
                 let urlShow = categoryListDB[indexPath.row].IMGResource.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
              if let url = NSURL(string: urlShow!) {
               cell.imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
              }
              cell.imgView.roundCorners([.topLeft,.bottomLeft], radius: 10)
               return cell
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
               
               return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
           {
               if(categoryListDB.count > 0){
               let label = UILabel(frame: CGRect.zero)
               label.text = categoryListDB[indexPath.item].Name
               label.sizeToFit()
                let screen = (UIScreen.main.bounds.width - 60) / 2
               // 50 + label.frame.width
               return CGSize(width: screen , height: 60)
               }else{
                      return CGSize(width: 40, height: 60)
               }
           }
           
           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               
               print("it worked")
               //print(indexPath.item)
               
               
           }
           
           func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
               
               
           }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

