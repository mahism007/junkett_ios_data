//
//  DeleteAccountViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 11/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DTGradientButton
class DeleteAccountViewController: UIViewController {

    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        btnDelete.backgroundColor = UIColor.init(hexString: "48C20B")
        btnCancel.backgroundColor = UIColor.init(hexString: "FF1493", alpha: 0.5)
        btnCancel.layer.borderColor = UIColor.init(hexString: "FF1493", alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.cornerRadius = 20
        btnDelete.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
