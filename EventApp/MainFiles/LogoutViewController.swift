//
//  LogoutViewController.swift
//  EventApp
//
//  Created by Bunga Maheshwar Rao on 06/10/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class LogoutViewController:UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

         img.layer.cornerRadius = 50
        btnDelete.backgroundColor = UIColor.init(hexString: "48C20B")
        btnCancel.backgroundColor = UIColor.init(hexString: "FF1493", alpha: 0.5)
        btnCancel.layer.borderColor = UIColor.init(hexString: "FF1493", alpha: 1.0).cgColor
        btnCancel.layer.borderWidth = 1.0
        btnCancel.layer.cornerRadius = 20
        btnDelete.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnDeleteClicked(_ sender: UIButton) {
       // self.dismiss(animated: true, completion: nil)
                   UserDefaults.standard.set(false, forKey: "isLogin")
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "SigninNavViewController") as! SigninNavViewController
        mainVC.modalPresentationStyle = .fullScreen
        self.present(mainVC, animated: true, completion: nil)
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
