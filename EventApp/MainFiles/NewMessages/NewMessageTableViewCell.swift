//
//  NewMessageTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 06/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SwipeCellKit
class NewMessageTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMessage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class CreateActivitySelectTableViewCell: SwipeTableViewCell {

    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgMessage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgArrow.setImageColor(color: .systemGreen)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
