//
//  OtherActivityTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 03/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class OtherActivityTableViewCell: UITableViewCell {

    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var textViewDesc: UITextView!
//    @IBOutlet weak var pickerAge: BalloonPickerView!
    @IBOutlet weak var lblAge: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
