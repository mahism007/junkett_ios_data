//
//  MessageNewViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 06/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SwipeCellKit
import SDWebImage
class MessageNewViewController:CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var messageDB:[MessageSessionModel] = []
     var messageAPI = MessageSessionModelDataAPI()
    var refreshControl = UIRefreshControl()
    var defaultOptions = SwipeOptions()
    var isSwipeRightEnabled = true
    var buttonDisplayMode: ButtonDisplayMode = .imageOnly
    var buttonStyle: ButtonStyle = .circular
    var usesTallCells = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
  @objc func getSessionMsgData(){
  
            
        
          
             var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                        "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                     ]
           
             
             
             
             messageAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                 
                 self.messageDB = dict as! [MessageSessionModel]
                if(self.messageDB.count > 0 ){
                    self.tableView.reloadData()
                }
                 
             }
         }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
getSessionMsgData()
          self.navigationController?.navigationBar.isHidden = true
 
            self.tabBarController?.tabBar.isHidden = false
        }

    
    @IBAction func btnTapClicked(_ sender: UIButton) {
                    let storyboard = UIStoryboard(name: "Event", bundle: nil)
                     let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ChatMessageViewController") as! ChatMessageViewController
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
 
       
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if(messageDB.count > 0){
        
            return messageDB.count
         }
         return 1
      
    }
    var reach = Reachability()!
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
if(messageDB.count == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNoData", for: indexPath) as! NoDataTableViewCell
    if(reach.connection == .none){
            cell.imgNoData.image = #imageLiteral(resourceName: "noInternet")
        cell.textViewNoData.text = "No Internet Connection"
    }else{
            cell.imgNoData.image = #imageLiteral(resourceName: "noData")
        cell.textViewNoData.text = "No Messages Available"
    }

    return cell
}else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewMessageTableViewCell
        cell.delegate = self
    
    cell.lblTitle.text = messageDB[indexPath.row].SendBy
    cell.textViewMessage.text = messageDB[indexPath.row].Message
    let mesgDate = dateFormatChange(inputDateStr: messageDB[indexPath.row].MessageDateTime, inputFormat: "yyyy-MM-dd'T'hh:mm:ss.SSS", outputFromat: "dd MMM yyyy")
    let mesgTime = dateFormatChange(inputDateStr: messageDB[indexPath.row].MessageDateTime, inputFormat: "yyyy-MM-dd'T'hh:mm:ss.SSS", outputFromat: "hh:mm a")
        let mesgDateTime = dateFormatChange(inputDateStr: messageDB[indexPath.row].MessageDateTime, inputFormat: "yyyy-MM-dd'T'hh:mm:ss.SSS", outputFromat: "dd MMM yyyy hh:mm a")
    let dateFormatter21 = DateFormatter()
          dateFormatter21.timeZone = NSTimeZone.system
          dateFormatter21.dateFormat = "dd MMM yyyy"
    let dateFormatter2 = DateFormatter()
          dateFormatter2.timeZone = NSTimeZone.system
          dateFormatter2.dateFormat = "hh:mm a"
    
    
    let todayDate = dateFormatter21.string(from: Date())
    let todayTime = dateFormatter21.string(from: Date())
    if(mesgDate == todayDate){
        if(mesgTime == todayTime){
            cell.lblDate.text = "Now"
        }else{
            cell.lblDate.text = mesgTime
        }
    }else{
        cell.lblDate.text = mesgDateTime
    }

            cell.imgMessage.layer.cornerRadius = 10
        cell.btnTap.tag = indexPath.row
    var urlString = ""
    if(messageDB[indexPath.row].listFromProfilePic.count > 0){
     urlString = messageDB[indexPath.row].listFromProfilePic[0].Img

        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if NSURL(string: urlShow!) != nil {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    
                    cell.imgMessage.image = image
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                   

                    cell.imgMessage.image = UIImage(named: "ProfileSample")
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
         
           
            cell.imgMessage.image = UIImage(named: "ProfileSample")
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
    }else{

       
        cell.imgMessage.image = UIImage(named: "ProfileSample")
        //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
    }
   
        
        return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(messageDB.count > 0){
       
            return 123
        }
        return self.view.frame.height - 150
    
    }
    func configure(action: SwipeAction, with descriptor: ActionDescriptor) {
        action.title = descriptor.title(forDisplayMode: buttonDisplayMode)
        action.image = descriptor.image(forStyle: buttonStyle, displayMode: buttonDisplayMode)
        
        switch buttonStyle {
        case .backgroundColor:
            action.backgroundColor = descriptor.color(forStyle: buttonStyle)
        case .circular:
            action.backgroundColor = .white
            action.textColor = descriptor.color(forStyle: buttonStyle)
            action.font = .systemFont(ofSize: 13)
            action.transitionDelegate = ScaleTransition.default
        }
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MessageNewViewController: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        
       
       
            
            let delete = SwipeAction(style: .default, title: nil) { action, indexPath in
                //self.emails.remove(at: indexPath.row)
            }
            configure(action: delete, with: .trash)
            

            
            return [delete]
        }
    func tableView(_ tableView: UITableView, editActionsOptionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> SwipeOptions {
        var options = SwipeOptions()
        options.expansionStyle = orientation == .left ? .selection : .destructive
        options.transitionStyle = defaultOptions.transitionStyle
        
        switch buttonStyle {
        case .backgroundColor:
            options.buttonSpacing = 4
        case .circular:
            options.buttonSpacing = 4
        #if canImport(Combine)
            if #available(iOS 13.0, *) {
                options.backgroundColor = UIColor.systemGray6
            } else {
                options.backgroundColor = #colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)
            }
        #else
            options.backgroundColor = #colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)
        #endif
        }
        
        return options
    }
    }
    
   

