//
//  ChatMessageViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 11/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class ChatMessageViewController:CommonClassViewController,LynnBubbleViewDataSource {
    
    @IBOutlet weak var imgProfile: RoundedImageCornerView!
    @IBOutlet weak var tbBubbleDemo: LynnBubbleTableView!
    var toSeeID = String()
    var arrChatTest:Array<LynnBubbleData> = []
        let textInputBar = ALTextInputBar()
     let keyboardObserver = ALKeyboardObservingView()
     
     let scrollView = UIScrollView()
     
     // This is how we observe the keyboard position
     override var inputAccessoryView: UIView? {
         get {
             return keyboardObserver
         }
     }
     var proDB = ProfileListModel()
     // This is also required
     override var canBecomeFirstResponder: Bool {
         return true
     }
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
           self.tabBarController?.tabBar.isHidden = true
          self.navigationController?.navigationBar.isHidden = false
    self.navigationController?.navigationBar.topItem?.title = ""
          //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
            
            
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = true
            self.navigationController?.view.backgroundColor = .clear
                
            
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        tbBubbleDemo.delegate = self
        
        getProfileData()
        
        imgProfile.layer.cornerRadius = 20
        
        tbBubbleDemo.bubbleDelegate = self
        tbBubbleDemo.bubbleDataSource = self

        }
    var profileDB:[ProfileListModel] = []
     var profileAPI = ProfileChatDataListModelDataAPI()
    @objc func getProfileData(){
    
              
          
            
               var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                          "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                       ]
             
               
               
               
               profileAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                   
                   self.profileDB = dict as! [ProfileListModel]
                   if(self.profileDB.count > 0 ){
                    self.title = self.profileDB[0].FName + " " + self.profileDB[0].LName
                   }
                   
               }
           }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        scrollView.frame = view.bounds
        textInputBar.frame.size.width = view.bounds.size.width
    }

    func configureScrollView() {
        view.addSubview(scrollView)
        
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height * 2))
        contentView.backgroundColor = UIColor(white: 0.8, alpha: 1)
        
        scrollView.addSubview(contentView)
        scrollView.contentSize = contentView.bounds.size
        scrollView.keyboardDismissMode = .interactive
        scrollView.backgroundColor = UIColor(white: 0.6, alpha: 1)
    }
    
    func configureInputBar() {
//        let leftButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
  //      leftButton.setImage(#imageLiteral(resourceName: "leftIcon"), for: .normal)
        rightButton.setImage(UIImage(named: "sendMessage"), for: .normal)
        
        keyboardObserver.isUserInteractionEnabled = false
        
        textInputBar.showTextViewBorder = true
     //   textInputBar.leftView = leftButton
        textInputBar.rightView = rightButton
        textInputBar.frame = CGRect(x: 0, y: view.frame.size.height - textInputBar.defaultHeight, width: view.frame.size.width, height: textInputBar.defaultHeight)
        textInputBar.backgroundColor = UIColor(white: 0.95, alpha: 1)
        textInputBar.keyboardObserver = keyboardObserver
        
        view.addSubview(textInputBar)
    }

    @objc func keyboardFrameChanged(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
            textInputBar.frame.origin.y = frame.origin.y
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
                    //configureScrollView()
                    configureInputBar()
        
                    NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(notification:)), name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    
    var reachablity = Reachability()!
    var handsUppId = String()
    var empId = String()
    @IBAction func btnSendMessage(_ sender: UIButton) {
        
        
        if reachablity.connection == .none{
            
            self.showError(strMessage: "Internet is not available, please check your internet connection try again.")
            
        }
            
        else if self.textInputBar.text == ""{
            
            self.showError(strMessage: "Please write message")
        }
        else{
            
            
            
            self.startLoadingPK(view: self.view)

            var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                       "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                    ]
           
          
          
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.ChattingMessageSend, parameters: parameter, successHandler: { (dict:[String : AnyObject]) in
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                    
                    self.stopLoadingPK(view: self.view)
                    self.textInputBar.text = ""
                    
                    self.ReportMessageInternal()
                    
                    
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.showError(strMessage:msg)
                }
                }
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
            
            
            
            
        
        
        
        
    }
    var DataAPI = ChatMessageEmpLoadDataAPI()
    var ReportedDB : [ChatMessageEmpLoadDataModel] = []
    @objc func ReportMessage(){
let session = UserDefaults.standard.string(forKey: "UserID")! + toSeeID
            let  param = [
                       "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                       "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                         "SessionID":session]
       
              print(param)
              
              
        DataAPI.serviceCalling(obj: self,  param: param ) { (dict) in
                  
                  self.ReportedDB = dict as! [ChatMessageEmpLoadDataModel]
                //  self.refreshControl.endRefreshing()
                  self.tbBubbleDemo.reloadData()
                  
              }
              
    }
    var DataInternalAPI = ChatMessageEmpLoadInternalDataAPI()
    
    @objc func ReportMessageInternal(){
       let session = UserDefaults.standard.string(forKey: "UserID")! + toSeeID
            let  param = [
                       "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                       "UserID": UserDefaults.standard.string(forKey: "UserID")!,
                         "SessionID":session]
       
              print(param)
              
              
        DataInternalAPI.serviceCalling(obj: self,  param: param ) { (dict) in
                  
                  self.ReportedDB = dict as! [ChatMessageEmpLoadDataModel]
                //  self.refreshControl.endRefreshing()
                  self.tbBubbleDemo.reloadData()
                  
              }
    }
    
    func bubbleTableView(dataAt index: Int, bubbleTableView: LynnBubbleTableView) -> LynnBubbleData {
        return self.arrChatTest[index]
    }
    
    func bubbleTableView(numberOfRows bubbleTableView: LynnBubbleTableView) -> Int {
        return self.arrChatTest.count
    }
}

extension ChatMessageViewController : LynnBubbleViewDelegate {
    // optional
    func bubbleTableView(_ bubbleTableView: LynnBubbleTableView, didSelectRowAt index: Int) {
        
        let alert = UIAlertController(title: nil, message: "selected index : " + "\(index)", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close",
                                        style: .default) { (action: UIAlertAction!) -> Void in
        }
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func bubbleTableView(_ bubbleTableView: LynnBubbleTableView, didLongTouchedAt index: Int) {
        let alert = UIAlertController(title: nil, message: "LongTouchedAt index : " + "\(index)", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close",
                                        style: .default) { (action: UIAlertAction!) -> Void in
        }
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func bubbleTableView(_ bubbleTableView: LynnBubbleTableView, didTouchedAttachedImage image: UIImage, at index: Int) {
        let alert = UIAlertController(title: nil, message: "AttachedImage index : " + "\(index)", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close",
                                        style: .default) { (action: UIAlertAction!) -> Void in
        }
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func bubbleTableView(_ bubbleTableView: LynnBubbleTableView, didTouchedUserProfile userData: LynnUserData, at index: Int) {
        let alert = UIAlertController(title: nil, message: "UserProfile index : " + "\(index)", preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close",
                                        style: .default) { (action: UIAlertAction!) -> Void in
        }
        alert.addAction(closeAction)
        self.present(alert, animated: true, completion: nil)
    }
}


