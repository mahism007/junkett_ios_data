//
//  ActivityPageViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 23/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DTGradientButton
import SDWebImage
import EzPopup
class ActivityPageViewController: CommonClassViewController,UITableViewDelegate, UITableViewDataSource {


     @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnMember: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
 
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
              
        NotificationCenter.default.addObserver(self, selector: #selector(self.ShareWithOther), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "ShareWithOther")), object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(self.ShareWithFriends), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "ShareWithFriends")), object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(self.ShareWith), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "ShareWith")), object: nil)
        
             btnMember.layer.cornerRadius = 20
             btnMember.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    @IBAction func btnFavClicked(_ sender: UIButton) {
        submitFavData()
       }
    
    func submitFavData(){

           self.startLoadingPK(view: self.view)
           
           DispatchQueue.global(qos: .background).async {
           

           let parameter = [
    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
      "UserID": UserDefaults.standard.string(forKey: "UserID")!,
            "ActivityID":self.actID,
            "Status":self.myFav


             
           ] as [String : Any]
           print(parameter)
             WebServices.sharedInstances.sendPostRequest(url: URLConstants.ActivityFavoriteAdd, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                 
                 if let response = response["response"]{
                  
                     let objectmsg = MessageCallServerModel1()
                     
                     let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      let objSucess = SuccessServerModel()
                       let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                     if(statusStringsuccess == "200"){
                     
                    self.stopLoadingPK(view: self.view)
                        if(self.myFav == 1){
                    self.showSuccess(strMessage: "Added as Favourite's")
                        }else{
                           self.showSuccess(strMessage: "Removed from Favourite's")
                        }
                        self.getActivityData()
                     
                     
                 }else{
                      self.stopLoadingPK(view: self.view)
                     self.showError(strMessage: msg)
                 }
                }
                 
             }) { (err) in
                self.stopLoadingPK(view: self.view)
                 print(err.description)
             }

           }
     }
    @objc func ShareWithFriends(){
    

    }
    @objc func ShareWith(){
    
        UIPasteboard.general.string = activityShare
        self.showSuccess(strMessage: "Copied")
    }
    var activityShare = String()
    @objc func ShareWithOther(){
    
    let textToShare = [ activityShare ]
       let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
       activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

       // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

       // present the view controller
       self.present(activityViewController, animated: true, completion: nil)
    
//    activityViewController.completionWithItemsHandler = { activity, success, items, error in
//
//        if success == true && error == nil
//        {
//            //self.screenshotSharer()?.dismissSharerViewController(true)
//        }
//    }

    }
    
    
    
    
    
    
    
    var showmore = false
    @IBAction func btnMateClicked(_ sender: Any) {
       }
    @IBAction func btnShowClicked(_ sender: Any) {
        if(showmore == false){
            showmore = true
        }else{
            showmore = false
        }
        self.tableView.reloadData()
    }
    
    @IBAction func btnShareClicked(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "ShareDataViewController") as! ShareDataViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 450 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    var actID = String()
       var actModelDB:[ActivityDetailsListModel] = []
        var actAPI = ActDetailModelDataAPI()
       @objc func getActivityData(){
    
                  var para = [String:String]()
                  let uid = UserDefaults.standard.string(forKey: "UserID")!
                  let parameter = ["AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                  "UserID": uid,
                  "ActivityID":actID]
                  
                  para = parameter.filter { $0.value != ""}
                  
                  actAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                      
                      self.actModelDB = dict as! [ActivityDetailsListModel]
                    for i in 0...self.actModelDB.count - 1 {
                        if(uid == self.actModelDB[0].listMates[i].ID){
                            self.join = 1
                            self.btnMember.setTitle("JOIN ACTIVITY", for: .normal)
                            break;
                        }
                    }
                    self.tableView.reloadData()
                    
                      
                  }
              }
    var join = 0
    @IBAction func btnbackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
       }
    @IBAction func btnMemberClicked(_ sender: UIButton) {
        
        if(join == 1){
            submitData(status: "Join")
        }else{
            submitData(status: "Left")
        }
        
        
    }
    func submitData(status : String){

           self.startLoadingPK(view: self.view)
           
           DispatchQueue.global(qos: .background).async {
           

           let parameter = [
    "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
      "UserID": UserDefaults.standard.string(forKey: "UserID")!,
      "ActivityID":self.actID,
    "Status":status


             
           ] as [String : Any]
           print(parameter)
             WebServices.sharedInstances.sendPostRequest(url: URLConstants.ActivityJoin, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                 
                 if let response = response["response"]{
                  
                     let objectmsg = MessageCallServerModel1()
                     
                     let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      let objSucess = SuccessServerModel()
                       let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                     if(statusStringsuccess == "200"){
                     
                    self.stopLoadingPK(view: self.view)
                
                    self.showSuccess(strMessage: msg)
                        self.getActivityData()
                     
                     
                 }else{
                      self.stopLoadingPK(view: self.view)
                     self.showError(strMessage: msg)
                 }
                }
                 
             }) { (err) in
                self.stopLoadingPK(view: self.view)
                 print(err.description)
             }

           }
     }
var myFav = 1
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
              self.tabBarController?.tabBar.isHidden = true
             self.navigationController?.navigationBar.isHidden = false
       self.navigationController?.navigationBar.topItem?.title = ""
             //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
             
               getActivityData()
               self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
               self.navigationController?.navigationBar.shadowImage = UIImage()
               self.navigationController?.navigationBar.isTranslucent = true
               self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = .white
    }
    
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
           
           
           
       }

 
       
       func numberOfSections(in tableView: UITableView) -> Int {
        if(actModelDB.count > 0){
           return 2
        }
        return 0
       }
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

           return 1
       }
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if(indexPath.section == 0){
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ActDetailTopTableViewCell
                                   if(self.actModelDB.count > 0 ){
                                      // self.tableView.reloadData()

                                    activityShare = self.actModelDB[0].ActivityShareLink
                                      cell.imgCollect.layer.cornerRadius = 10
                                              cell.viewOverlay.layer.cornerRadius = 10
                                    let datee = dateFormatChange(inputDateStr: self.actModelDB[0].StartDateTime, inputFormat: "yyyy-MM-dd'T'hh:mm:ss", outputFromat: "hh:mm a")
                                             cell.lblDate.text = datee
                                      cell.lblCategory.text = self.actModelDB[0].CategoryNames
                                             if(actModelDB[0].IsRepeated == "0"){
                                             cell.lblDay.text = "No Repeat"
                                             }
                                    //  self.textView.text = self.actModelDB[0].Description
                                      cell.lblTitle.text = self.actModelDB[0].Title
                                    cell.lblName.text = self.actModelDB[0].CreatedBy
                                    cell.lblArt.text = "Creator"
                                             cell.widthDate.constant = cell.lblDate.intrinsicContentSize.width + 10
                                              cell.widthCategory.constant = cell.lblCategory.intrinsicContentSize.width + 10
                                             cell.widthDay.constant = cell.lblDay.intrinsicContentSize.width + 10
                                             cell.viewDay.layer.cornerRadius = 5
                                             cell.viewCategory.layer.cornerRadius = 5
                                             cell.viewDate.layer.cornerRadius = 5
                                             cell.imgPro1.layer.cornerRadius = 3
                                                  
                                             cell.imgLoc.setImageColor(color: .white)
                                      cell.lblLoc.text = self.actModelDB[0].FullAddress
                                    if(actModelDB[0].MyFavorite == "0"){
                                        self.myFav = 1
                                        cell.btnFavourite.setImage(#imageLiteral(resourceName: "unfav"), for: .normal)
                                    }else{
                                        self.myFav = 0
                                        cell.btnFavourite.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
                                    }
                                                let urlShow1 = self.actModelDB[0].listFromProfilePic[0].Img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                             if let url = NSURL(string: urlShow1!) {
                                              cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "ProfileSample"))
                                             }
                                    let  urlString = self.actModelDB[0].listFromActPic[0].Img

                                                 let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                                 if NSURL(string: urlShow!) != nil {
                                                     
                                                     SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                                                         (receivedSize :Int, ExpectedSize :Int) in
                                                         
                                                     }, completed: {
                                                         (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                                                         if(image != nil) {
                                                             
                                                             cell.imgCollect.image = image
                                                             //  self.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                                             
                                                         }else{
                                                            

                                                             cell.imgCollect.image = UIImage(named: "defaulticon")
                                                             // self.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                                         }
                                                     })
                                                 }else{
                                                  
                                                    
                                                     cell.imgCollect.image = UIImage(named: "defaulticon")
                                                     //self.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                                                 }
               
                                   }
           return cell
           }else {
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetails", for: indexPath) as! ActDeatils2TableViewCell
            if(self.actModelDB.count > 0 ){
              if(showmore == false){

                  cell.btnShow.setTitle("SHOW MORE", for: .normal)
              }else{

                  cell.btnShow.setTitle("SHOW LESS", for: .normal)
              }
                if(actModelDB[0].MateCount != nil){
                if(actModelDB[0].MateCount == "0" || actModelDB[0].MateCount == "1"){
                    cell.lblCount.text = actModelDB[0].MateCount + " Mate"
                }else{
                    cell.lblCount.text = actModelDB[0].MateCount + " Mates"
                }
                }
            let fullImgArray = self.actModelDB[0].listFromMatesProfilePic

                if(fullImgArray.count > 0){
                if(fullImgArray.count == 1){
                    if let url = NSURL(string: fullImgArray[0].Img) {
             cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                    }
                    cell.imgPro2.isHidden = true
                    cell.imgPro3.isHidden = true
//                    cell.width3.constant = 0
//                    cell.width2.constant = 0
            }

                
                    if(fullImgArray.count == 2){
                        if let url = NSURL(string: fullImgArray[0].Img) {
                        cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                               }
                        if let url = NSURL(string: fullImgArray[1].Img) {
                 cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                        }
                   
                                   cell.imgPro3.isHidden = true
                                 
//                                   cell.width3.constant = 0
                }
                
                    if(fullImgArray.count == 3){
                        if let url = NSURL(string: fullImgArray[0].Img) {
                        cell.imgPro1.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                               }
                        if let url = NSURL(string: fullImgArray[1].Img) {
                 cell.imgPro2.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                        }
                        if let url = NSURL(string: fullImgArray[2].Img) {
                        cell.imgPro3.sd_setImage(with: url as URL?, placeholderImage: #imageLiteral(resourceName: "ProfileSample"))
                               }
                }
            }
            }
            cell.textViewDesc.text = actModelDB[0].Description
               return cell
           }
           
       }
       
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           if(indexPath.section == 0){
               return (self.view.frame.height / 2) + 100
           }else {
               if(showmore == true){
               return UITableView.automaticDimension
               }else{
                  return 180
               }
           }
           
       }
       
     
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
