//
//  AppDelegate.swift
//  EventApp
//
//  Created by Bunga Mahesh on 12/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import CoreData
import FluidTabBarController
import DTGradientButton
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn
import AVKit

import Firebase
import Foundation
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate ,  NSFetchedResultsControllerDelegate,UNUserNotificationCenterDelegate,MessagingDelegate  {

 var window: UIWindow?
     var connectedToGCM = false
     var subscribedToTopic = false
     var gcmSenderID: String?
     var registrationToken: InstanceIDResult?
     var registrationOptions = [String: AnyObject]()
     
     let registrationKey = "onRegistrationCompleted"
     let messageKey = "onMessageReceived"
     let subscriptionTopic = "/topics/global"
     var timerUpdate = Timer()
    
    var reachablity = Reachability()!
     var player: AVAudioPlayer?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        GMSServices.provideAPIKey("AIzaSyDBxF0YE9o-4CTfQGyTOEJkG_rYm7aoU7U")
        GMSPlacesClient.provideAPIKey("AIzaSyDBxF0YE9o-4CTfQGyTOEJkG_rYm7aoU7U")
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        
GIDSignIn.sharedInstance().clientID = "895186469098-du4fjlqpk2qnejvkq9gqp1if2qk0cl9d.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        FirebaseApp.configure()
        InstanceID.instanceID().instanceID(handler: registrationHandler)
        UNUserNotificationCenter.current().delegate = self
        registerForNotification()
        if(UserDefaults.standard.bool(forKey: "isLogin") == true){

            self.window?.rootViewController = createTabBarController()
            self.window?.makeKeyAndVisible()
//            let storyboard = UIStoryboard(name: "Event", bundle: nil)
//             let mainVC  =  storyboard.instantiateViewController(withIdentifier: "CreateActivityViewController") as! CreateActivityViewController
                           
//             self.window?.rootViewController = mainVC
//             self.window?.makeKeyAndVisible()
            
        }else{
        
        
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
         let mainVC  =  storyboard.instantiateViewController(withIdentifier: "SigninNavViewController") as! SigninNavViewController
                       
         self.window?.rootViewController = mainVC
         self.window?.makeKeyAndVisible()
        }
        return true
    }
    func registerForNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    private func createTabBarController() -> UITabBarController {
        let tabBarController = FluidTabBarController()
        tabBarController.tabBar.tintColor = UIColor.systemRed
        tabBarController.tabBar.barTintColor = #colorLiteral(red: 0, green: 0.829695344, blue: 0.5481160283, alpha: 1)
       // tabBarController.tabBar.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let vLoc = storyboard.instantiateViewController(withIdentifier: "ActivityByLocNavViewController") as! ActivityByLocNavViewController
        let itemLoc = FluidTabBarItem(title: "Map Activity", image:UIImage(named: "map_Pin"), tag: 0)
        itemLoc.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vLoc.tabBarItem = itemLoc
        
        
        
        let vMessage = storyboard.instantiateViewController(withIdentifier: "MessageNavViewController") as! MessageNavViewController
        let item1 = FluidTabBarItem(title: "Message", image:UIImage(named: "msg"), tag: 1)
        item1.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vMessage.tabBarItem = item1
        
        let vActivities = storyboard.instantiateViewController(withIdentifier: "ActivityShowNavViewController") as! ActivityShowNavViewController
        let item2 = FluidTabBarItem(title: "Activities", image:UIImage(named: "shirt"), tag: 2)
        item2.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vActivities.tabBarItem = item2
        
        let vProfile = storyboard.instantiateViewController(withIdentifier: "ProfileNavViewController") as! ProfileNavViewController
               let itemPro = FluidTabBarItem(title: "Profile", image:UIImage(named: "profile"), tag: 3)
               itemPro.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               vProfile.tabBarItem = itemPro
        
        let vsetting = storyboard.instantiateViewController(withIdentifier: "SettingNavViewController") as! SettingNavViewController
        let itemsetting = FluidTabBarItem(title: "Setting", image:UIImage(named: "setting"), tag: 4)
        itemsetting.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vsetting.tabBarItem = itemsetting

        let viewControllers = [
            vMessage,vProfile,vLoc,vActivities,vsetting
        ]
        
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 2
        return tabBarController
    }

    // MARK: UISceneSession Lifecycle
// func application(
//     _ app: UIApplication,
//     open url: URL,
//     options: [UIApplication.OpenURLOptionsKey : Any] = [:]
// ) -> Bool {
//
//     ApplicationDelegate.shared.application(
//         app,
//         open: url,
//         sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
//         annotation: options[UIApplication.OpenURLOptionsKey.annotation]
//     )
//
// }
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let sourceApplication =  options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
        
        let googleHandler = GIDSignIn.sharedInstance().handle(url)
        
        let facebookHandler = ApplicationDelegate.shared.application (
            app,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation )
        
        return googleHandler || facebookHandler
    }
   
    // [START signin_handler]
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            // [START_EXCLUDE silent]
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"), object: nil, userInfo: nil)
            // [END_EXCLUDE]
        } else {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // [START_EXCLUDE]
            if(lg == 0){
            NotificationCenter.default.post(
                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                object: nil,
                userInfo: ["email":email,"fullName":fullName])
            }else{
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: "ToggleAuthUINotificationReg"),
                    object: nil,
                    userInfo: ["email":email,"fullName":fullName])
            }
            // [END_EXCLUDE]
        }
    }
    // [END signin_handler]
    // [START disconnect_handler]
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // [START_EXCLUDE]
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: "ToggleAuthUINotification"),
            object: nil,
            userInfo: ["statusText": "User has disconnected."])
        // [END_EXCLUDE]
    }
      var backgroundSessionCompletionHandler: (() -> Void)?
            func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
                backgroundSessionCompletionHandler = completionHandler
            }
            
            func application( _ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
                deviceToken: Data ) {
                
               
                InstanceID.instanceID().instanceID(handler: registrationHandler)
                
                let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
               print(deviceTokenString)
            }
            
            func application( _ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
                error: Error ) {
                print("Registration for remote notification failed with error: \(error.localizedDescription)")
                // [END receive_apns_token_error]
                let userInfo = ["error": error.localizedDescription]
                NotificationCenter.default.post(
                    name: Notification.Name(rawValue: registrationKey), object: nil, userInfo: userInfo)
            }
           func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
             


             
              
             }

            func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
                print("Handle push from background or closed")
                // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
                print("\(response.notification.request.content.userInfo)")
                Messaging.messaging().appDidReceiveMessage(response.notification.request.content.userInfo)
                  
                  print("Notification received: \(response.notification.request.content.userInfo)")
                
                  let userInfo = response.notification.request.content.userInfo
                  
                  guard
                  let aps = userInfo[AnyHashable("aps")] as? NSDictionary,
                  let alert = aps["alert"] as? NSDictionary,
                  let body = alert["body"] as? String,
                  let title = alert["title"] as? String
                     
                  else {
                      // handle any error here
                      return
                  }

               
    //              guard let appName = userInfo[AnyHashable("gcm.notification.appName")] else {
    //                  // print("Message ID: \(messageID)")
    //                  return
    //              }
    //              guard let message = userInfo[AnyHashable("gcm.message_id")] else {
    //                  // print("Message ID: \(messageID)")
    //                  return
    //              }
                  guard let action = userInfo[AnyHashable("Action")] else {
                      // print("Message ID: \(messageID)")
                      return
                  }
        //          guard let senderName = userInfo[AnyHashable("gcm.notification.senderName")] else {
        //              return
        //              // print("Message ID: \(messageID)")
        //          }
                  guard let actionId = userInfo["ActionID"] else {
                      return
                      // print("Message ID: \(messageID)")
                  }
        //          guard let Receiver_Name = userInfo[AnyHashable("gcm.notification.Receiver_Name")] else {
        //              return
        //              // print("Message ID: \(messageID)")
        //          }
        //          guard let Action_Type = userInfo[AnyHashable("gcm.notification.Action_Type")] else {
        //              return
        //              // print("Message ID: \(messageID)")
        //          }

                  // Print full message.
                  print(userInfo)


                      notificationRedirect(action: action as! String, actionType: "" as! String, Action_ID: actionId as! String, appName: "" as! String, message: "" as! String, senderName: "" as! String,subTitle : "")
                      
                  
                  
            }
            //Speak notification.....received
            func application( _ application: UIApplication,
                              didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                              fetchCompletionHandler handler: @escaping (UIBackgroundFetchResult) -> Void) {
                
                
            }
            func notificationRedirect (action : String , actionType : String , Action_ID : String,appName : String,message : String,senderName : String,subTitle : String){
                
                if UserDefaults.standard.value(forKey:"notificationEnable") as? String == "Enable"
                {
                    
       
                            
                            
//                            if UserDefaults.standard.bool(forKey: "isLogin") == true {
//
//
//                               let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                          let handsupNav = storyboard.instantiateViewController(withIdentifier: "HandsUppFeedNavViewController") as! HandsUppFeedNavViewController
//                               notify = true
//                                actionData = action
//                                Action_IDData = Action_ID
//                                          self.window?.rootViewController = handsupNav
//                                          self.window?.makeKeyAndVisible()
//
//                            }
//                            else
//                            {
//                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//
//                                self.window?.rootViewController = loginVC
//                                self.window?.makeKeyAndVisible()
//                            }
                            
                            
                            
                            
                           
     
                    
                }
                
            }
            func playSound (soundName:String) {
                
                guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else { return }
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    player = try AVAudioPlayer(contentsOf: url)
                    guard let player = player else { return }
                    
                    player.play()
                } catch let error {
                    print(error.localizedDescription)
                }
            }
            
            
            // [END ack_message_reception]
            func registrationHandler(_ registrationToken: InstanceIDResult?, error: Error?) {
                
                
                if let registrationToken = registrationToken {
                    self.registrationToken = registrationToken
                    
                 //   print("Registration Token: \(registrationToken)")
                  //  self.subscribeToTopic()
                    let userInfo = ["registrationToken": registrationToken]
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
                    let userDefaults = UserDefaults.standard
                    print(registrationToken.token)
                  // print( Messaging.messaging().fcmToken)
                    //  UserDefaults.standard.set(false, forKey: "GCMRegister")
                    userDefaults.set(registrationToken.token, forKey: "Token")
                    
                } else if let error = error {
                    print("Registration to GCM failed with error: \(error.localizedDescription)")
                    let userInfo = ["error": error.localizedDescription]
                    NotificationCenter.default.post(
                        name: Notification.Name(rawValue: self.registrationKey), object: nil, userInfo: userInfo)
                }
            }
            
            
            // [START on_token_refresh]
            func onTokenRefresh() {
                // A rotation of the registration tokens is happening, so the app needs to request a new token.
                print("The GCM registration token needs to be changed.")
                // GGLInstanceID.sharedInstance().token(withAuthorizedEntity: gcmSenderID,
                //scope: kGGLInstanceIDScopeGCM, options: registrationOptions, handler: registrationHandler)
            }
        

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "EventApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

