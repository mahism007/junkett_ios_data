//
//  FourthRegViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 16/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import ImagePicker
import Alamofire
class FourthRegViewController: CommonClassViewController, ImagePickerDelegate {
    
    @IBOutlet weak var imgCam: UIImageView!
    @IBOutlet weak var imgUpload: UIImageView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
      UserDefaults.standard.set(3, forKey: "page")
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.colorwithHexString("C559E8", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("9340B4", alpha: 1.0)?.cgColor,UIColor.systemPurple.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        imgCam.setImageColor(color: UIColor.init(hexString: "D7D7D7"))
      
       
        // Do any additional setup after loading the view.
    }
    func submitFileData(){

          self.startLoading()
          
          DispatchQueue.global(qos: .background).async {

              
            let parameter = [
                 "Type":"PP"
                                ] as [String:Any]
          print(parameter)
          
          let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
          let para: [String: Data] = ["data": jsonData!]

          Alamofire.upload(multipartFormData: { multipartFormData in
          
          if !(RegisterStruct.imageData == nil) {
          
            multipartFormData.append(RegisterStruct.imageData!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
         
          }
          
          for (key, value) in para {
          multipartFormData.append(value, withName: key)
          
          
          }
          
          },
          to:URLConstants.UpdatePic)
          { (result) in
          switch result {
          case .success(let upload, _, _):
          
          upload.uploadProgress(closure: { (progress) in
          
          })
          
          
          upload.responseJSON { response in
          
          
          if let json = response.result.value
          {
          var dict = json as! [String: AnyObject]
          
          if let response = dict["response"]{
           
              let objectmsg = MessageCallServerModel1()
              
              let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
               let objSucess = SuccessServerModel()
                let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
              if(statusStringsuccess == "200"){
                  
                  
               
                  
                  
                  if let data = dict["data"]
                  {
                      
                      var dataArray: [UpProfileModel] = []
                      
                     
                          if let cell = data as? [String:AnyObject]
                          {
                              let object = UpProfileModel()
                              object.setDataInModel(cell: cell)
                              dataArray.append(object)
                          }
                    if(dataArray.count > 0){
                        RegisterStruct.fileId = dataArray[0].FileID
                         UserDefaults.standard.set(true, forKey: "next")
                    }
                          

                      
                      
                      
                      
                  }
                  
                  
                  
                  self.stopLoading()
              }
              else
              {
                  
                  
                  self.showSuccess(strMessage: msg)
                  self.refresh.endRefreshing()
                  self.stopLoading()
                  
                  
              }
              
              
          }
          
          //self.stopLoadingPK()
          }
          
          // self.stopLoadingPK()
          }
          
          case .failure(let encodingError):
          
          self.errorChecking(error: encodingError)
          self.stopLoadingPK(view: self.view)
          print(encodingError.localizedDescription)
          }
          //self.stopLoadingPK()
          }
          // self.stopLoadingPK()
          
          }
    }
    @IBAction func btnCamClicked(_ sender: UIButton) {
                let config = Configuration()
                config.doneButtonTitle = "Finish"
                config.noImagesTitle = "Sorry! There are no images here!"
                config.recordLocation = false
                config.allowVideoSelection = false
                config.allowMultiplePhotoSelection = false
                let imagePicker = ImagePickerController(configuration: config)
                imagePicker.delegate = self

                present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        
//        let storyboard = UIStoryboard(name: "Register", bundle: nil)
//                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "FifthRegViewController") as! FifthRegViewController
//        mainVC.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(mainVC, animated: true)
    }
 // MARK: - ImagePickerDelegate

 func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
   imagePicker.dismiss(animated: true, completion: nil)
 }

 func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
    
    
   /*
   guard images.count > 0 else { return }

   let lightboxImages = images.map {
     return LightboxImage(image: $0)
   }

   let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
   imagePicker.present(lightbox, animated: true, completion: nil)
    */
 }

 func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
    if(images.count > 0){
        imgUpload.image = images[0]
        let imageData = images[0].jpegData(compressionQuality: 1.0)
        RegisterStruct.imageData = imageData
        self.submitFileData()
    }
    
   imagePicker.dismiss(animated: true, completion: nil)
 }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
