//
//  ThirdRegViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import EzPopup
class ThirdRegViewController: UIViewController {

    
    @IBOutlet weak var txtDate: DesignableUITextField!
    
    
    @IBOutlet weak var viewDate: UIView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
       UserDefaults.standard.set(2, forKey: "page")
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.colorwithHexString("E65982", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("B44171", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("842A75", alpha: 1.0)?.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        viewDate.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
         viewDate.layer.borderWidth = 1.0
         viewDate.layer.cornerRadius = 10
        NotificationCenter.default.addObserver(self, selector: #selector(self.showDatareload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "showDatareload")), object: nil)
       
       
    }
    @objc func showDatareload(){
        txtDate.text = RegisterStruct.dateofbirth
         UserDefaults.standard.set(true, forKey: "next")
      //  cellData.lblInterstedIn.text = interest
    }
    @IBAction func btnEditDob(_ sender: DesignableUITextField) {
       
    }
    
    @IBAction func btndobClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "FourthRegViewController") as! FourthRegViewController
        mainVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(mainVC, animated: true)
    }
  
        
        
    }


