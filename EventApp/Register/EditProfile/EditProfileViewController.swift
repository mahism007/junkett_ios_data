//
//  EditProfileViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 04/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import EzPopup
import ImagePicker
import Alamofire
import SDWebImage
class EditProfileViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource ,ImagePickerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{

    
    var imageArray : [ImgModel] =  []
    @IBOutlet weak var tableView: UITableView!
   
    var refreshControl = UIRefreshControl()
   var profileDB:[ProfileListModel] = []
   var profileAPI = ProfileListModelDataAPI()

    override func viewDidLoad() {
        super.viewDidLoad()
        getProfileData()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.GenderData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "Gender")), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.showDatareload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "showDatareload")), object: nil)
           
           
        }
        @objc func showDatareload(){
            cellOther.txtAge.text = RegisterStruct.dateofbirth
             
          //  cellData.lblInterstedIn.text = interest
        }
        @objc func getProfileData(){
 
           
       
         
            var parameter = [ "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                       "UserID": UserDefaults.standard.string(forKey: "UserID")!
                                    ]
          
            
            print(parameter)
            
            profileAPI.serviceCalling(obj: self, parameter: parameter ) { (dict) in
                
                self.profileDB = dict as! [ProfileListModel]
                if(self.profileDB.count > 0 ){
                     let dateFormatter11 = DateFormatter()
                        dateFormatter11.timeZone = NSTimeZone.system
                     
                        dateFormatter11.dateFormat = "MMM dd yyyy HH:mma"

                    
                    let date : Date = dateFormatter11.date(from: self.profileDB[0].DOB)!
                        let dateFormatter21 = DateFormatter()
                             dateFormatter21.timeZone = NSTimeZone.system
                             dateFormatter21.dateFormat = "dd MMM yyyy"
                     
                             let dateFormatter2 = DateFormatter()
                             dateFormatter2.timeZone = NSTimeZone.system
                             dateFormatter2.dateFormat = "dd MMM yyyy"
                    self.cellOther.txtAge.text = dateFormatter2.string(from: date)
                    self.cellOther.txtFirstName.text = self.profileDB[0].FName
                    self.cellOther.txtLastName.text = self.profileDB[0].LName
                    self.cellOther.lblGender.text = self.profileDB[0].Gender
                    RegisterStruct.dateofbirth = dateFormatter2.string(from: date)
                    RegisterStruct.gender = self.profileDB[0].Gender
                    self.imageArray.append(contentsOf: self.profileDB[0].listProfilePic)
                    self.cellDataCover.collectionView.reloadData()
                }
                
            }
        }
    @IBAction func btnAddImageClicked(_ sender: UIButton) {
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = true
        config.allowMultiplePhotoSelection = false
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.delegate = self

        present(imagePicker, animated: true, completion: nil)
        
    }
    @objc func GenderData(){
        cellOther.lblGender.text = RegisterStruct.gender
        cellOther.lblGender.textColor = .black
       }
 


 
    
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
           self.tabBarController?.tabBar.isHidden = true
          self.navigationController?.navigationBar.isHidden = false
 //  self.navigationController?.navigationBar.topItem?.title = ""
          //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
            self.title = "Edit Profile"
            

                
            
        }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }
    @IBAction func btnSaveClicked(_ sender: Any) {
        updateProfile()
    }
    
    @IBAction func btnGender(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Event",bundle : nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "GenderSelectViewController") as! GenderSelectViewController
               
               let popupVC = PopupViewController(contentController: ZIVC, position: .bottom(0.0), popupWidth: self.view.frame.width, popupHeight: 300 )
        
               popupVC.cornerRadius = 5
               popupVC.canTapOutsideToDismiss = true
        
               present(popupVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var cellDataCover : EditProfileTableViewCell!

    var cellOther : EditProfileOtherTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCollect", for: indexPath) as! EditProfileTableViewCell
        cellDataCover = cell
          
//      let flowLayout = AlignedCollectionViewFlowLayout()
//      flowLayout.verticalAlignment = .top
//       flowLayout.horizontalAlignment = .left
//       cell.collectionView.collectionViewLayout = flowLayout
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
        return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOther", for: indexPath) as! EditProfileOtherTableViewCell
            if(cell.lblGender.text == ""){
                cell.lblGender.text = "Gender"
                cell.lblGender.textColor = .darkGray
            }
            cellOther = cell
            getProfileData()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
          if(indexPath.section == 1){
      
          return 640
          }else{
            let hei = (imageArray.count + 1) / 3
            let hei1 = (imageArray.count + 1) % 3
            if(hei1 > 0){
                return CGFloat(hei + 1) * 120
            }else{
                return CGFloat(hei) * 120
            }
              
          }

        
    }
    
    
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        imageData = nil
        addImage()
        
    }
    
    var dateVal = String()
    @IBAction func btnDobClickewd(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Register", bundle: nil)
          let ZIVC = storyBoard.instantiateViewController(withIdentifier: "DatePopupViewController") as! DatePopupViewController
          ZIVC.modalPresentationStyle = UIModalPresentationStyle.popover
           let popupVC = PopupViewController(contentController: ZIVC,  popupWidth: self.view.frame.width - 50, popupHeight: 300)
           popupVC.cornerRadius = 5
           popupVC.canTapOutsideToDismiss = false
           self.present(popupVC, animated: true, completion: nil)
    }
    
   
    func updateProfile() {
        DispatchQueue.global(qos: .background).async {
        let parameter = [
          "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
            "UserID": UserDefaults.standard.string(forKey: "UserID")!,
            "FName": self.cellOther.txtFirstName.text!,
            "LNAme": self.cellOther.txtLastName.text!,
            "Mobile": "",
            "DOB": RegisterStruct.dateofbirth,
            "Gender": RegisterStruct.gender

                   
        ] as [String : String]
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.UpdateProfile, parameters: parameter, successHandler: { (dict:[String : AnyObject]) in
             
             if let response = dict["response"]{
              
                 let objectmsg = MessageCallServerModel1()
                 
                 let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                  let objSucess = SuccessServerModel()
                   let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                 if(statusStringsuccess == "200"){
                 self.stopLoading()
                 self.getProfileData()
                    self.showSuccess(strMessage: "Profile Updated.")
                 
             }else{
                self.stopLoading()
                 self.showError(strMessage: msg)
             }
            }
             
         }) { (err) in
             self.stopLoading()
             print(err.description)
         }
        }
        
    }
    
    func addImage(){

             self.startLoading()
             
             DispatchQueue.global(qos: .background).async {
             
               
                 
               let parameter = [
                "AuthKey": UserDefaults.standard.string(forKey: "SessionID")!,
                   "UserID": UserDefaults.standard.string(forKey: "UserID")!
              
                               
                                   
                                   
                                   ] as [String:String]
             print(parameter)
             
             let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
             let para: [String: Data] = ["data": jsonData!]

             Alamofire.upload(multipartFormData: { multipartFormData in
             
                if !(self.imageData == nil) {
             
                    multipartFormData.append(self.imageData!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
            
             }
       
             for (key, value) in para {
             multipartFormData.append(value, withName: key)
             
             
             }
             
             },
             to:URLConstants.UpdateProfilePic)
             { (result) in
             switch result {
             case .success(let upload, _, _):
             
             upload.uploadProgress(closure: { (progress) in
             
             })
             
             
             upload.responseJSON { response in
             
             
             if let json = response.result.value
             {
                let dict = json as! [String: AnyObject]
             
             if let response = dict["response"]{
              
                 let objectmsg = MessageCallServerModel1()
                 
                 let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                  let objSucess = SuccessServerModel()
                   let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                 if(statusStringsuccess == "200"){
               
                     
                    self.showSuccess(strMessage: msg)
                    self.cellDataCover.collectionView.reloadData()
                     self.stopLoading()
                 }
                 else
                 {
                     
                     
                     self.showError(strMessage: msg)
                     self.refresh.endRefreshing()
                     self.stopLoading()
                     
                     
                 }
                 
                 
             }
             
             //self.stopLoadingPK()
             }
             
             // self.stopLoadingPK()
             }
             
             case .failure(let encodingError):
             
             self.errorChecking(error: encodingError)
             self.stopLoadingPK(view: self.view)
             print(encodingError.localizedDescription)
             }
             //self.stopLoadingPK()
             }
             // self.stopLoadingPK()
             
             }
       }
    
    
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
      imagePicker.dismiss(animated: true, completion: nil)
    }

    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
       
       
      /*
      guard images.count > 0 else { return }

      let lightboxImages = images.map {
        return LightboxImage(image: $0)
      }

      let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
      imagePicker.present(lightbox, animated: true, completion: nil)
       */
    }
    var imageData : Data? = nil
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
           if(images.count > 0){
            
            imageData = images[0].jpegData(compressionQuality: 1.0)!
            addImage()
        }
        
      imagePicker.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(imageArray.count > 0){
            return imageArray.count + 1
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(indexPath.row == imageArray.count){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAdd", for: indexPath) as! EditProfileCollectCollectionViewCell
            
            cell.imgProfile.layer.cornerRadius = 10
            return cell
        }else{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCollect", for: indexPath) as! EditProfileCollectCollectionViewCell

        cell.btnCancel.tag = indexPath.row
        cell.imgProfile.layer.cornerRadius = 10
       
            
            
            
            
            
            let urlString = self.imageArray[indexPath.row].Img
            let imageView = UIImageView()
            
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if NSURL(string: self.imageArray[indexPath.row].Img) != nil {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL?, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        
                        cell.imgProfile.image = image
                        //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                        
                    }else{
                        imageView.image =  UIImage(named: "placed")

                        cell.imgProfile.image = UIImage(named: "placed")
                        // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
                imageView.image =  UIImage(named: "placed")
               
                cell.imgProfile.image = UIImage(named: "placed")
                //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
            }
        return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {

        return CGSize(width: 100 , height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("it worked")
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
