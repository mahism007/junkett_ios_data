//
//  EditProfileCollectCollectionViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 04/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class EditProfileCollectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
}
