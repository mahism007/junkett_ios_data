//
//  EditProfileOtherTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 04/09/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class EditProfileOtherTableViewCell: UITableViewCell {

    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtAge: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
