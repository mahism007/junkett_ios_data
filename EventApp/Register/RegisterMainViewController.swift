//
//  RegisterMainViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 18/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//




import SwiftMessageBar
import UIKit
import liquid_swipe
var trans = false

class RegisterMainViewController: LiquidSwipeContainerController, LiquidSwipeContainerDataSource {
   
    
    
    
    var viewControllers: [UIViewController] = {
        let firstPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "FirstRegViewController")
        let secondPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "SecondRegViewController")
        let thirdPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "ThirdRegViewController")
        let fourthPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "FourthRegViewController")
        let fifthPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "FifthRegViewController")
        let sixthPageVC = UIStoryboard(name: "Register", bundle: nil).instantiateViewController(withIdentifier: "SixthRegViewController")
        var controllers: [UIViewController] = [firstPageVC, secondPageVC,thirdPageVC,fourthPageVC,fifthPageVC,sixthPageVC]
      // var controllers: [UIViewController] = [fifthPageVC,sixthPageVC]
        return controllers
    }()
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
                 self.navigationController?.navigationBar.isHidden = false
           self.navigationController?.navigationBar.topItem?.title = ""
                 //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
    //               self.title = "Alice Goodwin"
                   
                   self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
                   self.navigationController?.navigationBar.shadowImage = UIImage()
                   self.navigationController?.navigationBar.isTranslucent = true
                   self.navigationController?.view.backgroundColor = .clear
          
            
        }
    override func viewDidLoad() {
        super.viewDidLoad()
        datasource = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.ProcessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "Validate")), object: nil)
    }
    @objc func ProcessData(){
    
        let page = UserDefaults.standard.integer(forKey: "page")
        if(page == 1){
            if(RegisterStruct.firstName == ""){
                self.showError(strMessage: "Please Enter First name")
            }else if(RegisterStruct.lastName == ""){
                self.showError(strMessage: "Please Enter Last name")
            }
        }else if(page == 2){
            if(RegisterStruct.gender == ""){
                self.showError(strMessage: "Please Select Gender")
            }
        }else if(page == 3){
            if(RegisterStruct.dateofbirth == ""){
                self.showError(strMessage: "Please Select Date of Birth")
            }
        }else if(page == 4){
            if(RegisterStruct.imageData == nil){
                self.showError(strMessage: "Please Upload Profile Image")
            }else if(RegisterStruct.fileId == ""){
                self.showError(strMessage: "Please Upload Profile Image again")
            }
        }else if(page == 5){
            if(RegisterStruct.categoryList == ""){
                self.showError(strMessage: "Please Select Category")
            }
        }else if(page == 6){
            if(RegisterStruct.country == ""){
                self.showError(strMessage: "Please Select Country")
            }
        }

        
        
    }
    private var uuid: UUID?
      @objc  func showSuccess(strMessage : String) {
            let message = strMessage
            uuid = SwiftMessageBar.showMessage(withTitle: nil, message: message, type: .success, duration: 3, dismiss: true) {
                print("Dismiss callback")
            }
        }
        
        @objc  func showError(strMessage : String) {
            let message = strMessage
            uuid = SwiftMessageBar.showMessage(withTitle: "", message: message, type: .error, duration: 3, dismiss: true) {
                print("Dismiss callback")
            }
        }
    func numberOfControllersInLiquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController) -> Int {
        return viewControllers.count
    }
    
    func liquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController, viewControllerAtIndex index: Int) -> UIViewController {
        return viewControllers[index]
    }
    

}

