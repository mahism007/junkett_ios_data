//
//  ForgetSelectViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DLRadioButton
import DTGradientButton
class ForgetSelectViewController: CommonClassViewController {

    @IBOutlet weak var btnOTP: UIButton!
    @IBOutlet weak var btnPhone: DLRadioButton!
    @IBOutlet weak var btnEmail: DLRadioButton!
    var resultEmail = String()
    var resultMobile = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        let email = UserDefaults.standard.string(forKey: "Email")!
        let mobileNumer = UserDefaults.standard.string(forKey: "Mobile")!
        
        let startingIndex = mobileNumer.index(mobileNumer.startIndex, offsetBy: 3)
        let endingIndex = mobileNumer.index(mobileNumer.endIndex, offsetBy: -2)
        let stars = String(repeating: "*", count: mobileNumer.count - 5)
         resultMobile = mobileNumer.replacingCharacters(in: startingIndex..<endingIndex,
                with: stars)
        
        let startingIndex1 = email.index(email.startIndex, offsetBy: 3)
        let endingIndex1 = email.index(email.endIndex, offsetBy: -2)
        let stars1 = String(repeating: "*", count: email.count - 5)
         resultEmail = email.replacingCharacters(in: startingIndex1..<endingIndex1,
                with: stars1)
        
        btnOTP.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        
             btnOTP.layer.cornerRadius = 20
             btnOTP.layer.masksToBounds = true
        btnEmail.setTitle(resultEmail, for: .normal)
        btnPhone.setTitle(resultMobile, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    var selectType = String()
    var data = String()
       @IBAction func btnSelectTypeClicked(_ sender: DLRadioButton) {
           for button in sender.selectedButtons() {
              
               data =   button.titleLabel!.text!
            if(data == resultMobile){
                selectType = "Mobile"
            }else{
                selectType = "Email"
            }
           }
        
         
       }
    
    var otoListDB:[OTPListModel] = []
    var otpListAPI = OtpForgetReegisterModelDataAPI()
    
    func otpData()  {
        
        RegisterStruct.verificationType = "Forget"

        let paramCategory = [
            "SendOnID": data,
        "SendFor": selectType,
        "OtpType":"ForgotPass"
            ] as [String : String]

        otpListAPI.serviceCalling(obj: self,  parameter : paramCategory  ) { (dict) in
           
            //self.InterestCollectionView.reloadData()
            let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "OTPRegisterViewController") as! OTPRegisterViewController
                        mainVC.modalPresentationStyle = .fullScreen
            mainVC.sendType = self.selectType
                        self.navigationController?.pushViewController(mainVC, animated: true)
                        //self.InterestCollectionView.reloadData()
        }
        
        
    }
    @IBAction func btnSendOtpClicked(_ sender: UIButton) {
        if(selectType != ""){
        otpData()
        }else{
            self.showError(strMessage: "Please select to send OTP.")
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
