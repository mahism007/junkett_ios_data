//
//  SigninViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import FluidTabBarController
import GoogleSignIn
import FBSDKLoginKit

class SigninViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource,LoginButtonDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if let token = AccessToken.current,
            !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }
        NotificationCenter.default.addObserver(forName: .AccessTokenDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            let result =  LoginManagerLoginResult.init(token: AccessToken.current, isCancelled: false, grantedPermissions: ["public_profile", "email"], declinedPermissions: [])
            
            print(result)
        // Print out access token
        print("FB Access Token: \(String(describing: AccessToken.current?.tokenString))")
        }
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().presentingViewController = self
        NotificationCenter.default.addObserver(self,
                                                     selector: #selector(SigninViewController.receiveToggleAuthUINotification(_:)),
                                                     name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                                     object: nil)
        
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        CategoryData()
        CountryData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
// MARK: LoginButtonDelegate
     
 // [END viewdidload]
 
 // [START signout_tapped]
    @IBAction func didTapSignOut(_ sender: AnyObject) {
      GIDSignIn.sharedInstance().signOut()
    }

 // [END signout_tapped]
 
 // [START disconnect_tapped]
 @IBAction func didTapDisconnect(_ sender: AnyObject) {
     // GIDSignIn.sharedInstance().disconnect()
     // [START_EXCLUDE silent]
     // statusText.text = "Disconnecting."
     // [END_EXCLUDE]
 }
 // [END disconnect_tapped]
 
 // [START toggle_auth]

 // [END toggle_auth]
 
 override var preferredStatusBarStyle: UIStatusBarStyle {
     return UIStatusBarStyle.lightContent
 }
 
 //    deinit {
 //        NotificationCenter.default.removeObserver(self,
 //                                                  name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
 //                                                  object: nil)
 //    }
    
    
    @IBAction func btnSignClicked(_ sender: RoundShapeButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
 var name = String()
 @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
     
     if notification.name.rawValue == "ToggleAuthUINotification" {
         //self.toggleAuthUI()
         if notification.userInfo != nil {
             guard let userInfo = notification.userInfo as? [String:String] else { return }
         email = userInfo["email"]!
             name = userInfo["fullName"]!
             print(userInfo)
              self.LoginData(email: self.email, tyep: "Google")
         }
     }
 }
 
//
   //// Facebook
    var email = String()
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
               if (error == nil){
                   let fbDetails = result as! NSDictionary
                   print(fbDetails)
                   let name = fbDetails["name"]
                   if(name != nil){
                   self.name = name as! String
                   }
                   let emailData = fbDetails["email"]
                   if(emailData != nil){
                       self.email = emailData as! String
                    self.LoginData(email: self.email, tyep: "fb")
                   }else{
                       let loginManager = LoginManager()
                       loginManager.logOut()
                    self.showError(strMessage: "Login failed from Facebook")
                       
                   }
                 
                   
               }else{
                   print(error?.localizedDescription ?? "Not found")
               }
           })
       }
       
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
           
       }
//    func loginButtonWillLogin(_ loginButton: FBLoginButton) -> Bool {
//
//    }
       
//    func loginButtonDidCompleteLogin(_ loginButton: FBLoginButton, result: LoginManagerLoginResult) {
//           print(result)
//       }

    var categoryListAPI = CategoryListModelDataAPI()
         
         func CategoryData()  {
         
             
           categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
                 categoryListDB = dict as! [CategoryCountryListModel]
                 //self.InterestCollectionView.reloadData()
                 
             }
             
             
         }
       var countryListAPI = CountryModelDataAPI()
       
       func CountryData()  {
         
        
         countryListAPI.serviceCalling( parameter : [:]  ) { (dict) in
               countryListDB = dict as! [CategoryCountryListModel]
               //self.InterestCollectionView.reloadData()
               
           }
           
           
       }
  @IBAction func btnSignInClicked(_ sender: UIButton) {
    if(cellData.txtEmail.text! == ""){
        self.showError(strMessage: "Please Enter Email")
    }else if(cellData.txtPasword.text! == ""){
        self.showError(strMessage: "Please Enter Password")
    }else{
        LoginData(email: cellData.txtEmail.text!, tyep: "SignIn")
    }
   
    
    
    
    
    
        
  }
    var loginApi = SigninModelDataAPI()
    var GCMToken = String()
    func LoginData(email : String , tyep : String)  {
        if let object = UserDefaults.standard.value(forKey: "Token") {
            
            GCMToken = object as! String
        }
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        print(GCMToken)
         
        let paramCategory = [  "UID": email,
                                  "PWD": cellData.txtPasword.text!,
           "FCM": GCMToken,
           "SDK_Version": build,
           "App_Version": version,
           "Device_Serial_No": UIDevice.current.identifierForVendor!.uuidString,
           "Manufacturer": UIDevice.current.name,
           "Model": UIDevice.current.model,
           "Platform": URLConstants.platform,
           "SHeight": String(UIScreen.main.bounds.height.description),
           "SWidth": String(UIScreen.main.bounds.width.description)
                              ] as [String : String]
    
       
           
        loginApi.serviceCalling(obj: self,  parameter : paramCategory,tyep : tyep  ) { (dict) in
            
            UserDefaults.standard.set(true, forKey: "isLogin")
            let tab = self.createTabBarController()
            tab.modalPresentationStyle = .fullScreen
               self.present(tab, animated: true, completion: nil)
               
           }
           
           
       }
    
    
    @IBAction func btnFBClicked(_ sender: Any) {
        
        // 1
        let loginManager = LoginManager()
        
        if let _ = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out
            
            // 2
            loginManager.logOut()
//            updateButton(isLoggedIn: false)
//            updateMessage(with: nil)
            
        } else {
            // Access token not available -- user already logged out
            // Perform log in
            
            // 3
            loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
                
                // 4
                // Check for error
                guard error == nil else {
                    // Error occurred
                    print(error!.localizedDescription)
                    return
                }
                
                
                // 5
                // Check for cancel
                guard let result = result, !result.isCancelled else {
                    print("User cancelled login")
                    return
                }
                
                // Successfully logged in
                // 6
               // self?.updateButton(isLoggedIn: true)
                
                // 7
                Profile.loadCurrentProfile { (profile, error) in
                    //self?.updateMessage(with: Profile.current?.name)
                    
                    self?.LoginData(email: (Profile.current?.email!)!, tyep: "fb")
                }
            }
        }
    }

    
    private func createTabBarController() -> UITabBarController {
        let tabBarController = FluidTabBarController()
        tabBarController.tabBar.tintColor = UIColor.systemRed
        tabBarController.tabBar.barTintColor = #colorLiteral(red: 0, green: 0.829695344, blue: 0.5481160283, alpha: 1)
       // tabBarController.tabBar.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let vLoc = storyboard.instantiateViewController(withIdentifier: "ActivityByLocNavViewController") as! ActivityByLocNavViewController
        let itemLoc = FluidTabBarItem(title: "Map Activity", image:UIImage(named: "map_Pin"), tag: 0)
        itemLoc.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vLoc.tabBarItem = itemLoc
        
        
        
        let vMessage = storyboard.instantiateViewController(withIdentifier: "MessageNavViewController") as! MessageNavViewController
        let item1 = FluidTabBarItem(title: "Message", image:UIImage(named: "msg"), tag: 1)
        item1.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vMessage.tabBarItem = item1
        
        let vActivities = storyboard.instantiateViewController(withIdentifier: "ActivityShowNavViewController") as! ActivityShowNavViewController
        let item2 = FluidTabBarItem(title: "Activities", image:UIImage(named: "shirt"), tag: 2)
        item2.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vActivities.tabBarItem = item2
        
        let vProfile = storyboard.instantiateViewController(withIdentifier: "ProfileNavViewController") as! ProfileNavViewController
               let itemPro = FluidTabBarItem(title: "Profile", image:UIImage(named: "profile"), tag: 3)
               itemPro.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               vProfile.tabBarItem = itemPro
        
        let vsetting = storyboard.instantiateViewController(withIdentifier: "SettingNavViewController") as! SettingNavViewController
        let itemsetting = FluidTabBarItem(title: "Setting", image:UIImage(named: "setting"), tag: 4)
        itemsetting.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vsetting.tabBarItem = itemsetting

        let viewControllers = [
            vMessage,vProfile,vLoc,vActivities,vsetting
        ]
        
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 2
        return tabBarController
    }
    
 @IBAction func btnForgetPasswordClicked(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Register", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
    mainVC.modalPresentationStyle = .fullScreen
    self.navigationController?.pushViewController(mainVC, animated: true)
       
 }
    @IBAction func btnSignupClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        mainVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(mainVC, animated: true)
        
        
//  let storyboard = UIStoryboard(name: "Register", bundle: nil)
//                                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterMainViewController") as! RegisterMainViewController
//                        mainVC.modalPresentationStyle = .fullScreen
//                        self.navigationController?.pushViewController(mainVC, animated: true)
//          
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
      
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var cellData : SignInTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SignInTableViewCell
        cellData = cell
        cell.txtPasword.isSecureTextEntry = true
//        cell.btnFBLogin.delegate = self
//        cell.btnFBLogin.permissions = ["public_profile", "email"]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.view.frame.height
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
