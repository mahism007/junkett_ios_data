//
//  RegistrationViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 13/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
var lg = 0
class RegistrationViewController: CommonClassViewController ,UITableViewDelegate, UITableViewDataSource,LoginButtonDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        RegisterStruct.emailid = String()
        RegisterStruct.social = String()
        RegisterStruct.regType = String()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
        if let token = AccessToken.current,
            !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }
        NotificationCenter.default.addObserver(forName: .AccessTokenDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            let result =  LoginManagerLoginResult.init(token: AccessToken.current, isCancelled: false, grantedPermissions: ["public_profile", "email"], declinedPermissions: [])
            
            print(result)
        // Print out access token
        print("FB Access Token: \(String(describing: AccessToken.current?.tokenString))")
        }
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().presentingViewController = self
        NotificationCenter.default.addObserver(self,
                                                     selector: #selector(RegistrationViewController.receiveToggleAuthUINotificationREg(_:)),
                                                     name: NSNotification.Name(rawValue: "ToggleAuthUINotificationReg"),
                                                     object: nil)
        
        // Uncomment to automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
      
    }
  @IBAction func btnSignInClicked(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Register", bundle: nil)
            let mainVC  =  storyboard.instantiateViewController(withIdentifier: "SigninNavViewController") as! SigninNavViewController
    mainVC.modalPresentationStyle = .fullScreen
    self.present(mainVC, animated: true, completion: nil)
        
  }
    @IBAction func btnbackClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTermsClicked(_ sender: UIButton) {
        let checkedImage = UIImage(named: "ic_check_box")! as UIImage
        let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
        if(cellData.imgAgree.tag == 0){
            cellData.imgAgree.tag = 1
            cellData.imgAgree.image  = checkedImage
        }else{
            cellData.imgAgree.tag = 0
            cellData.imgAgree.image  = uncheckedImage
        }
               
    }

    @IBAction func btnSinnUpClciked(_ sender: UIButton) {
        RegisterStruct.mobileno = cellData.txtMobile.text!
         RegisterStruct.emailid = cellData.txtEmail.text!
         RegisterStruct.password = cellData.txtPasword.text!
        RegisterStruct.verificationType = "Register"
        RegisterStruct.otp = ""
        RegisterStruct.otpid = ""
        otpData(email: cellData.txtEmail.text!, tyep: "Sign")
    }
    
    var otoListDB:[OTPListModel] = []
      var otpListAPI = OtpRegisterModelDataAPI()
      
    func otpData(email:String, tyep: String)  {
           RegisterStruct.regType = "Signup"
          let paramCategory = [
            "SendOnID": cellData.txtEmail.text!,
          "SendFor": "Email",
          "OtpType":"Registration"
              ] as [String : String]
          
          
          
          otpListAPI.serviceCalling(obj: self,  parameter : paramCategory  ) { (dict) in
              let storyboard = UIStoryboard(name: "Register", bundle: nil)
                      let mainVC  =  storyboard.instantiateViewController(withIdentifier: "OTPRegisterViewController") as! OTPRegisterViewController
              mainVC.modalPresentationStyle = .fullScreen
              self.navigationController?.pushViewController(mainVC, animated: true)
              //self.InterestCollectionView.reloadData()
              
          }
          
          
      }
      @IBAction func didTapSignOut(_ sender: AnyObject) {
         GIDSignIn.sharedInstance().signOut()
       }

    // [END signout_tapped]
    
    // [START disconnect_tapped]
    @IBAction func didTapDisconnect(_ sender: AnyObject) {
        // GIDSignIn.sharedInstance().disconnect()
        // [START_EXCLUDE silent]
        // statusText.text = "Disconnecting."
        // [END_EXCLUDE]
    }
    // [END disconnect_tapped]
    
    // [START toggle_auth]

    // [END toggle_auth]
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    @IBAction func btnGoogleClicked(_ sender: UIButton) {
              lg = 1
              GIDSignIn.sharedInstance().signIn()
              
              
             }
     
    @IBAction func btnFBClicked(_ sender: UIButton) {
           
              // 1
                   let loginManager = LoginManager()
                   
                   if let _ = AccessToken.current {
                       // Access token available -- user already logged in
                       // Perform log out
                       
                       // 2
                       loginManager.logOut()
           //            updateButton(isLoggedIn: false)
           //            updateMessage(with: nil)
                       
                   } else {
                       // Access token not available -- user already logged out
                       // Perform log in
                       
                       // 3
                       loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
                           
                           // 4
                           // Check for error
                           guard error == nil else {
                               // Error occurred
                               print(error!.localizedDescription)
                               return
                           }
                           
                           
                           // 5
                           // Check for cancel
                           guard let result = result, !result.isCancelled else {
                               print("User cancelled login")
                               return
                           }

                           Profile.loadCurrentProfile { (profile, error) in
                               //self?.updateMessage(with: Profile.current?.name)
                            RegisterStruct.emailid = (Profile.current?.email)!
                            RegisterStruct.social = (Profile.current?.userID)!
                                UserDefaults.standard.set(false, forKey: "next")
                             RegisterStruct.regType = "fb"
                                 let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                               let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterMainViewController") as! RegisterMainViewController
                                       mainVC.modalPresentationStyle = .fullScreen
                            self?.navigationController?.pushViewController(mainVC, animated: true)
                             
                           }
                       }
                   }
           
           
          }
    var name = String()
     @objc func receiveToggleAuthUINotificationREg(_ notification: NSNotification) {
         
         if notification.name.rawValue == "ToggleAuthUINotificationReg" {
             //self.toggleAuthUI()
             if notification.userInfo != nil {
                 guard let userInfo = notification.userInfo as? [String:String] else { return }
             email = userInfo["email"]!
                 name = userInfo["fullName"]!
                 print(userInfo)
                RegisterStruct.emailid = self.email
                RegisterStruct.regType = "Google"
                 UserDefaults.standard.set(false, forKey: "next")
                lg = 0
                  let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterMainViewController") as! RegisterMainViewController
                        mainVC.modalPresentationStyle = .fullScreen
                        self.navigationController?.pushViewController(mainVC, animated: true)
             }
         }
     }
     
    //
       //// Facebook
        var email = String()
        
        func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                   if (error == nil){
                       let fbDetails = result as! NSDictionary
                       print(fbDetails)
                       let name = fbDetails["name"]
                       if(name != nil){
                       self.name = name as! String
                       }
                       let emailData = fbDetails["email"]
                       if(emailData != nil){
                           self.email = emailData as! String
                        RegisterStruct.emailid = self.email
                                         UserDefaults.standard.set(false, forKey: "next")
                         let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                       let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterMainViewController") as! RegisterMainViewController
                               mainVC.modalPresentationStyle = .fullScreen
                               self.navigationController?.pushViewController(mainVC, animated: true)
                       }else{
                           let loginManager = LoginManager()
                           loginManager.logOut()
                        self.showError(strMessage: "Login failed from Facebook")
                           
                       }
                     
                       
                   }else{
                       print(error?.localizedDescription ?? "Not found")
                   }
               })
           }
           
        func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
               
           }
    
    @IBAction func btnTermsConditionClicked(_ sender: UIButton) {
        
        
        
        
       }
    @IBAction func btnAgeClicked(_ sender: UIButton) {
         
        let checkedImage = UIImage(named: "ic_check_box")! as UIImage
        let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
        if(cellData.imgAge.tag == 0){
            cellData.imgAge.tag = 1
            cellData.imgAge.image  = checkedImage
        }else{
            cellData.imgAge.tag = 0
            cellData.imgAge.image  = uncheckedImage
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
             self.navigationController?.navigationBar.isHidden = false
       self.navigationController?.navigationBar.topItem?.title = ""
             //  self.navigationController?.navigationItem.backBarButtonItem?.title = "<"
//               self.title = "Alice Goodwin"
               
               self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
               self.navigationController?.navigationBar.shadowImage = UIImage()
               self.navigationController?.navigationBar.isTranslucent = true
               self.navigationController?.view.backgroundColor = .clear
      
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var cellData : RegsiterTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RegsiterTableViewCell
        cellData = cell
        
           let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")! as UIImage
        cell.imgAge.image  = uncheckedImage
        cell.imgAgree.image  = uncheckedImage
        cell.imgAge.tag = 0
        cell.imgAgree.tag = 0
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.view.frame.height
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
