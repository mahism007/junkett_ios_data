//
//  OTPRegisterViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
var categoryListDB:[CategoryCountryListModel] = []
var countryListDB:[CategoryCountryListModel] = []
class OTPRegisterViewController:CommonClassViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoryData()
        CountryData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.tableFooterView = UIView()
      
    }
  
    @IBAction func btnResendOTPClicked(_ sender: UIButton) {
          
                  RegisterStruct.otp = ""
                  RegisterStruct.otpid = ""
                  otpData()
    }
    var otoListDB:[OTPListModel] = []
    var otpListAPI = OtpRRReegisterModelDataAPI()
    var sendType = String()
    func otpData()  {
        var sendFor = String()
        var OtpType = String()
        if(RegisterStruct.verificationType == "Register"){
            sendFor = "Email"
            OtpType = "Registration"
        }else if(RegisterStruct.verificationType == "Forget"){
            sendFor = sendType
            OtpType = "ForgotPass"
        }
        let paramCategory = [
            "SendOnID": RegisterStruct.emailid,
        "SendFor": sendFor,
        "OtpType":OtpType
            ] as [String : String]
        
        
        
        otpListAPI.serviceCalling(obj: self,  parameter : paramCategory  ) { (dict) in
           
            //self.InterestCollectionView.reloadData()
            
        }
        
        
    }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if( cellData.txtNo1.text! != "" && cellData.txtNo2.text! != "" && cellData.txtNo3.text! != "" && cellData.txtNo4.text! != ""){
        let otp = cellData.txtNo1.text! + cellData.txtNo2.text! + cellData.txtNo3.text! + cellData.txtNo4.text!
       otpDataVerify(otp: otp)
        }else{
            self.showError(strMessage: "Please enter OTP.")
        }
      
       
           
    }
  
    
func otpDataVerify(otp : String)  {
        
     
         let parameters = [
                      "OTPGUID":RegisterStruct.otpid,
                    "OTP":otp]
                 
                 self.startLoadingPK(view: self.view)
                
                 WebServices.sharedInstances.sendPostRequest(url: URLConstants.OTPVerify,parameters: parameters, successHandler: { (dict) in
                     
                     self.stopLoadingPK(view: self.view)
                     
                     print("",dict)
                     
                     if let response = dict["response"] {
                         
                         
                         let objectmsg = MessageCallServerModel()
                 let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                         let objSucess = SuccessServerModel()
                 let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                 if(statusStringsuccess == "200"){
                    
                     self.stopLoadingPK(view: self.view)
                                    if(RegisterStruct.verificationType == "Register"){
                                        UserDefaults.standard.set(false, forKey: "next")
                        let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                      let mainVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterMainViewController") as! RegisterMainViewController
                              mainVC.modalPresentationStyle = .fullScreen
                              self.navigationController?.pushViewController(mainVC, animated: true)
                   }else if(RegisterStruct.verificationType == "Forget"){
                        let storyboard = UIStoryboard(name: "Register", bundle: nil)
                                      let mainVC  =  storyboard.instantiateViewController(withIdentifier: "SigninNavViewController") as! SigninNavViewController
                              mainVC.modalPresentationStyle = .fullScreen
                              self.present(mainVC, animated: true, completion: nil)
                   }
                     
                           
                         }
                         else
                         {
                             self.stopLoadingPK(view: self.view)
                            self.showError(strMessage: "Invalid OTP")
                         }
                     }
                     
                     
                     
                     
                 }, failureHandler: { (error) in
                     
                   
                      self.stopLoadingPK(view: self.view)
                     
                     self.errorChecking(error: error[0])
                     
                     
                     
                 })
              
        
    }
    
    
    
    
   var categoryListAPI = CategoryListModelDataAPI()
        
        func CategoryData()  {
        
            
          categoryListAPI.serviceCalling(  parameter : [:]  ) { (dict) in
                categoryListDB = dict as! [CategoryCountryListModel]
                //self.InterestCollectionView.reloadData()
                
            }
            
            
        }
      var countryListAPI = CountryModelDataAPI()
      
      func CountryData()  {
        
          
        countryListAPI.serviceCalling( parameter : [:]  ) { (dict) in
              countryListDB = dict as! [CategoryCountryListModel]
              //self.InterestCollectionView.reloadData()
              
          }
          
          
      }
      
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
      
        
    }
    @IBAction func txtNo1Changed(_ sender: UITextField) {
           
           if(cellData.txtNo1.text != "" && cellData.txtNo2.text != "" && cellData.txtNo3.text != "" && cellData.txtNo4.text != ""){
              
           }
           cellData.txtNo2.becomeFirstResponder()
       }
       @IBAction func txtNo2Changed(_ sender: UITextField) {
           if(cellData.txtNo1.text != "" && cellData.txtNo2.text != "" && cellData.txtNo3.text != "" && cellData.txtNo4.text != ""){
               
           }
            cellData.txtNo3.becomeFirstResponder()
       }
       @IBAction func txtNo3Changed(_ sender: UITextField) {
           if(cellData.txtNo1.text != "" && cellData.txtNo2.text != "" && cellData.txtNo3.text != "" && cellData.txtNo4.text != ""){
               
           }
            cellData.txtNo4.becomeFirstResponder()
       }
       @IBAction func txtNo4Changed(_ sender: UITextField) {
           if(cellData.txtNo1.text != "" && cellData.txtNo2.text != "" && cellData.txtNo3.text != "" && cellData.txtNo4.text != ""){
              
           }
       }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var cellData : OtpTableViewCell!
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OtpTableViewCell
        cellData = cell
        
    cellData.txtNo1.becomeFirstResponder()
      cellData.txtNo1.keyboardType = UIKeyboardType.numberPad
       cellData.txtNo2.keyboardType = UIKeyboardType.numberPad
       cellData.txtNo3.keyboardType = UIKeyboardType.numberPad
       cellData.txtNo4.keyboardType = UIKeyboardType.numberPad
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.view.frame.height
        
    }
    
  
    
    
    /*
    // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation be_  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
