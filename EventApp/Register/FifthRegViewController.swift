//
//  FifthRegViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 17/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class FifthRegViewController: CommonClassViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    @IBOutlet weak var heightCollect: NSLayoutConstraint!

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       UserDefaults.standard.set(4, forKey: "page")
    let gradientLayer = CAGradientLayer()
            gradientLayer.frame = self.view.bounds
            gradientLayer.colors = [UIColor.colorwithHexString("8BB755", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("7DA64A", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("648738", alpha: 1.0)?.cgColor]
            self.view.layer.insertSublayer(gradientLayer, at: 0)
       
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
          if(categoryListDB.count < 5){
             heightCollect.constant = 200
          }else{
            heightCollect.constant = collectionView.collectionViewLayout.collectionViewContentSize.height - 100;
         }
      //  heightCollect.constant = collectionView.collectionViewLayout.collectionViewContentSize.height - 100;
       
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnTypeClicked(_ sender: UIButton) {
RegisterStruct.category = []
  if(categoryListDB.count > 0){
      

      
      
          if(categoryListDB[sender.tag].boolSelected == true){
          categoryListDB[sender.tag].boolSelected = false
          }else if(categoryListDB[sender.tag].boolSelected == false){
          var ii = ""
            var iii = 0
            categoryListDB[sender.tag].boolSelected = true
              for i in 0...categoryListDB.count - 1 {
                  if(categoryListDB[i].boolSelected == true){
                    ii = ii + categoryListDB[i].ID + ","
                     iii = iii + 1
                  }
                  
              }
            if(iii > 1){
            ii.remove(at: ii.index(before: ii.endIndex))
                RegisterStruct.categoryList = ii
            }

          }
          

  }
        UserDefaults.standard.set(true, forKey: "next")
        collectionView.reloadData()
      
    }
 
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(categoryListDB.count > 0){
            return categoryListDB.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

        cell.btnType.tag = indexPath.row
        cell.lblType.text = categoryListDB[indexPath.row].Name
        cell.lblType.textColor = .white
        cell.viewType.backgroundColor = .clear
        cell.viewType.layer.borderWidth = 1.0
        cell.viewType.layer.cornerRadius = 10
        if(categoryListDB[indexPath.row].boolSelected == true){
            cell.viewType.layer.borderColor = UIColor.systemRed.cgColor
        }else{
            cell.viewType.layer.borderColor = UIColor.white.cgColor
        }
       if let url = NSURL(string: categoryListDB[indexPath.row].IMGResource) {
        cell.imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
       }
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if(categoryListDB.count > 0){
        let label = UILabel(frame: CGRect.zero)
        label.text = categoryListDB[indexPath.item].Name
        label.sizeToFit()
        return CGSize(width: 50 + label.frame.width, height: 40)
        }else{
               return CGSize(width: 50, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("it worked")
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

