//
//  ModelRegister.swift
//  EventApp
//
//  Created by Bunga Mahesh on 19/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import Foundation

import FBSDKLoginKit

class CategoryCountryListModel: NSObject {
    
    
    var ID = String()
    var Name = String()
    var ImagePath = String()
    var boolSelected = false
  var Is_Deleted = String()
     var ColorCode = String()
     var IMGResource = String()
  var StateID = String()
    var StateName = String()
    var StateImagePath = String()
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["StateID"] is NSNull || cell["StateID"] == nil {
            self.StateID = ""
        }else{
            let app = cell["StateID"]
            self.StateID = (app?.description)!
            
        }
        
        if cell["StateName"] is NSNull || cell["StateName"] == nil {
            self.StateName = ""
        }else{
            let app = cell["StateName"]
            self.StateName = (app?.description)!
            
        }
        if cell["StateImagePath"] is NSNull || cell["StateImagePath"] == nil {
            self.StateImagePath = ""
        }else{
            let app = cell["StateImagePath"]
            self.StateImagePath = (app?.description)!
            
        }
       
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.ID = ""
        }else{
            let app = cell["ID"]
            self.ID = (app?.description)!
            
        }
        
        if cell["Name"] is NSNull || cell["Name"] == nil {
            self.Name = ""
        }else{
            let app = cell["Name"]
            self.Name = (app?.description)!
            
        }
        if cell["ImagePath"] is NSNull || cell["ImagePath"] == nil {
            self.ImagePath = ""
        }else{
            let app = cell["ImagePath"]
            self.ImagePath = (app?.description)!
            
        }
        if cell["ColorCode"] is NSNull || cell["ColorCode"] == nil {
            self.ColorCode = ""
        }else{
            let app = cell["ColorCode"]
            self.ColorCode = (app?.description)!
            
        }
        if cell["Is_Deleted"] is NSNull || cell["Is_Deleted"] == nil {
            self.Is_Deleted = ""
        }else{
            let app = cell["Is_Deleted"]
            self.Is_Deleted = (app?.description)!
            
        }
        if cell["IMGResource"] is NSNull || cell["IMGResource"] == nil {
            self.IMGResource = ""
        }else{
            let app = cell["IMGResource"]
            self.IMGResource = (app?.description)!
            
        }
      
       
       
    }
    
}

class OTPListModel: NSObject {
    
    
    var OTPGUID = String()
    var OTP = String()
   
 
    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["OTP"] is NSNull || cell["OTP"] == nil {
            self.OTP = ""
        }else{
            let app = cell["OTP"]
            self.OTP = (app?.description)!
            RegisterStruct.otp = self.OTP
            print(self.OTP)
        }
        
        if cell["OTPGUID"] is NSNull || cell["OTPGUID"] == nil {
            self.OTPGUID = ""
        }else{
            let app = cell["OTPGUID"]
            self.OTPGUID = (app?.description)!
            RegisterStruct.otpid = self.OTPGUID
        }

      
       
    }
    
}
class LoginListModel: NSObject {
    
    
    var SessionID = String()
    var UserID = String()
   
 
    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["UserID"] is NSNull || cell["UserID"] == nil {
            self.UserID = ""
        }else{
            let app = cell["UserID"]
            self.UserID = (app?.description)!
            UserDefaults.standard.set(self.UserID, forKey: "UserID")
        }
        
        if cell["SessionID"] is NSNull || cell["SessionID"] == nil {
            self.SessionID = ""
        }else{
            let app = cell["SessionID"]
            self.SessionID = (app?.description)!
            UserDefaults.standard.set(self.SessionID, forKey: "SessionID")
        }

      
       
    }
    
}
class UpProfileModel: NSObject {
    
    
    var FileID = String()
    
   
 
    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["FileID"] is NSNull || cell["FileID"] == nil {
            self.FileID = ""
        }else{
            let app = cell["FileID"]
            self.FileID = (app?.description)!
       
        }


      
       
    }
    
}
class ProfileListModel: NSObject {

var StateID = String()

var ShowMyLocation = String()

var Registration_Mode = String()

var Social_AC_ID = String()

var State = String()

var CDate = String()

var Is_Deleted = String()

var ShowMyMobile = String()

var ShowDOB = String()

var BioDescription = String()

var InvitesCount = String()

var MSGReceivedFromAll = String()

var MSGReceivedFromMates = String()

var MSGReceivedFromPeople = String()

var ShowNotification = String()

var LName = String()

var BlockedUserCount = String()

var Gender = String()

var InstagramUserID = String()

var UID = String()

var InterestedCategoryCSV = String()

var InstagramUserName = String()

var MyMatesCount = String()

var Latitude = String()

var ShowMyEmail = String()

var MName = String()

var Email = String()

var FacebookUserID = String()

var Longitude = String()

var MobileVerificationStatus = String()

var ShowMyGender = String()

var ProfilePicsArray = String()

var Address = String()

var FavoriteActivityCount = String()

var ActiveStatus = String()

var MyActivityCount = String()

var ID = String()

var EmailVerificationStatus = String()

var FacebookUserName = String()

var UDate = String()

var DOB = String()

var Mobile = String()

var FName = String()
    
    var MateRequestedBy = String()

    var MateRequesteStatus = String()

    var MateRequesteID = String()

var listProfilePic = [ImgModel]()
    var listCategory = [CategoryCountryListModel]()
func setDataInModel(cell:[String:AnyObject])
{
    if let data1 = cell["ProfilePicsArray"] as? [AnyObject]
      {
          
          var dataArray1: [ImgModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = ImgModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
              listProfilePic = dataArray1
              
          }
          
      }
      if let data1 = cell["CategoryNames"] as? [AnyObject]
      {
          
          var dataArray1: [CategoryCountryListModel] = []
          if(data1.count  >  0){
              for i in 0..<data1.count
              {
                  
                  if let str = data1[i] as? [String:AnyObject]
                  {
                      let object1 = CategoryCountryListModel()
                      object1.setDataInModel(cell: str)
                      dataArray1.append(object1)
                  }
                  
              }
              listCategory = dataArray1
              
          }
          
      }
    

    
    if cell["MateRequestedBy"] is NSNull || cell["MateRequestedBy"] == nil {
     self.MateRequestedBy = ""
    }else{
    let app = cell["MateRequestedBy"]
    self.MateRequestedBy = (app?.description)!
    }

    if cell["MateRequesteStatus"] is NSNull || cell["MateRequesteStatus"] == nil {
     self.MateRequesteStatus = ""
    }else{
    let app = cell["MateRequesteStatus"]
    self.MateRequesteStatus = (app?.description)!
    }

    if cell["MateRequesteID"] is NSNull || cell["MateRequesteID"] == nil {
     self.MateRequesteID = ""
    }else{
    let app = cell["MateRequesteID"]
    self.MateRequesteID = (app?.description)!
    }
    
    
    
if cell["StateID"] is NSNull || cell["StateID"] == nil {
 self.StateID = ""
}else{
let app = cell["StateID"]
self.StateID = (app?.description)!
}

if cell["ShowMyLocation"] is NSNull || cell["ShowMyLocation"] == nil {
 self.ShowMyLocation = ""
}else{
let app = cell["ShowMyLocation"]
self.ShowMyLocation = (app?.description)!
}

if cell["Registration_Mode"] is NSNull || cell["Registration_Mode"] == nil {
 self.Registration_Mode = ""
}else{
let app = cell["Registration_Mode"]
self.Registration_Mode = (app?.description)!
}

if cell["Social_AC_ID"] is NSNull || cell["Social_AC_ID"] == nil {
 self.Social_AC_ID = ""
}else{
let app = cell["Social_AC_ID"]
self.Social_AC_ID = (app?.description)!
}

if cell["State"] is NSNull || cell["State"] == nil {
 self.State = ""
}else{
let app = cell["State"]
self.State = (app?.description)!
}

if cell["CDate"] is NSNull || cell["CDate"] == nil {
 self.CDate = ""
}else{
let app = cell["CDate"]
self.CDate = (app?.description)!
}

if cell["Is_Deleted"] is NSNull || cell["Is_Deleted"] == nil {
 self.Is_Deleted = ""
}else{
let app = cell["Is_Deleted"]
self.Is_Deleted = (app?.description)!
}

if cell["ShowMyMobile"] is NSNull || cell["ShowMyMobile"] == nil {
 self.ShowMyMobile = ""
}else{
let app = cell["ShowMyMobile"]
self.ShowMyMobile = (app?.description)!
}

if cell["ShowDOB"] is NSNull || cell["ShowDOB"] == nil {
 self.ShowDOB = ""
}else{
let app = cell["ShowDOB"]
self.ShowDOB = (app?.description)!
}

if cell["BioDescription"] is NSNull || cell["BioDescription"] == nil {
 self.BioDescription = ""
}else{
let app = cell["BioDescription"]
self.BioDescription = (app?.description)!
}

if cell["InvitesCount"] is NSNull || cell["InvitesCount"] == nil {
 self.InvitesCount = ""
}else{
let app = cell["InvitesCount"]
self.InvitesCount = (app?.description)!
}

if cell["MSGReceivedFromAll"] is NSNull || cell["MSGReceivedFromAll"] == nil {
 self.MSGReceivedFromAll = ""
}else{
let app = cell["MSGReceivedFromAll"]
self.MSGReceivedFromAll = (app?.description)!
}

if cell["MSGReceivedFromMates"] is NSNull || cell["MSGReceivedFromMates"] == nil {
 self.MSGReceivedFromMates = ""
}else{
let app = cell["MSGReceivedFromMates"]
self.MSGReceivedFromMates = (app?.description)!
}

if cell["MSGReceivedFromPeople"] is NSNull || cell["MSGReceivedFromPeople"] == nil {
 self.MSGReceivedFromPeople = ""
}else{
let app = cell["MSGReceivedFromPeople"]
self.MSGReceivedFromPeople = (app?.description)!
}

if cell["ShowNotification"] is NSNull || cell["ShowNotification"] == nil {
 self.ShowNotification = ""
}else{
let app = cell["ShowNotification"]
self.ShowNotification = (app?.description)!
}

if cell["LName"] is NSNull || cell["LName"] == nil {
 self.LName = ""
}else{
let app = cell["LName"]
self.LName = (app?.description)!
}

if cell["BlockedUserCount"] is NSNull || cell["BlockedUserCount"] == nil {
 self.BlockedUserCount = ""
}else{
let app = cell["BlockedUserCount"]
self.BlockedUserCount = (app?.description)!
}

if cell["Gender"] is NSNull || cell["Gender"] == nil {
 self.Gender = ""
}else{
let app = cell["Gender"]
self.Gender = (app?.description)!
}

if cell["InstagramUserID"] is NSNull || cell["InstagramUserID"] == nil {
 self.InstagramUserID = ""
}else{
let app = cell["InstagramUserID"]
self.InstagramUserID = (app?.description)!
}

if cell["UID"] is NSNull || cell["UID"] == nil {
 self.UID = ""
}else{
let app = cell["UID"]
self.UID = (app?.description)!
}

if cell["InterestedCategoryCSV"] is NSNull || cell["InterestedCategoryCSV"] == nil {
 self.InterestedCategoryCSV = ""
}else{
let app = cell["InterestedCategoryCSV"]
self.InterestedCategoryCSV = (app?.description)!
}

if cell["InstagramUserName"] is NSNull || cell["InstagramUserName"] == nil {
 self.InstagramUserName = ""
}else{
let app = cell["InstagramUserName"]
self.InstagramUserName = (app?.description)!
}

if cell["MyMatesCount"] is NSNull || cell["MyMatesCount"] == nil {
 self.MyMatesCount = ""
}else{
let app = cell["MyMatesCount"]
self.MyMatesCount = (app?.description)!
}

if cell["Latitude"] is NSNull || cell["Latitude"] == nil {
 self.Latitude = ""
}else{
let app = cell["Latitude"]
self.Latitude = (app?.description)!
}

if cell["ShowMyEmail"] is NSNull || cell["ShowMyEmail"] == nil {
 self.ShowMyEmail = ""
}else{
let app = cell["ShowMyEmail"]
self.ShowMyEmail = (app?.description)!
}

if cell["MName"] is NSNull || cell["MName"] == nil {
 self.MName = ""
}else{
let app = cell["MName"]
self.MName = (app?.description)!
}

if cell["Email"] is NSNull || cell["Email"] == nil {
 self.Email = ""
}else{
let app = cell["Email"]
self.Email = (app?.description)!
}

if cell["FacebookUserID"] is NSNull || cell["FacebookUserID"] == nil {
 self.FacebookUserID = ""
}else{
let app = cell["FacebookUserID"]
self.FacebookUserID = (app?.description)!
}

if cell["Longitude"] is NSNull || cell["Longitude"] == nil {
 self.Longitude = ""
}else{
let app = cell["Longitude"]
self.Longitude = (app?.description)!
}

if cell["MobileVerificationStatus"] is NSNull || cell["MobileVerificationStatus"] == nil {
 self.MobileVerificationStatus = ""
}else{
let app = cell["MobileVerificationStatus"]
self.MobileVerificationStatus = (app?.description)!
}

if cell["ShowMyGender"] is NSNull || cell["ShowMyGender"] == nil {
 self.ShowMyGender = ""
}else{
let app = cell["ShowMyGender"]
self.ShowMyGender = (app?.description)!
}

if cell["ProfilePicsArray"] is NSNull || cell["ProfilePicsArray"] == nil {
 self.ProfilePicsArray = ""
}else{
let app = cell["ProfilePicsArray"]
self.ProfilePicsArray = (app?.description)!
}

if cell["Address"] is NSNull || cell["Address"] == nil {
 self.Address = ""
}else{
let app = cell["Address"]
self.Address = (app?.description)!
}

if cell["FavoriteActivityCount"] is NSNull || cell["FavoriteActivityCount"] == nil {
 self.FavoriteActivityCount = ""
}else{
let app = cell["FavoriteActivityCount"]
self.FavoriteActivityCount = (app?.description)!
}

if cell["ActiveStatus"] is NSNull || cell["ActiveStatus"] == nil {
 self.ActiveStatus = ""
}else{
let app = cell["ActiveStatus"]
self.ActiveStatus = (app?.description)!
}

if cell["MyActivityCount"] is NSNull || cell["MyActivityCount"] == nil {
 self.MyActivityCount = ""
}else{
let app = cell["MyActivityCount"]
self.MyActivityCount = (app?.description)!
}

if cell["ID"] is NSNull || cell["ID"] == nil {
 self.ID = ""
}else{
let app = cell["ID"]
self.ID = (app?.description)!
}

if cell["EmailVerificationStatus"] is NSNull || cell["EmailVerificationStatus"] == nil {
 self.EmailVerificationStatus = ""
}else{
let app = cell["EmailVerificationStatus"]
self.EmailVerificationStatus = (app?.description)!
}

if cell["FacebookUserName"] is NSNull || cell["FacebookUserName"] == nil {
 self.FacebookUserName = ""
}else{
let app = cell["FacebookUserName"]
self.FacebookUserName = (app?.description)!
}

if cell["UDate"] is NSNull || cell["UDate"] == nil {
 self.UDate = ""
}else{
let app = cell["UDate"]
self.UDate = (app?.description)!
}

if cell["DOB"] is NSNull || cell["DOB"] == nil {
 self.DOB = ""
}else{
let app = cell["DOB"]
self.DOB = (app?.description)!
}

if cell["Mobile"] is NSNull || cell["Mobile"] == nil {
 self.Mobile = ""
}else{
let app = cell["Mobile"]
self.Mobile = (app?.description)!
}

if cell["FName"] is NSNull || cell["FName"] == nil {
 self.FName = ""
}else{
let app = cell["FName"]
self.FName = (app?.description)!
}

}
}



class ForgetListModel: NSObject {
    
    
    var Email = String()
    var UserID = String()
    var Mobile = String()
   

    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["UserID"] is NSNull || cell["UserID"] == nil {
            self.UserID = ""
        }else{
            let app = cell["UserID"]
            self.UserID = (app?.description)!
            UserDefaults.standard.set(self.UserID, forKey: "UserID")
        }
        
        if cell["Email"] is NSNull || cell["Email"] == nil {
            self.Email = ""
        }else{
            let app = cell["Email"]
            self.Email = (app?.description)!
            UserDefaults.standard.set(self.Email, forKey: "Email")
        }
        if cell["Mobile"] is NSNull || cell["Mobile"] == nil {
            self.Mobile = ""
        }else{
            let app = cell["Mobile"]
            self.Mobile = (app?.description)!
            UserDefaults.standard.set(self.Mobile, forKey: "Mobile")
        }
      
       
    }
    
}
class ProfileListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj: EditProfileViewController ,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.GetMyProfile, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ProfileListModel] = []
                            
                           
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ProfileListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class CategoryListModelDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
//        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.MasterCategoryListGet, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
               if let response = dict["response"]{
                
                   let objectmsg = MessageCallServerModel1()
                   
                   let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    let objSucess = SuccessServerModel()
                     let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                   if(statusStringsuccess == "200"){
                        
                       
                        
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [CategoryCountryListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = CategoryCountryListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
                        
                        
                        
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
                       
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                     obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
//                obj.stopLoading()
            })
            
            
        }
        else{
           
//            obj.refresh.endRefreshing()
//            obj.stopLoading()
        }
        
        
    }
    
    
}
class CountryModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
       // obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.MasterStateListGet, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        
                     
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [CategoryCountryListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = CategoryCountryListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
//                        obj.showSuccess(strMessage: msg)
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
//                obj.refresh.endRefreshing()
//                // obj.errorChecking(error: error)
//                obj.stopLoading()
            })
            
            
        }
        else{
            
//            obj.refresh.endRefreshing()
//            obj.stopLoading()
        }
        
        
    }
    
    
}
class OtpRegisterModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:RegistrationViewController,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.CreateOTP, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        
                     obj.stopLoading()
                        
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [OTPListModel] = []
                            
                            
                                
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = OTPListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.stopLoading()
                        obj.refresh.endRefreshing()
                        
                    }
                    else
                    {
                        
                        
                        obj.showSuccess(strMessage: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class OtpRRReegisterModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:OTPRegisterViewController,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.CreateOTP, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["Response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        
                     
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [OTPListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = OTPListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.showSuccess(strMessage: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class OtpForgetReegisterModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ForgetSelectViewController,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.CreateOTP, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["Response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        
                     
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [OTPListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = OTPListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.showSuccess(strMessage: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class SigninModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SigninViewController,parameter : [String:Any],tyep : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.Login, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){

                        if let data = dict["data"]
                        {
                            
                            var dataArray: [LoginListModel] = []
                            
                           
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = LoginListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                          
                               success(dataArray as AnyObject)
                            
                        }
                     
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        if(tyep == "fb"){
                            let loginManager = LoginManager()
                            loginManager.logOut()
                        }
                        
                        obj.showError(strMessage: "Invalid Login Credentials")
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}
class SigninRegisterModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SixthRegViewController,parameter : [String:Any],tyep : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.Login, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){

                        if let data = dict["data"]
                        {
                            
                            var dataArray: [LoginListModel] = []
                            
                           
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = LoginListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                          
                               success(dataArray as AnyObject)
                            
                        }
                     
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        if(tyep == "fb"){
                            let loginManager = LoginManager()
                            loginManager.logOut()
                        }
                        
                        obj.showError(strMessage: "Invalid Login Credentials")
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


class ForgetModelDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ForgetPasswordViewController,parameter : [String:Any], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url:  URLConstants.ForgotPWDRequest, parameters: parameter, successHandler: { (dict:[String:AnyObject]) in
                
                
                if let response = dict["response"]{
                 
                    let objectmsg = MessageCallServerModel1()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                     let objSucess = SuccessServerModel()
                      let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusStringsuccess == "200"){
                        
                        
                     
                        
                        
                        if let data = dict["data"]
                        {
                            
                            var dataArray: [ForgetListModel] = []
                            
                           
                                if let cell = data as? [String:AnyObject]
                                {
                                    let object = ForgetListModel()
                                    object.setDataInModel(cell: cell)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        obj.showSuccess(strMessage: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


//class MapfeedModelDataAPI
//{
//
//    var reachablty = Reachability()!
//
//
//    func serviceCalling(obj:ActivityByLocationViewController,parameter:[String:Any], success:@escaping (AnyObject)-> Void)
//    {
//
//
//
//        if(reachablty.connection != .none)
//        {
//            obj.startLoading()
//            WebServices.sharedInstances.sendPostRequest(url: URLConstants.UpdateCurrentLocation, parameters: parameter, successHandler: { (dict) in
//
//                if let response = dict["response"]{
//                    print(response)
//                    let statusString : String = response["status"] as! String
//                    let objectmsg = MessageCallServerModel()
//
//                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
//                    if(statusString == "ok")
//                    {
//                        obj.stopLoading()
//
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray : [HandsUppFeedListModel] = []
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = HandsUppFeedListModel()
//                                    object.setDataInModel(cell: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//
//
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
//                    }
//                    else
//                    {
//
//
//
//                        obj.refresh.endRefreshing()
//                        obj.stopLoading()
//
//                    }
//
//
//                }
//
//
//
//            }, failureHandler: { (error) in
//                let errorStr : String = error.description
//
//                print("DATA:error",errorStr)
//
//                // obj.errorChecking(error: error)
//                obj.stopLoading()
//            })
//
//
//        }
//        else{
//            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
//
//            obj.label.isHidden = false
//
//            // obj.noDataLabel(text: "No Data Found!" )
//            obj.refresh.endRefreshing()
//            obj.stopLoading()
//        }
//
//
//    }
//
//
//}
