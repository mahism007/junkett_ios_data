//
//  FirstRegViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class FirstRegViewController: CommonClassViewController {

    @IBOutlet weak var txtLName: DesignableUITextField!
    @IBOutlet weak var txtName: DesignableUITextField!
    
    
    @IBOutlet weak var viewName: UIView!
    
    @IBOutlet weak var viewLName: UIView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
      UserDefaults.standard.set(1, forKey: "page")
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.colorwithHexString("15C3E9", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("247EBC", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("344898", alpha: 1.0)?.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        viewName.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
         viewName.layer.borderWidth = 1.0
         viewName.layer.cornerRadius = 10
        
         viewLName.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
         viewLName.layer.borderWidth = 1.0
         viewLName.layer.cornerRadius = 10
        // Do any additional setup after loading the view.
    }

    @IBAction func btnFnameDob(_ sender: DesignableUITextField) {
        RegisterStruct.firstName = sender.text!
        if(RegisterStruct.lastName != ""){
            UserDefaults.standard.set(true, forKey: "next")
        }
    }
    @IBAction func btnLnameDob(_ sender: DesignableUITextField) {
        RegisterStruct.lastName = sender.text!
        if(RegisterStruct.firstName != ""){
            UserDefaults.standard.set(true, forKey: "next")
        }
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
