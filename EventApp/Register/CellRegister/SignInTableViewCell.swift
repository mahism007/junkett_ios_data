//
//  SignInTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DTGradientButton
import FBSDKLoginKit
class SignInTableViewCell: UITableViewCell {
    @IBOutlet weak var txtPasword: DesignableUITextField!
       @IBOutlet weak var txtEmail: DesignableUITextField!
    
   
   
    @IBOutlet weak var btnSigin: RoundShapeButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        
        
        txtEmail.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtEmail.layer.borderWidth = 1.0
        txtEmail.layer.cornerRadius = 20
        txtPasword.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
               txtPasword.layer.borderWidth = 1.0
               txtPasword.layer.cornerRadius = 20
        btnSigin.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        
             btnSigin.layer.cornerRadius = 20
             btnSigin.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
