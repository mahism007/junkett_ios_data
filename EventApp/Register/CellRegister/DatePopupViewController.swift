//
//  DatePopupViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 19/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class DatePopupViewController: CommonClassViewController {
var dateVal = Date()
    var popInt = 0
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var btnCancel: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(popInt == 0){
        if(RegisterStruct.dateofbirth == ""){
                           btnCancel.isHidden = true
                       }else{
                           btnCancel.isHidden = false
                       }
        }else if(popInt == 1){
            if(RegisterStruct.startDate == ""){
                               btnCancel.isHidden = true
                           }else{
                               btnCancel.isHidden = false
                           }
        }else if(popInt == 2){
            if(RegisterStruct.endDate == ""){
                               btnCancel.isHidden = true
                           }else{
                               btnCancel.isHidden = false
                           }
        }
        view.addSubview(MDate)
                          NSLayoutConstraint.activate([
                              MDate.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0),
                              MDate.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
                              MDate.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
                              MDate.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75)
                          ])
                          MDate.selectDate = Date()
       
                          view.addSubview(Today)
        Today.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
                         
        Today.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        let formatter = DateFormatter()
                          formatter.dateFormat = "dd/MM/yyyy"
                          let formatter1 = DateFormatter()
                          formatter1.dateFormat = "dd MMM yyyy"
                          let date = formatter.string(from: Date())
                          let date1 = formatter1.string(from: Date())
                          RegisterStruct.dateofbirth = date
                          Label.text = date1
                          view.addSubview(Label)
         Label.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
         
                          Label.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
       
        // Do any additional setup after loading the view.
 
            
           
            
            
        }
    
    lazy var MDate : MDatePickerView = {
                   let mdate = MDatePickerView()
                   mdate.delegate = self
                   mdate.Color = UIColor(red: 0/255, green: 178/255, blue: 113/255, alpha: 1)
                   mdate.cornerRadius = 14
                   mdate.translatesAutoresizingMaskIntoConstraints = false
                   mdate.from = 1920
                   mdate.to = 2050
                   return mdate
               }()
               
               let Today : UIButton = {
                   let but = UIButton(type:.system)
                   but.setTitle("Done", for: .normal)
                but.setTitleColor(UIColor.colorwithHexString("842A75", alpha: 1.0), for: .normal)
                   but.addTarget(self, action: #selector(today), for: .touchUpInside)
                but.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
                   but.translatesAutoresizingMaskIntoConstraints = false
                   return but
               }()
              
               let Label : UILabel = {
                   let label = UILabel()
                
                   label.textColor = UIColor.systemPurple
                   label.font = UIFont.boldSystemFont(ofSize: 16)
                   label.translatesAutoresizingMaskIntoConstraints = false
                   return label
               }()
              
               @objc func today() {
                
                let dateOfBirth = dateVal

                

                let gregorian = Calendar(identifier: .gregorian)

                
               // let age = gregorian.dateComponents([.year,.month,.day], from: dateOfBirth)
                let age = gregorian.dateComponents([.year,.month,.day], from: dateOfBirth, to: Date())
                if(popInt == 0){
                if age.year! < 18 {
                    // user is under 18
                    showInfo(strMessage: "You are not 18 years older.")
                }else{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy"
                    let date1 = formatter.string(from: dateVal)
                    let formatter1 = DateFormatter()
                    formatter1.dateFormat = "yyyy-MM-dd"
                    let date11 = formatter1.string(from: dateVal)
                    
                    if(popInt == 0){
                    RegisterStruct.dateofbirth = date1
                        RegisterStruct.dateofbirth1 = date11
                    }
                    }
                }else{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd MMM yyyy"
                    let date1 = formatter.string(from: dateVal)
                    let formatter1 = DateFormatter()
                    formatter1.dateFormat = "yyyy-MM-dd"
                    let date11 = formatter1.string(from: dateVal)
                    
                       if(popInt == 1){

                        RegisterStruct.startDate = date1
                        RegisterStruct.startDate1 = date11
                    }else if(popInt == 2){
                       RegisterStruct.endDate = date1
                        RegisterStruct.endDate1 = date11
                    }
                    }
                    
                    
                    
                    NotificationCenter.default.post(name:NSNotification.Name.init("showDatareload"), object: nil)
                    dismiss(animated: true, completion: nil)
                }
                
                
                
                   
               }
        

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


extension DatePopupViewController : MDatePickerViewDelegate {
    func mdatePickerView(selectDate: Date) {
        dateVal = selectDate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "dd MMM yyyy"
        let date = formatter.string(from: selectDate)
        let date1 = formatter1.string(from: selectDate)
       // RegisterStruct.dateofbirth = date
        Label.text = date1
    }
}
