//
//  RegsiterTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 13/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class RegsiterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var imgAgree: UIImageView!
    @IBOutlet weak var imgAge: UIImageView!
    @IBOutlet weak var txtRePassword: DesignableUITextField!
    @IBOutlet weak var txtPasword: DesignableUITextField!
    @IBOutlet weak var txtEmail: DesignableUITextField!
    @IBOutlet weak var txtMobile: DesignableUITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtPasword.isSecureTextEntry = true
        txtRePassword.isSecureTextEntry = true

                 
                      btnSignup.layer.cornerRadius = 20
                      btnSignup.layer.masksToBounds = true
        txtEmail.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtEmail.layer.borderWidth = 1.0
        txtEmail.layer.cornerRadius = 20
        txtPasword.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
               txtPasword.layer.borderWidth = 1.0
               txtPasword.layer.cornerRadius = 20
        txtRePassword.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtRePassword.layer.borderWidth = 1.0
        txtRePassword.layer.cornerRadius = 20
       
        txtMobile.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtMobile.layer.borderWidth = 1.0
        txtMobile.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
