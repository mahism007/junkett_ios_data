//
//  CategoryListCollectionViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 19/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class CategoryListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var viewType: CardView!
    @IBOutlet weak var lblType: UILabel!
}
class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
