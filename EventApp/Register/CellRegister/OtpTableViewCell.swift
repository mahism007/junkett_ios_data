//
//  OtpTableViewCell.swift
//  EventApp
//
//  Created by Bunga Mahesh on 14/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit

class OtpTableViewCell: UITableViewCell {
    @IBOutlet weak var btnOtp: UIButton!
       @IBOutlet weak var txtNo1: DesignableUITextField!
       @IBOutlet weak var txtNo2: DesignableUITextField!
       @IBOutlet weak var txtNo3: DesignableUITextField!
       @IBOutlet weak var txtNo4: DesignableUITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txtNo1.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtNo1.layer.borderWidth = 1.0
        txtNo1.layer.cornerRadius = 20
        
        txtNo2.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtNo2.layer.borderWidth = 1.0
        txtNo2.layer.cornerRadius = 20
        txtNo3.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtNo3.layer.borderWidth = 1.0
        txtNo3.layer.cornerRadius = 20
        txtNo4.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtNo4.layer.borderWidth = 1.0
        txtNo4.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
