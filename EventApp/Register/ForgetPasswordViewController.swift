//
//  ForgetPasswordViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 24/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import DTGradientButton
class ForgetPasswordViewController: CommonClassViewController {
@IBOutlet weak var txtEmail: DesignableUITextField!
    @IBOutlet weak var btnNext: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        
             btnNext.layer.cornerRadius = 20
             btnNext.layer.masksToBounds = true
        txtEmail.layer.borderColor = UIColor.colorwithHexString("D7D7D7", alpha: 1.0)?.cgColor
        txtEmail.layer.borderWidth = 1.0
        txtEmail.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }
    
    var forgetApi = ForgetModelDataAPI()
    func forgetData()  {
         
        let paramCategory = [  "UID": txtEmail.text!
        
                              ] as [String : String]
    
       
           
           forgetApi.serviceCalling(obj: self,  parameter : paramCategory  ) { (dict) in
            
               let storyboard = UIStoryboard(name: "Register", bundle: nil)
                let mainVC  =  storyboard.instantiateViewController(withIdentifier: "ForgetSelectViewController") as! ForgetSelectViewController
               mainVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(mainVC, animated:true)
               
           }
           
           
       }
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if(txtEmail.text != ""){
            forgetData() 
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
