//
//  SixthRegViewController.swift
//  EventApp
//
//  Created by Bunga Mahesh on 17/08/20.
//  Copyright © 2020 Bunga Mahesh. All rights reserved.
//

import UIKit
import SDWebImage
import FluidTabBarController
import Alamofire
class SixthRegViewController:CommonClassViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
  
    @IBOutlet weak var btnFinish: UIButton!
    @IBOutlet weak var imgNext: UIImageView!
    @IBOutlet weak var viewNext: UIView!
    @IBOutlet weak var heightCollect: NSLayoutConstraint!

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        UserDefaults.standard.set(5, forKey: "page")
//    let gradientLayer = CAGradientLayer()
//            gradientLayer.frame = self.view.bounds
//            gradientLayer.colors = [UIColor.colorwithHexString("8BB755", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("7DA64A", alpha: 1.0)?.cgColor,UIColor.colorwithHexString("648738", alpha: 1.0)?.cgColor]
//            self.view.layer.insertSublayer(gradientLayer, at: 0)
        CountryData()
        viewNext.backgroundColor = UIColor.colorwithHexString("8BB755", alpha: 1.0)
        imgNext.setImageColor(color: UIColor.colorwithHexString("8BB755", alpha: 1.0))
        viewNext.isHidden = true
        imgNext.isHidden = true
        btnFinish.isHidden = true
        let flowLayout = AlignedCollectionViewFlowLayout()
        flowLayout.verticalAlignment = .top
         flowLayout.horizontalAlignment = .left
         collectionView.collectionViewLayout = flowLayout
         collectionView.delegate = self
         collectionView.dataSource = self
         collectionView.allowsSelection = true
         collectionView.isUserInteractionEnabled = true
         if(countryListDB.count < 5){
            heightCollect.constant = 200
         }else{
           heightCollect.constant = collectionView.collectionViewLayout.collectionViewContentSize.height ;
        }
//
       
        // Do any additional setup after loading the view.
           NotificationCenter.default.addObserver(self, selector: #selector(self.ProcessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "FinishLoop")), object: nil)
        }
        @objc func ProcessData(){
            viewNext.isHidden = false
            imgNext.isHidden = false
            btnFinish.isHidden = false
    }
    var countryListAPI = CountryModelDataAPI()
    
    func CountryData()  {
      
     
      countryListAPI.serviceCalling( parameter : [:]  ) { (dict) in
            countryListDB = dict as! [CategoryCountryListModel]
            self.collectionView.reloadData()
         if(countryListDB.count < 5){
            self.heightCollect.constant = 200
         }else{
            self.heightCollect.constant = self.collectionView.collectionViewLayout.collectionViewContentSize.height ;
        }
            
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    @IBAction func btnFinishClciked(_ sender: UIButton) {
        
submitData()
        
        
    }

    func submitData(){

          self.startLoading()
          
          DispatchQueue.global(qos: .background).async {
            var EmailVerified = String()
            var MobileVerified = String()
         
            if(RegisterStruct.regType == "Google" || RegisterStruct.regType == "fb"){
                EmailVerified = "1"
                MobileVerified = "0"
            }else{
                EmailVerified = "0"
                MobileVerified = "1"
            }
            
              
            let parameter = [
                "FName": RegisterStruct.firstName,
                 "MName": "",
                 "LName": RegisterStruct.lastName,
                 "Mobile": RegisterStruct.mobileno,
                 "Email": RegisterStruct.emailid,
                 "Registration_Mode": "Email",
                 "Social_AC_ID": RegisterStruct.social,
                 "PWD": RegisterStruct.password,
                 "Gender": RegisterStruct.gender,
                 "DOB": RegisterStruct.dateofbirth1,
                 "State": self.stateName,
                 "StateID": self.statteId,
                 "ShowNotification": "1",
                 "ShowMyLocation": "1",
                 "ShowMyEmail": "1",
                 "ShowMyMobile": "1",
                 "ShowMyGender": "1",
                 "ShowDOB": "1",
                 "BioDescription": "1",
                 "UserCategoryCSV": RegisterStruct.categoryList,
                 "EmailVerified":EmailVerified,
                 "MobileVerified":MobileVerified,
                 "FileUIDCSV":RegisterStruct.fileId
                                
                                
                                ] as [String:Any]
          print(parameter)
            
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.RegistrationCreate, parameters: parameter, successHandler: { (dict:[String : AnyObject]) in
                     
                     if let response = dict["response"]{
                      
                         let objectmsg = MessageCallServerModel1()
                         
                         let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                          let objSucess = SuccessServerModel()
                           let statusStringsuccess = objSucess.sealizeMessage(cell: response as! [String : AnyObject])
                         if(statusStringsuccess == "200"){
                            self.LoginData()
                         
                     }else{
                        self.stopLoading()
                         self.showError(strMessage: msg)
                     }
                    }
                     
                 }) { (err) in
                     self.stopLoading()
                     print(err.description)
                 }
                }
                
    }
    var loginApi = SigninRegisterModelDataAPI()
    var GCMToken = String()
    func LoginData()  {
        if let object = UserDefaults.standard.value(forKey: "Token") {
            
            GCMToken = object as! String
        }
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        print(GCMToken)
         
        let paramCategory = [  "UID": RegisterStruct.emailid,
                                  "PWD": RegisterStruct.password,
           "FCM": GCMToken,
           "SDK_Version": build,
           "App_Version": version,
           "Device_Serial_No": UIDevice.current.identifierForVendor!.uuidString,
           "Manufacturer": UIDevice.current.name,
           "Model": UIDevice.current.model,
           "Platform": URLConstants.platform,
           "SHeight": String(UIScreen.main.bounds.height.description),
           "SWidth": String(UIScreen.main.bounds.width.description)
                              ] as [String : String]
    
       
           
        loginApi.serviceCalling(obj: self,  parameter : paramCategory,tyep : RegisterStruct.regType  ) { (dict) in
            
            UserDefaults.standard.set(true, forKey: "isLogin")
            let tab = self.createTabBarController()
            tab.modalPresentationStyle = .fullScreen
               self.present(tab, animated: true, completion: nil)
               
           }
           
           
       }
    
    private func createTabBarController() -> UITabBarController {
        let tabBarController = FluidTabBarController()
        tabBarController.tabBar.tintColor = UIColor.systemRed
        tabBarController.tabBar.barTintColor = #colorLiteral(red: 0, green: 0.829695344, blue: 0.5481160283, alpha: 1)
       // tabBarController.tabBar.setGradientBackgroundColors([UIColor.init(hexString: "68E32B"),UIColor.init(hexString: "48C20B"),UIColor.init(hexString: "00A1D2")], direction: DTImageGradientDirection.toRight, for: UIControl.State.normal)
        let storyboard = UIStoryboard(name: "Event", bundle: nil)
        let vLoc = storyboard.instantiateViewController(withIdentifier: "ActivityByLocNavViewController") as! ActivityByLocNavViewController
        let itemLoc = FluidTabBarItem(title: "Map Activity", image:UIImage(named: "map_Pin"), tag: 0)
        itemLoc.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vLoc.tabBarItem = itemLoc
        
        
        
        let vMessage = storyboard.instantiateViewController(withIdentifier: "MessageNavViewController") as! MessageNavViewController
        let item1 = FluidTabBarItem(title: "Message", image:UIImage(named: "msg"), tag: 1)
        item1.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vMessage.tabBarItem = item1
        
        let vActivities = storyboard.instantiateViewController(withIdentifier: "ActivityShowNavViewController") as! ActivityShowNavViewController
        let item2 = FluidTabBarItem(title: "Activities", image:UIImage(named: "shirt"), tag: 2)
        item2.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vActivities.tabBarItem = item2
        
        let vProfile = storyboard.instantiateViewController(withIdentifier: "ProfileNavViewController") as! ProfileNavViewController
               let itemPro = FluidTabBarItem(title: "Profile", image:UIImage(named: "profile"), tag: 3)
               itemPro.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               vProfile.tabBarItem = itemPro
        
        let vsetting = storyboard.instantiateViewController(withIdentifier: "SettingNavViewController") as! SettingNavViewController
        let itemsetting = FluidTabBarItem(title: "Setting", image:UIImage(named: "setting"), tag: 4)
        itemsetting.imageColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        vsetting.tabBarItem = itemsetting

        let viewControllers = [
            vMessage,vProfile,vLoc,vActivities,vsetting
        ]
        
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 2
        return tabBarController
    }
    
    @IBAction func btnTypeClicked(_ sender: UIButton) {
 
        if(countryListDB.count > 0){
            for i in 0...countryListDB.count - 1 {
                countryListDB[i].boolSelected = false
            }
        }
        countryListDB[sender.tag].boolSelected = true
        statteId = countryListDB[sender.tag].StateID
        stateName = countryListDB[sender.tag].StateName
        collectionView.reloadData()
      viewNext.isHidden = false
      imgNext.isHidden = false
      btnFinish.isHidden = false
    }
 var statteId = String()
    var stateName = String()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(countryListDB.count > 0){
            return countryListDB.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryListCollectionViewCell

        cell.btnType.tag = indexPath.row
        cell.lblType.text = countryListDB[indexPath.row].StateName
        cell.lblType.textColor = .darkGray
        cell.viewType.backgroundColor = .clear
        cell.viewType.layer.borderWidth = 1.0
        cell.viewType.layer.cornerRadius = 10
        if(countryListDB[indexPath.row].boolSelected == true){
            cell.viewType.layer.borderColor = UIColor.systemRed.cgColor
        }else{
            cell.viewType.layer.borderColor = UIColor.darkGray.cgColor
        }
       if let url = NSURL(string: countryListDB[indexPath.row].StateImagePath) {
        cell.imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "defaulticon"))
       }
       cell.imgView.roundCorners([.topLeft,.bottomLeft], radius: 10)
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {

        
        if(countryListDB.count > 0){
        let label = UILabel(frame: CGRect.zero)
            label.text = countryListDB[indexPath.item].StateName
        label.sizeToFit()
        return CGSize(width: 50 + label.frame.width, height: 40)
        }else{
               return CGSize(width: 50, height: 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("it worked")
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

